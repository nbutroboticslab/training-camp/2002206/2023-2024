;;
;; DO NOT EDIT THIS FILE
;;
;; THIS FILE IS AUTOMATICALLY GENERATED
;;  FROM /home/eaibot/2022_jsjds/ros_ws/src/ros_tutorials/turtlesim/./package.xml (0.7.1)
;; USING /home/eaibot/2022_jsjds/ros_ws/install/lib/python3.6/site-packages/geneus/geneus_main.py /home/eaibot/2022_jsjds/ros_ws/install/share/geneus/package.xml (2.2.6)
;;
(ros::load-ros-package "geometry_msgs")
(ros::load-ros-package "roscpp")
(ros::load-ros-package "std_msgs")
(ros::load-ros-package "std_srvs")
(ros::load-ros-package "turtlesim")
