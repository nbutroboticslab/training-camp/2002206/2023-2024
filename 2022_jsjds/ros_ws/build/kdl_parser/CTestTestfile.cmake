# CMake generated Testfile for 
# Source directory: /home/eaibot/2022_jsjds/ros_ws/src/kdl_parser/kdl_parser
# Build directory: /home/eaibot/2022_jsjds/ros_ws/build/kdl_parser
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_kdl_parser_rostest_test_test_kdl_parser.launch "/home/eaibot/2022_jsjds/ros_ws/build/kdl_parser/catkin_generated/env_cached.sh" "/usr/bin/python3" "/home/eaibot/2022_jsjds/ros_ws/install/share/catkin/cmake/test/run_tests.py" "/home/eaibot/2022_jsjds/ros_ws/build/kdl_parser/test_results/kdl_parser/rostest-test_test_kdl_parser.xml" "--return-code" "/opt/ros/kinetic/share/rostest/cmake/../../../bin/rostest --pkgdir=/home/eaibot/2022_jsjds/ros_ws/src/kdl_parser/kdl_parser --package=kdl_parser --results-filename test_test_kdl_parser.xml --results-base-dir \"/home/eaibot/2022_jsjds/ros_ws/build/kdl_parser/test_results\" /home/eaibot/2022_jsjds/ros_ws/src/kdl_parser/kdl_parser/test/test_kdl_parser.launch ")
add_test(_ctest_kdl_parser_rostest_test_test_inertia_rpy.launch "/home/eaibot/2022_jsjds/ros_ws/build/kdl_parser/catkin_generated/env_cached.sh" "/usr/bin/python3" "/home/eaibot/2022_jsjds/ros_ws/install/share/catkin/cmake/test/run_tests.py" "/home/eaibot/2022_jsjds/ros_ws/build/kdl_parser/test_results/kdl_parser/rostest-test_test_inertia_rpy.xml" "--return-code" "/opt/ros/kinetic/share/rostest/cmake/../../../bin/rostest --pkgdir=/home/eaibot/2022_jsjds/ros_ws/src/kdl_parser/kdl_parser --package=kdl_parser --results-filename test_test_inertia_rpy.xml --results-base-dir \"/home/eaibot/2022_jsjds/ros_ws/build/kdl_parser/test_results\" /home/eaibot/2022_jsjds/ros_ws/src/kdl_parser/kdl_parser/test/test_inertia_rpy.launch ")
subdirs(gtest)
