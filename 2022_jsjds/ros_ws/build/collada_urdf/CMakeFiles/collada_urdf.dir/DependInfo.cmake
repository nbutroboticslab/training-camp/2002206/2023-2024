# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/eaibot/2022_jsjds/ros_ws/src/collada_urdf/collada_urdf/src/collada_urdf.cpp" "/home/eaibot/2022_jsjds/ros_ws/build/collada_urdf/CMakeFiles/collada_urdf.dir/src/collada_urdf.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ASSIMP_EXPORT_API"
  "ASSIMP_UNIFIED_HEADER_NAMES"
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"collada_urdf\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/eaibot/2022_jsjds/ros_ws/src/collada_urdf/collada_urdf/include"
  "/usr/include/assimp"
  "/usr/include/collada-dom2.4/1.5"
  "/usr/include/collada-dom2.4"
  "/home/eaibot/2022_jsjds/ros_ws/install/include"
  "/opt/ros/kinetic/include"
  "/opt/ros/kinetic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/usr/include/eigen3"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
