# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "${prefix}/include;/usr/include/assimp;/usr/include/collada-dom2.4/1.5;/usr/include/collada-dom2.4;/usr/include".split(';') if "${prefix}/include;/usr/include/assimp;/usr/include/collada-dom2.4/1.5;/usr/include/collada-dom2.4;/usr/include" != "" else []
PROJECT_CATKIN_DEPENDS = "collada_parser;geometric_shapes;resource_retriever;rosconsole;urdf".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lcollada_urdf;-lassimp;-lcollada-dom2.4-dp".split(';') if "-lcollada_urdf;-lassimp;-lcollada-dom2.4-dp" != "" else []
PROJECT_NAME = "collada_urdf"
PROJECT_SPACE_DIR = "/home/eaibot/2022_jsjds/ros_ws/install"
PROJECT_VERSION = "1.12.13"
