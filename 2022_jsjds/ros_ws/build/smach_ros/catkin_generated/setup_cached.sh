#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/eaibot/2022_jsjds/ros_ws/devel/.private/smach_ros:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/eaibot/2022_jsjds/ros_ws/devel/.private/smach_ros/lib:$LD_LIBRARY_PATH"
export PWD='/home/eaibot/2022_jsjds/ros_ws/build/smach_ros'
export PYTHONPATH="/home/eaibot/2022_jsjds/ros_ws/devel/.private/smach_ros/lib/python3.6/site-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES='/home/eaibot/2022_jsjds/ros_ws/devel/.private/smach_ros/share/common-lisp'
export ROS_PACKAGE_PATH="/home/eaibot/2022_jsjds/ros_ws/src/executive_smach/smach_ros:$ROS_PACKAGE_PATH"