# generated from catkin/cmake/template/cfg-extras.context.py.in
DEVELSPACE = 'FALSE' == 'TRUE'
INSTALLSPACE = 'TRUE' == 'TRUE'

CATKIN_DEVEL_PREFIX = '/home/eaibot/2022_jsjds/ros_ws/devel/.private/python_qt_binding'

CATKIN_GLOBAL_BIN_DESTINATION = 'bin'
CATKIN_GLOBAL_ETC_DESTINATION = 'etc'
CATKIN_GLOBAL_INCLUDE_DESTINATION = 'include'
CATKIN_GLOBAL_LIB_DESTINATION = 'lib'
CATKIN_GLOBAL_LIBEXEC_DESTINATION = 'lib'
CATKIN_GLOBAL_PYTHON_DESTINATION = 'lib/python3.6/site-packages'
CATKIN_GLOBAL_SHARE_DESTINATION = 'share'

CATKIN_PACKAGE_BIN_DESTINATION = 'lib/python_qt_binding'
CATKIN_PACKAGE_ETC_DESTINATION = 'etc/python_qt_binding'
CATKIN_PACKAGE_INCLUDE_DESTINATION = 'include/python_qt_binding'
CATKIN_PACKAGE_LIB_DESTINATION = 'lib'
CATKIN_PACKAGE_LIBEXEC_DESTINATION = 'lib/python_qt_binding'
CATKIN_PACKAGE_PYTHON_DESTINATION = 'lib/python3.6/site-packages/python_qt_binding'
CATKIN_PACKAGE_SHARE_DESTINATION = 'share/python_qt_binding'

CMAKE_BINARY_DIR = '/home/eaibot/2022_jsjds/ros_ws/build/python_qt_binding'
CMAKE_CURRENT_BINARY_DIR = '/home/eaibot/2022_jsjds/ros_ws/build/python_qt_binding'
CMAKE_CURRENT_SOURCE_DIR = '/home/eaibot/2022_jsjds/ros_ws/src/python_qt_binding'
CMAKE_INSTALL_PREFIX = '/home/eaibot/2022_jsjds/ros_ws/install'
CMAKE_SOURCE_DIR = '/home/eaibot/2022_jsjds/ros_ws/src/python_qt_binding'

PKG_CMAKE_DIR = '${python_qt_binding_DIR}'

PROJECT_NAME = 'python_qt_binding'
PROJECT_BINARY_DIR = '/home/eaibot/2022_jsjds/ros_ws/build/python_qt_binding'
PROJECT_SOURCE_DIR = '/home/eaibot/2022_jsjds/ros_ws/src/python_qt_binding'
