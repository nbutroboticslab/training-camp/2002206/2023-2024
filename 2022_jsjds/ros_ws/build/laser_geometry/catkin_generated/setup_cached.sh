#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/eaibot/2022_jsjds/ros_ws/devel/.private/laser_geometry:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/eaibot/2022_jsjds/ros_ws/devel/.private/laser_geometry/lib:$LD_LIBRARY_PATH"
export PWD='/home/eaibot/2022_jsjds/ros_ws/build/laser_geometry'
export PYTHONPATH="/home/eaibot/2022_jsjds/ros_ws/devel/.private/laser_geometry/lib/python3.6/site-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES='/home/eaibot/2022_jsjds/ros_ws/devel/.private/laser_geometry/share/common-lisp'
export ROS_PACKAGE_PATH="/home/eaibot/2022_jsjds/ros_ws/src/laser_geometry:$ROS_PACKAGE_PATH"