#!/bin/sh

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
fi

echo_and_run() { echo "+ $@" ; "$@" ; }

echo_and_run cd "/home/eaibot/2022_jsjds/ros_ws/src/rqt_image_view"

# ensure that Python install destination exists
echo_and_run mkdir -p "$DESTDIR/home/eaibot/2022_jsjds/ros_ws/install/lib/python3.6/site-packages"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
echo_and_run /usr/bin/env \
    PYTHONPATH="/home/eaibot/2022_jsjds/ros_ws/install/lib/python3.6/site-packages:/home/eaibot/2022_jsjds/ros_ws/build/rqt_image_view/lib/python3.6/site-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/home/eaibot/2022_jsjds/ros_ws/build/rqt_image_view" \
    "/usr/bin/python3" \
    "/home/eaibot/2022_jsjds/ros_ws/src/rqt_image_view/setup.py" \
     \
    build --build-base "/home/eaibot/2022_jsjds/ros_ws/build/rqt_image_view" \
    install \
    --root="${DESTDIR-/}" \
     --prefix="/home/eaibot/2022_jsjds/ros_ws/install" --install-scripts="/home/eaibot/2022_jsjds/ros_ws/install/bin"
