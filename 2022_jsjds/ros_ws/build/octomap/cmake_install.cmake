# Install script for directory: /home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/eaibot/2022_jsjds/ros_ws/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/octomap" TYPE FILE FILES
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/octomap.h"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/octomap_timing.h"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/OcTreeStamped.h"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/CountingOcTree.h"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/MapCollection.h"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/MCTables.h"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/OcTreeBase.h"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/ScanGraph.h"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/ColorOcTree.h"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/OcTree.h"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/AbstractOcTree.h"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/octomap_deprecated.h"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/OcTreeBaseImpl.h"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/OcTreeKey.h"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/OcTreeDataNode.h"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/OccupancyOcTreeBase.h"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/octomap_types.h"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/Pointcloud.h"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/octomap_utils.h"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/AbstractOccupancyOcTree.h"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/MapNode.h"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/OcTreeNode.h"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/OccupancyOcTreeBase.hxx"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/OcTreeIterator.hxx"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/OcTreeDataNode.hxx"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/OcTreeBaseImpl.hxx"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/MapNode.hxx"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/MapCollection.hxx"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/octomap/math" TYPE FILE FILES
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/math/Vector3.h"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/math/Utils.h"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/math/Quaternion.h"
    "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/include/octomap/math/Pose6D.h"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/octomap" TYPE FILE FILES "/home/eaibot/2022_jsjds/ros_ws/src/octomap/octomap/package.xml")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/octomap" TYPE FILE FILES
    "/home/eaibot/2022_jsjds/ros_ws/build/octomap/InstallFiles/octomap-config.cmake"
    "/home/eaibot/2022_jsjds/ros_ws/build/octomap/InstallFiles/octomap-config-version.cmake"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/eaibot/2022_jsjds/ros_ws/build/octomap/lib/pkgconfig/octomap.pc")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/eaibot/2022_jsjds/ros_ws/build/octomap/src/math/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/ros_ws/build/octomap/src/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/eaibot/2022_jsjds/ros_ws/build/octomap/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
