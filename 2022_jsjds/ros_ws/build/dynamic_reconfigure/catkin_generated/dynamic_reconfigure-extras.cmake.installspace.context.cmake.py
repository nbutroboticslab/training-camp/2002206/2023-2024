# generated from catkin/cmake/template/cfg-extras.context.py.in
DEVELSPACE = 'FALSE' == 'TRUE'
INSTALLSPACE = 'TRUE' == 'TRUE'

CATKIN_DEVEL_PREFIX = '/home/eaibot/2022_jsjds/ros_ws/devel/.private/dynamic_reconfigure'

CATKIN_GLOBAL_BIN_DESTINATION = 'bin'
CATKIN_GLOBAL_ETC_DESTINATION = 'etc'
CATKIN_GLOBAL_INCLUDE_DESTINATION = 'include'
CATKIN_GLOBAL_LIB_DESTINATION = 'lib'
CATKIN_GLOBAL_LIBEXEC_DESTINATION = 'lib'
CATKIN_GLOBAL_PYTHON_DESTINATION = 'lib/python3.6/site-packages'
CATKIN_GLOBAL_SHARE_DESTINATION = 'share'

CATKIN_PACKAGE_BIN_DESTINATION = 'lib/dynamic_reconfigure'
CATKIN_PACKAGE_ETC_DESTINATION = 'etc/dynamic_reconfigure'
CATKIN_PACKAGE_INCLUDE_DESTINATION = 'include/dynamic_reconfigure'
CATKIN_PACKAGE_LIB_DESTINATION = 'lib'
CATKIN_PACKAGE_LIBEXEC_DESTINATION = 'lib/dynamic_reconfigure'
CATKIN_PACKAGE_PYTHON_DESTINATION = 'lib/python3.6/site-packages/dynamic_reconfigure'
CATKIN_PACKAGE_SHARE_DESTINATION = 'share/dynamic_reconfigure'

CMAKE_BINARY_DIR = '/home/eaibot/2022_jsjds/ros_ws/build/dynamic_reconfigure'
CMAKE_CURRENT_BINARY_DIR = '/home/eaibot/2022_jsjds/ros_ws/build/dynamic_reconfigure'
CMAKE_CURRENT_SOURCE_DIR = '/home/eaibot/2022_jsjds/ros_ws/src/dynamic_reconfigure'
CMAKE_INSTALL_PREFIX = '/home/eaibot/2022_jsjds/ros_ws/install'
CMAKE_SOURCE_DIR = '/home/eaibot/2022_jsjds/ros_ws/src/dynamic_reconfigure'

PKG_CMAKE_DIR = '${dynamic_reconfigure_DIR}'

PROJECT_NAME = 'dynamic_reconfigure'
PROJECT_BINARY_DIR = '/home/eaibot/2022_jsjds/ros_ws/build/dynamic_reconfigure'
PROJECT_SOURCE_DIR = '/home/eaibot/2022_jsjds/ros_ws/src/dynamic_reconfigure'
