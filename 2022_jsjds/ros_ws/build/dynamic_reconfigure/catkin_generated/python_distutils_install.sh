#!/bin/sh

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
fi

echo_and_run() { echo "+ $@" ; "$@" ; }

echo_and_run cd "/home/eaibot/2022_jsjds/ros_ws/src/dynamic_reconfigure"

# ensure that Python install destination exists
echo_and_run mkdir -p "$DESTDIR/home/eaibot/2022_jsjds/ros_ws/install/lib/python3.6/site-packages"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
echo_and_run /usr/bin/env \
    PYTHONPATH="/home/eaibot/2022_jsjds/ros_ws/install/lib/python3.6/site-packages:/home/eaibot/2022_jsjds/ros_ws/build/dynamic_reconfigure/lib/python3.6/site-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/home/eaibot/2022_jsjds/ros_ws/build/dynamic_reconfigure" \
    "/usr/bin/python3" \
    "/home/eaibot/2022_jsjds/ros_ws/src/dynamic_reconfigure/setup.py" \
     \
    build --build-base "/home/eaibot/2022_jsjds/ros_ws/build/dynamic_reconfigure" \
    install \
    --root="${DESTDIR-/}" \
     --prefix="/home/eaibot/2022_jsjds/ros_ws/install" --install-scripts="/home/eaibot/2022_jsjds/ros_ws/install/bin"
