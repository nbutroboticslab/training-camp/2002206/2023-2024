#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/eaibot/2022_jsjds/ros_ws/devel/.private/camera_calibration_parsers:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/eaibot/2022_jsjds/ros_ws/devel/.private/camera_calibration_parsers/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/home/eaibot/2022_jsjds/ros_ws/devel/.private/camera_calibration_parsers/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD='/home/eaibot/2022_jsjds/ros_ws/build/camera_calibration_parsers'
export PYTHONPATH="/home/eaibot/2022_jsjds/ros_ws/devel/.private/camera_calibration_parsers/lib/python3.6/site-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES='/home/eaibot/2022_jsjds/ros_ws/devel/.private/camera_calibration_parsers/share/common-lisp'
export ROS_PACKAGE_PATH="/home/eaibot/2022_jsjds/ros_ws/src/image_common/camera_calibration_parsers:$ROS_PACKAGE_PATH"