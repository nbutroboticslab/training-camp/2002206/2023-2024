# CMake generated Testfile for 
# Source directory: /home/eaibot/2022_jsjds/ros_ws/src/roscpp_core/rostime
# Build directory: /home/eaibot/2022_jsjds/ros_ws/build/rostime
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_rostime_gtest_rostime-test_duration "/home/eaibot/2022_jsjds/ros_ws/build/rostime/catkin_generated/env_cached.sh" "/usr/bin/python3" "/home/eaibot/2022_jsjds/ros_ws/install/share/catkin/cmake/test/run_tests.py" "/home/eaibot/2022_jsjds/ros_ws/build/rostime/test_results/rostime/gtest-rostime-test_duration.xml" "--return-code" "/home/eaibot/2022_jsjds/ros_ws/devel/.private/rostime/lib/rostime/rostime-test_duration --gtest_output=xml:/home/eaibot/2022_jsjds/ros_ws/build/rostime/test_results/rostime/gtest-rostime-test_duration.xml")
add_test(_ctest_rostime_gtest_rostime-test_time "/home/eaibot/2022_jsjds/ros_ws/build/rostime/catkin_generated/env_cached.sh" "/usr/bin/python3" "/home/eaibot/2022_jsjds/ros_ws/install/share/catkin/cmake/test/run_tests.py" "/home/eaibot/2022_jsjds/ros_ws/build/rostime/test_results/rostime/gtest-rostime-test_time.xml" "--return-code" "/home/eaibot/2022_jsjds/ros_ws/devel/.private/rostime/lib/rostime/rostime-test_time --gtest_output=xml:/home/eaibot/2022_jsjds/ros_ws/build/rostime/test_results/rostime/gtest-rostime-test_time.xml")
subdirs(gtest)
