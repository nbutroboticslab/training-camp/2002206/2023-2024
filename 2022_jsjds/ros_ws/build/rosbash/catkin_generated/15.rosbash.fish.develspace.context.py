# generated from catkin/cmake/template/env-hook.context.py.in
DEVELSPACE = True
INSTALLSPACE = False

CATKIN_DEVEL_PREFIX = '/home/eaibot/2022_jsjds/ros_ws/devel/.private/rosbash'

CATKIN_GLOBAL_BIN_DESTINATION = 'bin'
CATKIN_GLOBAL_ETC_DESTINATION = 'etc'
CATKIN_GLOBAL_INCLUDE_DESTINATION = 'include'
CATKIN_GLOBAL_LIB_DESTINATION = 'lib'
CATKIN_GLOBAL_LIBEXEC_DESTINATION = 'lib'
CATKIN_GLOBAL_PYTHON_DESTINATION = 'lib/python3.6/site-packages'
CATKIN_GLOBAL_SHARE_DESTINATION = 'share'

CATKIN_PACKAGE_BIN_DESTINATION = 'lib/rosbash'
CATKIN_PACKAGE_ETC_DESTINATION = 'etc/rosbash'
CATKIN_PACKAGE_INCLUDE_DESTINATION = 'include/rosbash'
CATKIN_PACKAGE_LIB_DESTINATION = 'lib'
CATKIN_PACKAGE_LIBEXEC_DESTINATION = 'lib/rosbash'
CATKIN_PACKAGE_PYTHON_DESTINATION = 'lib/python3.6/site-packages/rosbash'
CATKIN_PACKAGE_SHARE_DESTINATION = 'share/rosbash'

CMAKE_BINARY_DIR = '/home/eaibot/2022_jsjds/ros_ws/build/rosbash'
CMAKE_CURRENT_BINARY_DIR = '/home/eaibot/2022_jsjds/ros_ws/build/rosbash'
CMAKE_CURRENT_SOURCE_DIR = '/home/eaibot/2022_jsjds/ros_ws/src/ros/rosbash'
CMAKE_INSTALL_PREFIX = '/home/eaibot/2022_jsjds/ros_ws/install'
CMAKE_SOURCE_DIR = '/home/eaibot/2022_jsjds/ros_ws/src/ros/rosbash'

PROJECT_NAME = 'rosbash'
PROJECT_BINARY_DIR = '/home/eaibot/2022_jsjds/ros_ws/build/rosbash'
PROJECT_SOURCE_DIR = '/home/eaibot/2022_jsjds/ros_ws/src/ros/rosbash'

PYTHON_EXECUTABLE = '/usr/bin/python3'
