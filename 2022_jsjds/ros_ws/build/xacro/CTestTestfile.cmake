# CMake generated Testfile for 
# Source directory: /home/eaibot/2022_jsjds/ros_ws/src/xacro
# Build directory: /home/eaibot/2022_jsjds/ros_ws/build/xacro
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_xacro_nosetests_test "/home/eaibot/2022_jsjds/ros_ws/build/xacro/catkin_generated/env_cached.sh" "/usr/bin/python3" "/home/eaibot/2022_jsjds/ros_ws/install/share/catkin/cmake/test/run_tests.py" "/home/eaibot/2022_jsjds/ros_ws/build/xacro/test_results/xacro/nosetests-test.xml" "--return-code" "\"/usr/bin/cmake\" -E make_directory /home/eaibot/2022_jsjds/ros_ws/build/xacro/test_results/xacro" "/usr/bin/nosetests3 -P --process-timeout=60 --where=/home/eaibot/2022_jsjds/ros_ws/src/xacro/test --with-xunit --xunit-file=/home/eaibot/2022_jsjds/ros_ws/build/xacro/test_results/xacro/nosetests-test.xml")
subdirs(gtest)
subdirs(test)
