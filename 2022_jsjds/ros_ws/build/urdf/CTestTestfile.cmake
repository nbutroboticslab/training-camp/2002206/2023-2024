# CMake generated Testfile for 
# Source directory: /home/eaibot/2022_jsjds/ros_ws/src/urdf/urdf
# Build directory: /home/eaibot/2022_jsjds/ros_ws/build/urdf
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_urdf_rostest_test_test_robot_model_parser.launch "/home/eaibot/2022_jsjds/ros_ws/build/urdf/catkin_generated/env_cached.sh" "/usr/bin/python3" "/home/eaibot/2022_jsjds/ros_ws/install/share/catkin/cmake/test/run_tests.py" "/home/eaibot/2022_jsjds/ros_ws/build/urdf/test_results/urdf/rostest-test_test_robot_model_parser.xml" "--return-code" "/opt/ros/kinetic/share/rostest/cmake/../../../bin/rostest --pkgdir=/home/eaibot/2022_jsjds/ros_ws/src/urdf/urdf --package=urdf --results-filename test_test_robot_model_parser.xml --results-base-dir \"/home/eaibot/2022_jsjds/ros_ws/build/urdf/test_results\" /home/eaibot/2022_jsjds/ros_ws/src/urdf/urdf/test/test_robot_model_parser.launch ")
add_test(_ctest_urdf_gtest_urdfdom_compatibility_test "/home/eaibot/2022_jsjds/ros_ws/build/urdf/catkin_generated/env_cached.sh" "/usr/bin/python3" "/home/eaibot/2022_jsjds/ros_ws/install/share/catkin/cmake/test/run_tests.py" "/home/eaibot/2022_jsjds/ros_ws/build/urdf/test_results/urdf/gtest-urdfdom_compatibility_test.xml" "--return-code" "/home/eaibot/2022_jsjds/ros_ws/devel/.private/urdf/lib/urdf/urdfdom_compatibility_test --gtest_output=xml:/home/eaibot/2022_jsjds/ros_ws/build/urdf/test_results/urdf/gtest-urdfdom_compatibility_test.xml")
subdirs(gtest)
