# CMake generated Testfile for 
# Source directory: /home/eaibot/2022_jsjds/ros_ws/src/stage
# Build directory: /home/eaibot/2022_jsjds/ros_ws/build/stage
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(libstage)
subdirs(examples)
subdirs(assets)
subdirs(worlds)
