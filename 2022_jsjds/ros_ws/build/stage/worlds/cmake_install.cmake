# Install script for directory: /home/eaibot/2022_jsjds/ros_ws/src/stage/worlds

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/eaibot/2022_jsjds/ros_ws/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RELEASE")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/stage/worlds" TYPE FILE FILES
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/everything.cfg"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/test.cfg"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/lsp_test.cfg"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/mbicp.cfg"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/wavefront-remote.cfg"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/amcl-sonar.cfg"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/simple.cfg"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/autolab.cfg"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/uoa_robotics_lab.cfg"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/wifi.cfg"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/vfh.cfg"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/wavefront.cfg"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/nd.cfg"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/fasr.world"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/everything.world"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/SFU.world"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/simple.world"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/mbicp.world"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/large.world"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/fasr2.world"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/fasr_plan.world"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/wifi.world"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/pioneer_walle.world"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/pioneer_flocking.world"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/sensor_noise_module_demo.world"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/autolab.world"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/uoa_robotics_lab.world"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/lsp_test.world"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/objects.inc"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/ubot.inc"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/sick.inc"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/hokuyo.inc"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/irobot.inc"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/pioneer.inc"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/beacons.inc"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/chatterbox.inc"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/uoa_robotics_lab_models.inc"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/walle.inc"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/pantilt.inc"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/map.inc"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/worldgen.sh"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/cfggen.sh"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/worlds/test.sh"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/eaibot/2022_jsjds/ros_ws/build/stage/worlds/benchmark/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/ros_ws/build/stage/worlds/bitmaps/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/ros_ws/build/stage/worlds/wifi/cmake_install.cmake")

endif()

