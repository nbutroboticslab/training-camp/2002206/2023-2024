# Install script for directory: /home/eaibot/2022_jsjds/ros_ws/src/stage/assets

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/eaibot/2022_jsjds/ros_ws/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RELEASE")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/stage/assets" TYPE FILE FILES
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/assets/red.png"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/assets/green.png"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/assets/mains.png"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/assets/blue.png"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/assets/question_mark.png"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/assets/death.png"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/assets/logo.png"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/assets/mainspower.png"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/assets/stall.png"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/assets/stagelogo.png"
    "/home/eaibot/2022_jsjds/ros_ws/src/stage/assets/rgb.txt"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/stage" TYPE FILE FILES "/home/eaibot/2022_jsjds/ros_ws/src/stage/assets/rgb.txt")
endif()

