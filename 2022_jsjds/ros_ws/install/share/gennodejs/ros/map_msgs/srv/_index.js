
"use strict";

let ProjectedMapsInfo = require('./ProjectedMapsInfo.js')
let GetPointMap = require('./GetPointMap.js')
let GetMapROI = require('./GetMapROI.js')
let SaveMap = require('./SaveMap.js')
let SetMapProjections = require('./SetMapProjections.js')
let GetPointMapROI = require('./GetPointMapROI.js')

module.exports = {
  ProjectedMapsInfo: ProjectedMapsInfo,
  GetPointMap: GetPointMap,
  GetMapROI: GetMapROI,
  SaveMap: SaveMap,
  SetMapProjections: SetMapProjections,
  GetPointMapROI: GetPointMapROI,
};
