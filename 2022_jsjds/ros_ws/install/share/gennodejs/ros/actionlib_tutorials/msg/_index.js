
"use strict";

let AveragingAction = require('./AveragingAction.js');
let FibonacciActionFeedback = require('./FibonacciActionFeedback.js');
let AveragingGoal = require('./AveragingGoal.js');
let AveragingActionResult = require('./AveragingActionResult.js');
let FibonacciFeedback = require('./FibonacciFeedback.js');
let FibonacciAction = require('./FibonacciAction.js');
let AveragingActionFeedback = require('./AveragingActionFeedback.js');
let FibonacciResult = require('./FibonacciResult.js');
let FibonacciActionResult = require('./FibonacciActionResult.js');
let FibonacciActionGoal = require('./FibonacciActionGoal.js');
let AveragingResult = require('./AveragingResult.js');
let FibonacciGoal = require('./FibonacciGoal.js');
let AveragingActionGoal = require('./AveragingActionGoal.js');
let AveragingFeedback = require('./AveragingFeedback.js');

module.exports = {
  AveragingAction: AveragingAction,
  FibonacciActionFeedback: FibonacciActionFeedback,
  AveragingGoal: AveragingGoal,
  AveragingActionResult: AveragingActionResult,
  FibonacciFeedback: FibonacciFeedback,
  FibonacciAction: FibonacciAction,
  AveragingActionFeedback: AveragingActionFeedback,
  FibonacciResult: FibonacciResult,
  FibonacciActionResult: FibonacciActionResult,
  FibonacciActionGoal: FibonacciActionGoal,
  AveragingResult: AveragingResult,
  FibonacciGoal: FibonacciGoal,
  AveragingActionGoal: AveragingActionGoal,
  AveragingFeedback: AveragingFeedback,
};
