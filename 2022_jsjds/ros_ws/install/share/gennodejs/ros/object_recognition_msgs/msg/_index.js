
"use strict";

let ObjectRecognitionActionFeedback = require('./ObjectRecognitionActionFeedback.js');
let ObjectRecognitionActionResult = require('./ObjectRecognitionActionResult.js');
let ObjectRecognitionFeedback = require('./ObjectRecognitionFeedback.js');
let ObjectRecognitionResult = require('./ObjectRecognitionResult.js');
let ObjectRecognitionGoal = require('./ObjectRecognitionGoal.js');
let ObjectRecognitionActionGoal = require('./ObjectRecognitionActionGoal.js');
let ObjectRecognitionAction = require('./ObjectRecognitionAction.js');
let ObjectInformation = require('./ObjectInformation.js');
let RecognizedObject = require('./RecognizedObject.js');
let ObjectType = require('./ObjectType.js');
let Table = require('./Table.js');
let RecognizedObjectArray = require('./RecognizedObjectArray.js');
let TableArray = require('./TableArray.js');

module.exports = {
  ObjectRecognitionActionFeedback: ObjectRecognitionActionFeedback,
  ObjectRecognitionActionResult: ObjectRecognitionActionResult,
  ObjectRecognitionFeedback: ObjectRecognitionFeedback,
  ObjectRecognitionResult: ObjectRecognitionResult,
  ObjectRecognitionGoal: ObjectRecognitionGoal,
  ObjectRecognitionActionGoal: ObjectRecognitionActionGoal,
  ObjectRecognitionAction: ObjectRecognitionAction,
  ObjectInformation: ObjectInformation,
  RecognizedObject: RecognizedObject,
  ObjectType: ObjectType,
  Table: Table,
  RecognizedObjectArray: RecognizedObjectArray,
  TableArray: TableArray,
};
