
"use strict";

let ModelCoefficients = require('./ModelCoefficients.js');
let PointIndices = require('./PointIndices.js');
let PolygonMesh = require('./PolygonMesh.js');
let Vertices = require('./Vertices.js');

module.exports = {
  ModelCoefficients: ModelCoefficients,
  PointIndices: PointIndices,
  PolygonMesh: PolygonMesh,
  Vertices: Vertices,
};
