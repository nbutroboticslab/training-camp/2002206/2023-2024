# Install script for directory: /home/eaibot/2022_jsjds/rosbase/dashgo_ws/src

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/eaibot/2022_jsjds/rosbase/dashgo_ws/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  
      if (NOT EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}")
        file(MAKE_DIRECTORY "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}")
      endif()
      if (NOT EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/.catkin")
        file(WRITE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/.catkin" "")
      endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/eaibot/2022_jsjds/rosbase/dashgo_ws/install/_setup_util.py")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/eaibot/2022_jsjds/rosbase/dashgo_ws/install" TYPE PROGRAM FILES "/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/catkin_generated/installspace/_setup_util.py")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/eaibot/2022_jsjds/rosbase/dashgo_ws/install/env.sh")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/eaibot/2022_jsjds/rosbase/dashgo_ws/install" TYPE PROGRAM FILES "/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/catkin_generated/installspace/env.sh")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/eaibot/2022_jsjds/rosbase/dashgo_ws/install/setup.bash;/home/eaibot/2022_jsjds/rosbase/dashgo_ws/install/local_setup.bash")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/eaibot/2022_jsjds/rosbase/dashgo_ws/install" TYPE FILE FILES
    "/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/catkin_generated/installspace/setup.bash"
    "/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/catkin_generated/installspace/local_setup.bash"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/eaibot/2022_jsjds/rosbase/dashgo_ws/install/setup.sh;/home/eaibot/2022_jsjds/rosbase/dashgo_ws/install/local_setup.sh")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/eaibot/2022_jsjds/rosbase/dashgo_ws/install" TYPE FILE FILES
    "/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/catkin_generated/installspace/setup.sh"
    "/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/catkin_generated/installspace/local_setup.sh"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/eaibot/2022_jsjds/rosbase/dashgo_ws/install/setup.zsh;/home/eaibot/2022_jsjds/rosbase/dashgo_ws/install/local_setup.zsh")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/eaibot/2022_jsjds/rosbase/dashgo_ws/install" TYPE FILE FILES
    "/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/catkin_generated/installspace/setup.zsh"
    "/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/catkin_generated/installspace/local_setup.zsh"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/eaibot/2022_jsjds/rosbase/dashgo_ws/install/.rosinstall")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/eaibot/2022_jsjds/rosbase/dashgo_ws/install" TYPE FILE FILES "/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/catkin_generated/installspace/.rosinstall")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/gtest/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo_src/navigation/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo/dashgo_description/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo/dashgo_nav/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo/dashgo_rviz/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo_src/map_server/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo/smart_node/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo_src/amcl/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo/dashgo_tools/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo/mapping/slam_gmapping-hydro-devel/gmapping/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo_src/robot_pose_ekf/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo_src/fake_localization/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo/laser_filters/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo/traffic/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo/rviz/rviz-kinetic-devel/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo/rviz/loop_move/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo/rviz/move_to_grab/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo_src/voxel_grid/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo_src/costmap_2d/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo_src/nav_core/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo_src/base_local_planner/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo_src/carrot_planner/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo_src/clear_costmap_recovery/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo_src/dwa_local_planner/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo_src/move_slow_and_clear/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo_src/navfn/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo_src/global_planner/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo_src/rotate_recovery/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo_src/move_base/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo_src/teb_local_planner/cmake_install.cmake")
  include("/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo/ydlidar_ros-master/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
