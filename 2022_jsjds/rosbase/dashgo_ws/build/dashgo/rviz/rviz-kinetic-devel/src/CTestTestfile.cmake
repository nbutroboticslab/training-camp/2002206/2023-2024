# CMake generated Testfile for 
# Source directory: /home/eaibot/2022_jsjds/rosbase/dashgo_ws/src/dashgo/rviz/rviz-kinetic-devel/src
# Build directory: /home/eaibot/2022_jsjds/rosbase/dashgo_ws/build/dashgo/rviz/rviz-kinetic-devel/src
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(rviz)
subdirs(image_view)
subdirs(test)
subdirs(python_bindings)
