
"use strict";

let MoveToGrabMsg = require('./MoveToGrabMsg.js');
let LoopMoveToGrabMsg = require('./LoopMoveToGrabMsg.js');

module.exports = {
  MoveToGrabMsg: MoveToGrabMsg,
  LoopMoveToGrabMsg: LoopMoveToGrabMsg,
};
