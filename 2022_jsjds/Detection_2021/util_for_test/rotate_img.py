import cv2 as cv
import numpy as np
from util.utils_cv import test_imshow
img = cv.imread("./img/1153.jpg")

h,w = img.shape[:2]
padding = (w - h) // 2
center = ((w // 2), (h // 2))

img_padded = np.zeros(shape=(w, w, 3), dtype=np.uint8)
img_padded[padding:padding+h, :, :] = img
print(img_padded.shape)
test_imshow(img_padded,scale_percent=20)
M = cv.getRotationMatrix2D(center, -90, 1)
rotated_padded = cv.warpAffine(img_padded, M, (w, w))
print(rotated_padded.shape)
test_imshow(rotated_padded,20)
output = rotated_padded[:w-padding, :h, :]
test_imshow(output,20)
cv.imwrite("./img/1153_Rotated.jpg", output)
