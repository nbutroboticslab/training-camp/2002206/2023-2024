import cv2 as cv

cap = cv.VideoCapture(0)
_,frame = cap.read()
y,x,_ = frame.shape
while True:
    _,frame = cap.read()
    cv.line(frame,(0,y//2),(x,y//2),(0,0,255),2)
    cv.line(frame,(x//2,0),(x//2,y),(0,0,255),2)
    cv.imshow("test",frame)
    cv.waitKey(30)
