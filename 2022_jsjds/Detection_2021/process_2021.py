# -*- coding: utf-8 -*-

from logging import fatal
import os
import sys
from time import clock
import threading

# 移除ros默认的python路径避免python版本错误
try:
    sys.path.remove("/opt/ros/kinetic/lib/python2.7/dist-packages")
except:
    pass

from serial.tools import list_ports
from pydobotkeo.dobot import Dobot
from cv_2021 import depth_l, get_box_name, pipeline_stop
from drop_2021 import *
from arm_init import *
from grab_2021 import grab, l_mutex
from util.keys_prov import *
from util.utils_cv import *

# 导入省份信息字典
from util.keys_prov import province

##################################################################
# 决策部分，使用较为简单的顺时针或逆时针策略
##################################################################
def find_position_inidex(name, drop_boxes):
    for i, boxName in enumerate(drop_boxes):
        if name == boxName:
            return i
    return "null"


def path_planning(store, drop_boxes, clockwise):
    """
    路径规划,按顺时针或逆时针前进
    """
    positions = []
    path = []
    location_area_1 = []
    location_area_2 = []
    location_area_3 = []

    for box in store.values():
        positionIndex = find_position_inidex(box[1], drop_boxes)
        if positionIndex != "null":
            print(positionIndex)
            positions.append(positionIndex)

    for i in positions:
        if i in range(0, 2):
            location_area_1.append(i)
        elif i in range(4, 10):
            location_area_2.append(i)
        elif i in range(2, 4):
            location_area_3.append(i)

    if not clockwise:
        # 逆时针
        location_area_1.sort(reverse=True)
        location_area_2.sort(reverse=True)
        location_area_3.sort(reverse=True)
        path = location_area_1 + location_area_2 + location_area_3
    else:
        # 顺时针
        location_area_1.sort()
        location_area_2.sort()
        location_area_3.sort()
        path = location_area_3 + location_area_2 + location_area_1

    return path


##################################################################
# 工具函数
##################################################################
def store_full(store_list):
    """检测是否已装满"""
    for _, v in store_list.items():
        if v[0]:
            return False
    return True


def store_empty(store_list):
    """检测是否全部为空"""
    for _, v in store_list.items():
        if not v[0]:
            return False
    return True


def check_store(box_name, store_list):
    """
    box_name: 投递箱名
    store_list: 车载物品信息
    检查是否有与当前投递箱匹配的盒子
    """
    for i in range(2, 0, -1):
        if not store_list["r" + str(i)][0] and store_list["r" + str(i)][1] == box_name:
            return True
        if not store_list["l" + str(i)][0] and store_list["l" + str(i)][1] == box_name:
            return True
    return False


def move(dst):
    """向前或向后移动一定距离"""
    os.system(
        ". ~/anaconda3/etc/profile.d/conda.sh && conda deactivate && conda deactivate && rosrun dashgo_tools move.py "
        + str(dst)
    )


def navigate_to_point(x, y, angle):
    """导航至某点"""
    os.system(
        "rosrun dashgo_tools to_point.py tmp_point "
        + str(x)
        + " "
        + str(y)
        + " "
        + str(angle)
    )

##################################################################
# 投递过程
##################################################################
def drop_process():
    """
    投递步骤
    """

    global offset_dst
    global store
    global end

    # 先前进至中转处
    os.system(
        "rosrun dashgo_tools to_point.py tmp_point "
        + str(3.30 - offset_dst - 0.9)
        + " "
        + str(1.5)
        + " "
        + str(-170)
    )
    device.rotate_to(90, 0, 0, 0, wait=True)
    # 进入投递循环
    while not store_empty(store):
        name = None  # 当前目标投递盒
        drop_box_index = None  # 目标投递盒序号

        # 当所有识别点均完成识别
        if drop_boxes.count("None") == 0:
            positionIndexs = path_planning(store, drop_boxes, False)
            print(positionIndexs)
            for positionIndex in positionIndexs:
                os.system("rosrun dashgo_tools to_point.py p" + str(positionIndex))
                name = drop_boxes[positionIndex]
                store = drop_by_name(device, store, name)

        else:
            # 逆时针策略
            for i, box in zip(
                [1, 0, 9, 8, 7, 6, 5, 4, 3, 2],
                drop_boxes[1::-1] + drop_boxes[9:3:-1] + drop_boxes[3:1:-1],
            ):
                if box != "None" and check_store(box, store):
                    name, drop_box_index = box, i  # 如果盒子标签对应的投递点已知且载有对应盒子
                    break

            if drop_box_index is None:  # 没有对应的投递盒
                for i, box in zip(
                    [8, 9, 0, 1, 6, 7, 4, 5, 2, 3],
                    drop_boxes[8:]
                    + drop_boxes[0:2]
                    + drop_boxes[6:8]
                    + drop_boxes[4:6]
                    + drop_boxes[2:4],
                ):
                    if box == "None":
                        os.system(
                            "rosrun dashgo_tools to_point.py r" + str(i // 2)
                        )  # 到第i个识别点侧对
                        box_left, box_right = get_box_name()
                        drop_boxes[i : i + 2] = box_left, box_right  # 标签赋值
                        flag = True
                        if check_store(box_right, store):
                            os.system("rosrun dashgo_tools to_point.py p" + str(i + 1))
                            store = drop_by_name(device, store, box_right)
                            flag = False
                            break
                        elif check_store(box_left, store):
                            if i == 2 and flag:
                                os.system(
                                    ". ~/anaconda3/etc/profile.d/conda.sh && conda deactivate && conda deactivate && rosrun dashgo_tools rotate.py 150"
                                )
                            elif i == 4 and flag:
                                os.system(
                                    ". ~/anaconda3/etc/profile.d/conda.sh && conda deactivate && conda deactivate && rosrun dashgo_tools rotate.py -150"
                                )

                            os.system("rosrun dashgo_tools to_point.py p" + str(i))
                            store = drop_by_name(device, store, box_left)
                            break

            else:
                os.system("rosrun dashgo_tools to_point.py p" + str(i))  # 到第i个抓取点侧对
                store = drop_by_name(device, store, name)
                pass

    if not end:
        #  复位到抓取形态
        device.rotate_to(0.0, 0.0, 0.0, 0.0, True)
        # device.move_to(234.8227081298828, 0, 168.0938690185547, 0)
        device.move_to(220.30555725097656, 0.0, 170.3561553955078, 0, wait=True)


##################################################################
# 抓取过程
##################################################################
def grab_process():

    global offset_dst
    global end

    while not store_full(store) and offset_dst < 1.68:

        # 启动抓取线程
        p = threading.Thread(target=grab, args=(device, store,))
        p.start()

        # 等待和处理子线程传递的信号
        while 1:
            # 判断深度是否已检测完毕
            if depth_l[0]:
                print("--------get depth!--------")
                # 深度是否符合要求（相机下有无物品）
                if depth_l[1]:
                    while 1:
                        # 判断抓取范围计算是否已完毕
                        if l_mutex[2]:
                            # 是否在抓取范围内
                            if l_mutex[1]:
                                print("------needGrab!------")
                                while 1:
                                    # 判断是否已经抓取物品并处于放置阶段
                                    if l_mutex[0]:
                                        navigate_to_point(3.10-offset_dst,1.94,-180)
                                        break
                            else:
                                navigate_to_point(3.10-offset_dst,1.94,-180)
                            break
                else:
                    navigate_to_point(3.10-offset_dst,1.94,-180)
                break

        # 等待抓取完毕
        if p.is_alive():
            p.join()
            print("waiting for grab thread finished...")

        # 重置所有信号量
        for i in range(3):
            l_mutex[i] = False
            if i != 2:
                depth_l[i] = False

        offset_dst += 0.08

        if offset_dst >= 1.68:
            os.system(
                ". ~/anaconda3/etc/profile.d/conda.sh && conda deactivate && conda deactivate && rosrun dashgo_tools move.py -0.5"
            )
            end = True

        print(store)
        # time.sleep(1)

    print("grab phase has completed, start to drop...")


if __name__ == "__main__":

    #############
    # 初始化操作
    #############
    for i in range(0, 4):
        try:
            port = list_ports.comports()[i].device  # 寻找机械臂端口
            device = Dobot(port=port, verbose=False)
        except:
            pass
        else:
            break
    # 机械臂初始化操作
    device._set_queued_cmd_clear()
    device.clear_all_alarms_state()
    device.rotate_to(90, 0, 0, 0, wait=True)

    # 初始化投递盒-序号以左上角为0,顺时针为正向一直到左下角的9号箱,起初十个均未知
    drop_boxes = ["None"] * 10

    # 第一层，第二层，第三层高度信息
    drop_position = [-27.0, 10, 44.0]

    #############
    # 预动作
    #############

    init()  # 机械臂初始化
    # 先识别6个投递箱
    for i, drop_box in zip([8, 0, 6], drop_boxes):
        os.system("rosrun dashgo_tools to_point.py r" + str(i // 2))
        box_left, box_right = get_box_name()
        drop_boxes[i : i + 2] = box_left, box_right
        print(drop_boxes)

    os.system("rosrun dashgo_tools to_point.py start")  # 到第一个抓取点,正对

    #############
    # 抓取-投递循环
    #############
    offset_dst = 0.08  # 每次前进的距离(m)
    end = False
    for _ in range(4):
        # 初始化车载空间，True表示空
        store = {"l1": [True, ""], "l2": [True, ""], "r1": [True, ""], "r2": [True, ""]}
        full = False  # 是否装满
        stored = True  # 是否还有盒子
        name = None  # 当前的目标投递盒

        grab_process()
        drop_process()

        if offset_dst >= 1.7:
            print("结束，返回起点...")
            break

    #############
    # 返回起点
    #############
    os.system(
        "rosrun dashgo_tools to_point.py tmp_point "
        + str(1.0)
        + " "
        + str(1.0)
        + " "
        + str(0)
    )
    device.rotate_to(90, 0, 0, 0, wait=True)
    pipeline_stop()
