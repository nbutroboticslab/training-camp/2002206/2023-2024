
from IPython import get_ipython
import time
import os
import tensorflow as tf
import cv2 as cv
import numpy as np
import sys
# sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
from util.utils_cv import *
from util.keys_prov import *
from util.utils_recg import *
from ctpn.text_detect import text_detect
from imutils import perspective

import matplotlib.pyplot as plt

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

# get_ipython().run_line_magic('matplotlib', 'inline')
# plt.rcParams['figure.figsize'] = (16.0,9.0) # set default size of plots
# plt.rcParams['figure.dpi']=300
# plt.rcParams['image.interpolation'] = 'nearest'
# plt.rcParams['image.cmap'] = 'gray'


def get_image(num):
    cap = cv.VideoCapture(0)
    for i in range(0,30):
        _, origin_img = cap.read()
    #origin_img = cv.imread("./test2.jpg")
    blur_img = cv.blur(origin_img, (4, 4))
    # 直方图均值化
    lab = cv.cvtColor(blur_img, cv.COLOR_BGR2LAB)
    lab_planes = cv.split(lab)
    clahe = cv.createCLAHE(clipLimit=3.0, tileGridSize=(8, 8))
    lab_planes[0] = clahe.apply(lab_planes[0])
    lab = cv.merge(lab_planes)
    clahe_img = cv.cvtColor(lab, cv.COLOR_LAB2BGR)
    # clahe1 = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
    # clahe2 = clahe1.apply(clahe)
    # hist1 = cv2.calcHist([clahe2], [0], None, [256], [0, 256])
    # hsv的下红色范围
    hsv = cv.cvtColor(clahe_img, cv.COLOR_BGR2HSV)
    # h,s,v = cv.split(hsv)
    # if v!=125:
    #     v = 125
    # hsv = cv.merge(h,s,v)
    lower_red = np.array([0, 40, 40])
    upper_red = np.array([10, 255, 255])

    kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (3, 3))
    if num ==0:
        lower_red = np.array([0, 40, 40])
        upper_red = np.array([10, 255, 255])
        mask_red = cv.inRange(hsv, lower_red, upper_red)
        kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (5, 5))
        mask_red = cv.morphologyEx(
            mask_red, cv.MORPH_OPEN, kernel, iterations=1)
        lower_red = np.array([160, 30, 40])
        upper_red = np.array([180, 255, 255])
        mask_red_2 = cv.inRange(hsv, lower_red, upper_red)
        kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (3, 3))
        mask_red_2 = cv.morphologyEx(
            mask_red_2, cv.MORPH_OPEN, kernel, iterations=1)
    elif num ==1:
        lower_red = np.array([0, 40, 40])
        upper_red = np.array([10, 255, 255])
        mask_red = cv.inRange(hsv, lower_red, upper_red)
        mask_red = cv.morphologyEx(
            mask_red, cv.MORPH_OPEN, kernel, iterations=1)
        lower_red = np.array([160, 30, 40])
        upper_red = np.array([180, 255, 255])
        mask_red_2 = cv.inRange(hsv, lower_red, upper_red)
        kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (3, 3))
        mask_red_2 = cv.morphologyEx(
            mask_red_2, cv.MORPH_OPEN, kernel, iterations=1)
    else:
        lower_red = np.array([0, 40, 40])
        upper_red = np.array([10, 255, 255])
        mask_red = cv.inRange(hsv, lower_red, upper_red)
        # kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (7, 7))
        mask_red = cv.morphologyEx(
            mask_red, cv.MORPH_OPEN, kernel, iterations=1)
        lower_red = np.array([160, 30, 40])
        upper_red = np.array([180, 255, 255])
        mask_red_2 = cv.inRange(hsv, lower_red, upper_red)
        kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (1, 1))
        mask_red_2 = cv.morphologyEx(
            mask_red_2, cv.MORPH_OPEN, kernel, iterations=1)

    # hsv的上红色范围
    # test_imshow(mask_red)
    # test_imshow(mask_red_2, cv_show=True)

    mask_red = cv.bitwise_or(mask_red, mask_red_2)
    # test_imshow(mask_red, cv_show=True)
    _, mask_red = cv.threshold(mask_red, 250, 255, cv.THRESH_BINARY)
    # test_imshow(mask_red, cv_show=True)
    # -------------------
    kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (4, 4))
    # mask = mask_red.copy()
    mask_red = cv.morphologyEx(mask_red, cv.MORPH_CLOSE, kernel, iterations=1)
    # test_imshow(mask_red, cv_show=True)
    # -------------------
    kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (2, 2))
    mask_red = cv.morphologyEx(mask_red, cv.MORPH_OPEN, kernel, iterations=1)
    # test_imshow(mask_red,cv_show = True)
    test_imshow(mask_red, cv_show=True)
    return mask_red, origin_img

def get_image_closer(lower=False):
    cap = cv.VideoCapture(0)
    for i in range(0,30):
        _, origin_img = cap.read()
        # i += 1
    blur_img = cv.blur(origin_img, (5, 5))

    # 直方图均值化
    # lab = cv.cvtColor(blur_img, cv.COLOR_BGR2LAB)
    # lab_planes = cv.split(lab)
    # clahe = cv.createCLAHE(clipLimit=3.0, tileGridSize=(16, 16))
    # lab_planes[0] = clahe.apply(lab_planes[0])
    # lab = cv.merge(lab_planes)
    # clahe_img = cv.cvtColor(lab, cv.COLOR_LAB2BGR)
    # hsv的下红色范围
    hsv = cv.cvtColor(blur_img, cv.COLOR_BGR2HSV)
    lower_red = np.array([0, 60, 60])
    upper_red = np.array([10, 255, 255])

    mask_red = cv.inRange(hsv, lower_red, upper_red)
    if lower:
        kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (1, 1))
    else:
        kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (2, 2))
    mask_red = cv.morphologyEx(
        mask_red, cv.MORPH_CLOSE, kernel, iterations=1)
    # test_imshow(mask_red,cv_show=True)
    # hsv的上红色范围
    lower_red = np.array([160, 60, 60])
    upper_red = np.array([180, 255, 255])
    mask_red_2 = cv.inRange(hsv, lower_red, upper_red)
    kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (2, 2))
    mask_red_2 = cv.morphologyEx(
        mask_red_2, cv.MORPH_OPEN, kernel, iterations=1)
    # test_imshow(mask_red_2, cv_show=True)f

    mask_red = cv.bitwise_or(mask_red, mask_red_2)
    # test_imshow(mask_red, cv_show=True)
    _, mask_red = cv.threshold(mask_red, 250, 255, cv.THRESH_BINARY)
    # test_imshow(mask_red, cv_show=True)
    # -------------------
    kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (5, 5))
    # mask = mask_red.copy()
    mask_red = cv.morphologyEx(mask_red, cv.MORPH_CLOSE, kernel, iterations=1)
    # test_imshow(clahe_img,100)
    # test_imshow(mask_red,100)
    return mask_red, origin_img

def get_info(show_img):
    for i in range(0,3):
        print("--------------- "+str(i)+"--------------------")
        mask_red, origin_img = get_image(i)
        img, contours, _ = cv.findContours(
            mask_red, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
        test_img = origin_img.copy()

        boxes, positions, angles, _,_ = find_label(
            contours, 60, 190, 70, 700, 1400, 51.458)
        if len(boxes)>0:
            break
    for box in boxes:
        cv.drawContours(test_img, [box], 0, (0, 0, 255), 2)
    for position in positions:
        cv.putText(test_img, str(position[0])+' '+str(position[1]), (int(
            position[0]), int(position[1])), cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255))
    if show_img:
        test_imshow(origin_img, cv_show=True)
        test_imshow(mask_red, cv_show=True)
        test_imshow(test_img, cv_show=True)
        pass
    relative_positions_to_grab = []
    y, x = origin_img.shape[:2]
    # test_imshow(test_img, cv_show=False)
    for position in positions:
        relative_y = (position[1]-y/2)/y*200
        relative_x = -(position[0]-x/2)/x*200
        relative_positions_to_grab.append([relative_x, relative_y])

    return relative_positions_to_grab, angles


def get_address(show_img):
    labels = []
    results = []
    result = None
    for i in range(0,2):
        mask_red, origin_img = get_image_closer(i==1)
        # test_imshow(mask_red,100,cv_show=True)
        img, contours, _ = cv.findContours(
            mask_red, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
        test_img = origin_img.copy()

        # boxes = find_boxes_closer(contours)
        box, _, _, flip,reverse = find_label(contours, 100, 360, 140, 1000, 4500, 88.5)

        if len(box)!=0:
            break

    # if show_img:
    #         test_imshow(origin_img)
    #         test_imshow(mask_red)
    if show_img:
            test_imshow(origin_img)
            test_imshow(mask_red)
            pass
    if len(box)>0:
        box = box[0]
        # for box in boxes:
        # cv.drawContours(test_img, [box], 0, (0, 0, 255), 2)
        label = crop_rotate_box(box, origin_img, flip=flip,reverse = reverse)
        # for position in positions  :
        #     cv.putText(test_img,str(position[0])+' '+str(position[1]),(
        #       int(position[0]),int(position[1])),cv.FONT_HERSHEY_SIMPLEX,0.5,(0,0,255))
        text_recs, temp, img = text_detect(label)

        text_recs = sort_box(text_recs)
        result = crnnRec(img, text_recs, adjust=True)
        if show_img:
            test_imshow(test_img)
            test_imshow(label)
            test_imshow(temp)
            pass
        for key in result:
            # print(result[key][1])
            results.append(result[key][1])
        print(results)
        return results
    else:
        print("no result")
        return ["no result"]
# print(get_info(True))
def get_address_new(img,show_img):
    labels = []
    results = []
    result = None
    # for i in range(0,2):
    #     mask_red, origin_img = get_image_closer(i==1)
    #     # test_imshow(mask_red,100,cv_show=True)
    #     img, contours, _ = cv.findContours(
    #         mask_red, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    #     test_img = origin_img.copy()

    #     # boxes = find_boxes_closer(contours)
    #     box, _, _, flip,reverse = find_label(contours, 100, 360, 140, 1000, 4500, 88.5)

    #     if len(box)!=0:
    #         break

    # if show_img:
    #         test_imshow(origin_img)
    #         test_imshow(mask_red)
    # if show_img:
    #         test_imshow(origin_img)
    #         test_imshow(mask_red)
    #         pass
    # if len(box)>0:
    #     box = box[0]
    #     # for box in boxes:
    #     # cv.drawContours(test_img, [box], 0, (0, 0, 255), 2)
    #     label = crop_rotate_box(box, origin_img, flip=flip,reverse = reverse)
    #     # for position in positions  :
    #     #     cv.putText(test_img,str(position[0])+' '+str(position[1]),(
    #     #       int(position[0]),int(position[1])),cv.FONT_HERSHEY_SIMPLEX,0.5,(0,0,255))
    text_recs, temp, img = text_detect(img)

    text_recs = sort_box(text_recs)
    result = crnnRec(img, text_recs, adjust=True)
    if show_img:
        test_imshow(img)
        # test_imshow(label)
        test_imshow(temp)
        pass
    for key in result:
        # print(result[key][1])
        results.append(result[key][1])
    true_result = []
    for key in province:
        for result in results:
            if result.find(key)>-1:
                true_result.append(province[key]) 
    # true_result = "test"
    if true_result is not None:
        print("result = "+str(true_result))
    print(results)
    return results,true_result
# else:
#     print("no result")
#     return ["no result"]
if __name__ == '__main__':
    origin_img = cv.imread("./img/019.jpg")
    get_address_new(origin_img,True)