from IPython import get_ipython

import os
import cv2 as cv
import numpy as np
import sys
# sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
from util.utils_cv import test_imshow, find_box, crop_rotate_box, rotate_img
from util.keys_prov import *
from util.utils_recg import *
import matplotlib.pyplot as plt

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
def get_info(show_img):
    # cap = cv.VideoCapture(0)
    # i = 1
    # while(i < 40):
    #     _, origin_img = cap.read()
    #     i += 1
    origin_img = cv.imread("./img/019.jpg")
    # print(origin_img)
    # hsv = cv.cvtColor(origin_img, cv.COLOR_BGR2HSV)
    # lower_red = np.array([0, 60, 87])
    # upper_red = np.array([10, 255, 255])
    # mask_red = cv.inRange(hsv, lower_red, upper_red)
    # lower_pink = np.array([57, 10, 150])
    # upper_pink = np.array([69, 60, 200])
    # mask_pink = cv.inRange(hsv, lower_pink,upper_pink)
    # test_imshow(mask_pink, cv_show=True)

    # gray_img = cv.cvtColor(origin_img, cv.COLOR_BGR2GRAY)
    # # _, thresh_img = cv.threshold(
    # #     gray_img, 190, 255, cv.THRESH_BINARY)
    # _, thresh_img = cv.threshold(
    #     gray_img, 0, 255, cv.THRESH_BINARY+cv.THRESH_OTSU)
    # thresh_img = cv.bitwise_or(thresh_img, mask_red)
    # thresh_img = cv.bitwise_or(thresh_img, mask_pink)
    # test_imshow(thresh_img, cv_show=False)
    # blur_img = cv.erode(thresh_img,(31,31),6)
    # blur_img = cv.medianBlur(thresh_img, 1)
    # blur_img = cv.dilate(blur_img, (7, 7), 1)
    test_imshow(origin_img)
    #OpenCV3
    # img, contours,_her = cv.findContours(
    #     blur_img, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
    # OpenCV4
    contours,_ = cv.findContours(
        origin_img, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
    # contours = contours[::-1]
    labels = []
    test_img = origin_img.copy()
    boxes, positions, angles = find_box(contours)
    for box in boxes:
        cv.drawContours(test_img, [box], 0, (0, 0, 255), 3)
        labels.append(crop_rotate_box(box, origin_img))
    if show_img:
        test_imshow(origin_img, cv_show=True)
        # test_imshow(blur_img, cv_show=True)
        test_imshow(test_img, cv_show=True)
    # test_imshow(copyIma,cv_show=False,save_img=True)
    test_imshow(mask_pink,cv_show=True)
    relative_positions_to_grab = []
    y, x = origin_img.shape[:2]
    for position in positions:
        cv.putText(origin_img,str(position[0])+' '+str(position[1]),(int(position[0]),int(position[1])),cv.FONT_HERSHEY_SIMPLEX,2,(0,0,255))
    for position in positions:
        relative_y = (position[1]-y/2)/y*200
        relative_x = -(position[0]-x/2)/x*200
        relative_positions_to_grab.append([relative_x, relative_y])
    return relative_positions_to_grab, angles
get_info(True)