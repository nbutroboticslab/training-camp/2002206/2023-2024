from serial.tools import list_ports
# from cv_pro import camera_activate
from pydobotkeo.dobot import Dobot
import joblib
import time
import math
for i in range(0,4):
    try:
        port = list_ports.comports()[i].device
        device = Dobot(port=port, verbose=False)
    except:
        pass
    else:
        break
device._set_queued_cmd_clear()
device.clear_all_alarms_state()
store = joblib.load('./depository/store.pkl')
# store_position = [None,-5,15,35,55]
drop_position = [-30.0,9,43.0,60.0,50.0]
# device.rotate_to(90.0, 0.0, 0.0, 0, True)
drop_y = 167
drop_x = 68
print(store)
print(drop_position)
def drop():
    device.rotate_to(90.0, 0.0, 0.0, -90.0, True)
    device.rotate_to(-90.0, 50.0, 0.0, 0.0, True)
    time.sleep(0.5)
    device.suck(False)
    device.rotate_to(90.0, 0.0, 0.0, -90.0, True)
# def drop_trick_r():
#     device.move_to(drop_x, drop_y, 5, 0,True)
#     device.move_to(drop_x, drop_y, -30, 0, True)
#     time.sleep(0.5)
#     device.suck(True)
#     time.sleep(1)
#     device.move_to(drop_x, drop_y, 5, 0,True)
#     drop()
# def drop_trick_l():
#     device.move_to(-drop_x, drop_y, 5, 0, True)
#     device.move_to(-drop_x, drop_y, -30, 0, True)
#     time.sleep(0.5)
#     device.suck(True)
#     time.sleep(1)
#     device.move_to(-drop_x, drop_y, 5, 0,True)
#     drop()

# def drop_all():
#     for i in range(4,0,-1):
#         if not store['r'+ str(i)][0]:
#             device.move_to(drop_x, drop_y, drop_position[i], 0,True)
#             device.move_to(drop_x, drop_y, drop_position[i-1], 0, True)
#             store['r'+ str(i)][0]=True
#             time.sleep(0.5)
#             device.suck(True)
#             time.sleep(1)
#             device.move_to(drop_x, drop_y, drop_position[i], 0,True)
#             # device.move_to(-70, drop_y, 20, 0, True)
#             drop()
#             break
#         if not store['l'+ str(i)][0]:
#             device.move_to(-drop_x, drop_y, drop_position[i], 0, True)
#             device.move_to(-drop_x, drop_y, drop_position[i-1], 0, True)
#             store['l'+ str(i)][0]=True
#             time.sleep(0.5)
#             device.suck(True)
#             time.sleep(1)
#             device.move_to(-drop_x, drop_y, drop_position[i], 0,True)
#             # device.move_to(70, drop_y,20 , 0, True)
#             drop()
#             break
#     joblib.dump(store, './depository/store.pkl')



def grab_process(position):
    global store
    x ,i = position
    if x == 'l':
        x = -drop_x
    else:
        x = drop_x
    device.move_to(x, drop_y, drop_position[i], 0,True)
    device.move_to(x, drop_y, drop_position[i-1], 0, True)
    time.sleep(0.5)
    device.suck(True)
    time.sleep(1)
    device.move_to(x, drop_y, 45, 0,True)
    device.rotate_to(90.0, 0.0, 0.0, -90.0, True)

def pre_grab_process(position):
    # store = joblib.load('./depository/store.pkl')
    global store
    towards ,i = position
    if towards =='l':
        x = -drop_x
        reverse_towards = 'r'
    else:
        x = drop_x
        reverse_towards = 'l'
    ##################################################################
    #转移过程
    ##################################################################
    device.move_to(x, drop_y+20, drop_position[i+1], 0,True)
    device.move_to(x, drop_y+20, drop_position[i], 0, True)
    time.sleep(0.5)
    device.suck(True)
    time.sleep(1)
    device.move_to(x, drop_y+20, 80, 0,True)
    device.move_to(-x, drop_y+20, 80, 0,True)
    device.move_to(-x, drop_y+20, 60, 0,True)
    time.sleep(0.5)
    device.suck(False)
    device.move_to(x, drop_y+20, 60, 0,True)
    ##################################################################
    #抓取过程
    ##################################################################
    grab_process(position)
    drop()
    ##################################################################
    #返回过程
    ##################################################################

    for i in range(1,4):
        if store[reverse_towards+str(i)][0]:
            device.move_to(-x, drop_y+20, drop_position[i], 0,True)
            device.move_to(-x, drop_y+20, drop_position[i-1], 0, True)
            time.sleep(0.5)
            device.suck(True)
            time.sleep(1)
            device.move_to(-x, drop_y+20, 60, 0,True)
            device.move_to(x, drop_y+20, 60, 0,True)
            time.sleep(0.5)
            device.suck(False)
            #清理存储顺序
            store[towards+str(1)][1] = store[towards+str(2)][1]
            store[towards+str(1)][0] = False
            store[towards+str(2)][0] = True
            print("move job done with "+ str(store))
            print("#############################################")
            # joblib.dump(store, './depository/store.pkl')
            break
    # store['r'+ str(i)][1]=store['r'+ str(i+1)][1]

def drop_by_name(name):
    device.rotate_to(90.0, 0.0, 0.0, -90.0, True)
    # store = joblib.load('./depository/store.pkl')
    global store
    for i in range(4,0,-1):
        # store = joblib.load('./depository/store.pkl')
        if not store['r'+ str(i)][0] and (store['r'+ str(i)][1]==name or name=='test') :
            # store = joblib.load('./depository/store.pkl')
            #如果上面有盒子
            if not store['r'+ str(i+1)][0]:
                pre_grab_process(['r',i])#搬開上面的盒子
            else:
                grab_process(['r',i])
                drop()
                store['r'+ str(i)][0]=True
            print("move job done with "+ str(store))
            print("#############################################")
        # store = joblib.load('./depository/store.pkl')
        if not store['l'+ str(i)][0] and (store['l'+ str(i)][1]==name or name=='test'):
            # store = joblib.load('./depository/store.pkl')
            if not store['l'+ str(i+1)][0]:#如果上面有盒子
                pre_grab_process(['l',i])#搬開上面的盒子
            else:
                grab_process(['l',i])
                drop()
                store['l'+ str(i)][0]=True
            print("move job done with "+ str(store))
            print("#############################################")
    joblib.dump(store, './depository/store.pkl')
    device.rotate_to(90.0, 0.0, 0.0, -90.0, True)
#drop_all()
