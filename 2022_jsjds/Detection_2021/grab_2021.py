#!/home/eaibot/anaconda3/envs/pro/bin/python

import math
import os
from posixpath import join
import sys
import time
from typing import Tuple
from matplotlib.pyplot import box

try:
    sys.path.remove("/opt/ros/kinetic/lib/python2.7/dist-packages")
except:
    pass

from serial.tools import list_ports
from multiprocessing import Process
import threading

from cv_2021 import get_boxes_info, q, recongnize_task
from pydobotkeo.dobot import Dobot

store_position = [None, 30, 30, 30, 55]  # 存储四个高度
# 与主线程交互的三个信号量，分别判断是否已抓取货物，是否在抓取范围内，抓取范围的计算是否已完成
l_mutex = [False] * 3

# 存储函数
def locate(device, store, q_indexes):
    full = False
    for i in range(1, 3):
        if store["l" + str(i)][0]:
            device.rotate_to(110.0, 0.0, 0.0, -90, True)
            device.move_to(-50, 180, store_position[i], 0, True)
            device.move_to(-50, 180, store_position[i], 0, True)

            if recongnize_task.is_alive():
                recongnize_task.join()

            for index in q_indexes:
                if index == 0:
                    q.get()

            result = q.get()
            store["l" + str(i)][1] = result
            store["l" + str(i)][0] = False
            print("store " + str(result) + " to l" + str(i))
            break
        if store["r" + str(i)][0]:
            device.rotate_to(70.0, 0.0, 0.0, -90, True)
            device.move_to(50, 180, store_position[i], 0, True)
            device.move_to(50, 180, store_position[i], 0, True)

            if recongnize_task.is_alive():
                print("waiting for result----")
                recongnize_task.join()

            for index in q_indexes:
                if index == 0:
                    q.get()

            result = q.get()

            store["r" + str(i)][0] = False
            store["r" + str(i)][1] = result
            print("store " + str(result) + " to r" + str(i))
            full = i == 2
            break

    print("device suck finished")
    device.suck(False)
    print("--------------grabed-----------------")
    device.rotate_to(90.0, 0.0, 0.0, 0.0, True)
    device.rotate_to(0.0, 0.0, 0.0, 0.0, True)

    return full


def coordinateLimit(x, y):
    length = x ** 2 + y ** 2
    if length > 160 ** 2 and length < 326.67 ** 2:
        return True
    print("out of length")
    return False


def cameraLimit(x, y):
    if x > 10 and x < 50:
        return True
    print("not a proper position")
    return False


def grab(device, store):
    # 末端执行器到气泵
    coord_endpoint_to_sucker_z = -76
    # 摄像机拍摄点到末端执行器的坐标变换
    coord_camera_to_endpoint_x = 50
    coord_camera_to_endpoint_y = 40
    coord_camera_to_endpoint_z = 29

    global l_mutex
    lock = threading.Lock()

    base_x = 220.30555725097656
    base_y = 0.0
    base_z = 170.3561553955078
    grab_angle = 0

    # boxes: 快递盒坐标与角度| 摄像头坐标系
    boxes = get_boxes_info()  # 获取每个盒子相对摄像头左镜头的坐标与角度

    flag = False
    grab_info_list = []  # 每个可抓取盒子的信息列表
    winthin_list = []  # 记录可抓取盒子的个数

    q_indexes = []  # q中范围外结果的序号列表
    q_update_flag = False  # 是否要将q队尾的数据取出

    box_index = 0
    for coord, angle in boxes:  # 对于每一个盒子
        x, y, z = coord  # 物品中心点相对摄像头坐标系的空间坐标

        # 将参考坐标系变换为末端执行器上的坐标系
        endpoint_x = -y + coord_camera_to_endpoint_x
        endpoint_y = -x + coord_camera_to_endpoint_y
        endpoint_z = -z + coord_camera_to_endpoint_z

        # 确定最终的抓取点坐标
        grab_x = endpoint_x + base_x - 10
        grab_y = endpoint_y + base_y
        grab_z = endpoint_z + base_z - coord_endpoint_to_sucker_z

        print(grab_x, grab_y)

        within = coordinateLimit(grab_x, grab_y)
        # 信号量更新----------------------------------------
        lock.acquire()
        l_mutex[1] = within  # needGrab
        lock.release()

        lock.acquire()
        l_mutex[2] = True  # 检测是否在范围内完毕
        lock.release()
        # -----------------------------------------
        if within:
            grab_info_list.append([grab_x, grab_y, grab_z, angle])
            winthin_list.append(True)
        else:
            q_indexes.append(box_index)
            print("no result, regarded as unknown or out of length")
        box_index += 1
    for index in q_indexes:
        if index != 0:
            q_update_flag = True
        else:
            q.get()

    i = 0
    item_count = winthin_list.count(True)
    print(item_count)
    for info in grab_info_list:
        i += 1

        # 抓取部分
        device.move_to(info[0], info[1], info[2] - 13, grab_angle - info[3], wait=True)
        device.suck(True)
        time.sleep(0.3)
        device.rotate_to(grab_angle, 0.0, 0.0, 0.0, True)

        if item_count == i:
            lock.acquire()
            l_mutex[0] = True  # 抓取到空中再前进信号打开
            lock.release()

        if locate(device, store, q_indexes):
            print("fulled")
            device.suck(False)
            break

        device._set_queued_cmd_clear()
        device.clear_all_alarms_state()
        # device.move_to(234.8227081298828, 0, 161.0938690185547, 0)
        device.move_to(220.30555725097656, 0.0, 170.3561553955078, 0, wait=True)
        # device.rotate_to(0,-5,-15,0,wait=True)

    if q_update_flag:
        q.get()

    return store


def grab_test():
    for i in range(0, 4):
        try:
            port = list_ports.comports()[i].device
            device = Dobot(port=port, verbose=False)
        except:
            pass
        else:
            break
    device._set_queued_cmd_clear()
    device.clear_all_alarms_state()

    # device.move_to(234.8227081298828, 0, 168.0938690185547, 0)
    # device.rotate_to(0,-5,-15,0,wait=True) #highest position
    device.move_to(220.30555725097656, 0.0, 170.3561553955078, 0, wait=True)
    store = {
        "l1": [False, ""],
        "l2": [False, ""],
        "l3": [True, ""],
        "l4": [False, ""],
        "r1": [True, ""],
        "r2": [True, ""],
        "r3": [True, ""],
        "r4": [True, ""],
    }
    # 进行抓取
    p = threading.Thread(target=grab, args=(device, store,))
    print("jojo")
    p.start()
    result = 0
    for i in range(100):
        result += i
    print(result)
    if p.isAlive():
        print("waiting for the subthread over...")
        p.join()
    print("---over---")


if __name__ == "__main__":
    grab_test()
