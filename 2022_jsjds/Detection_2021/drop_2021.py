from io import TextIOWrapper
from grab_2021 import grab
from serial.tools import list_ports
import time

import sys
sys.path.append("..")
from pydobotkeo.dobot import Dobot

drop_position = [-30.0, 7, 43.0, 60.0, 50.0]
drop_j2 = 40
drop_j3 = 0
drop_y = 180
drop_x = 50


def drop(device, mode=1):
    """
        投放动作，分两种模式
    """

    device.rotate_to(90.0, 0.0, 0.0, -90.0, True)
    # 侧投
    if mode == 0:
        device.rotate_to(0, 50.0, 0.0, 0.0, True)

    # 后投
    else:
        device.rotate_to(-90, 50.0, 0.0, 0.0, True)
    device.suck(False)
    time.sleep(0.5)


def grab(device, position):
    """
        抓取动作
    """
    x, i = position
    if x == "l":
        x = -drop_x
    else:
        x = drop_x
    device.move_to(x, drop_y, drop_position[i], 0, True)
    device.move_to(x, drop_y, drop_position[i - 1], 0, True)
    device.suck(True)
    time.sleep(1)
    device.move_to(x, drop_y, 45, 0, True)
    device.rotate_to(90.0, 0.0, 0.0, -90.0, True)


def grab_bottom(device, store, position):
    """
        货物不在顶层，转移货物再投放最后更新store
    """
    towards, i = position
    if towards == "l":
        x = -drop_x
        reverse_towards = "r"
    else:
        x = drop_x
        reverse_towards = "l"
    ###########
    # 转移过程
    ###########
    device.move_to(x, drop_y + 20, drop_position[i + 1], 0, True)
    device.move_to(x, drop_y + 20, drop_position[i], 0, True)
    device.suck(True)
    time.sleep(0.5)
    device.move_to(x, drop_y + 20, 80, 0, True)
    device.move_to(-x, drop_y + 20, 80, 0, True)
    device.move_to(-x, drop_y + 20, 60, 0, True)
    time.sleep(0.5)
    device.suck(False)
    device.move_to(x, drop_y + 20, 60, 0, True)
    ###########
    # 抓取--投放
    ###########
    grab(device, position)
    drop(device)
    ###########
    # 更新过程
    ############
    flag = False    #决定是否要回调货物
    for i in range(1, 3):
        if store[reverse_towards + str(i)][0] == False:
            flag = True
    if flag:
        for i in range(2, 0, -1):
            if not store[reverse_towards + str(i)][0]:  # 根据对面的情况抓取(原本有一个或两个)
                device.rotate_to(90.0, 0.0, 0.0, -90.0, True)
                device.move_to(-x, drop_y + 20, drop_position[i], 0, True)
                time.sleep(0.1)
                device.suck(True)
                time.sleep(1)
                device.move_to(-x, drop_y + 20, 60, 0, True)
                device.move_to(x, drop_y + 20, 60, 0, True)
                time.sleep(0.1)
                device.suck(False)
                # 清理存储顺序
                store[towards + str(1)][1] = store[towards + str(2)][1]
                store[towards + str(1)][0] = False
                store[towards + str(2)][0] = True
                store[towards + str(2)][1] = ""
                break
    else:
        store[reverse_towards + str(1)][0] = False
        store[reverse_towards + str(1)][1] = store[towards + str(2)][1]
        for i in range(1, 3):
            store[towards + str(i)][0] = True
            store[towards + str(i)][1] = ""

    return store

def drop_by_name(device, store, name, mode=1):
    """
        调用接口,根据传入的名字选择特定货物投放
    """
    for i in range(2, 0, -1):
        if not store["r" + str(i)][0] and (
            store["r" + str(i)][1] == name or name == "test" or name == "unknown"
        ):
            if i < 2 and not store["r" + str(i + 1)][0]:  # 如果货物不在顶层
                store = grab_bottom(device, store, ["r", i])
            else:
                grab(device, ["r", i])
                drop(device, mode)
                store["r" + str(i)][0] = True
                store["r" + str(i)][1] = ""
            print("move job done with " + str(store))
            print("#############################################")

        elif not store["l" + str(i)][0] and (
            store["l" + str(i)][1] == name or name == "test" or name == "unknown"
        ):

            if i < 2 and not store["l" + str(i + 1)][0]:  # 如果上面有盒子
                store = grab_bottom(device, store, ["l", i])  # 搬开上面的盒子
            else:
                grab(device, ["l", i])
                drop(device, mode)
                store["l" + str(i)][0] = True
                store["l" + str(i)][1] = ""
            print("move job done with " + str(store))
            print("#############################################")

    device.rotate_to(90.0, 0.0, 0.0, -90.0, True)
    return store

def drop_test():

    for i in range(0, 4):
        try:
            port = list_ports.comports()[i].device
            device = Dobot(port=port, verbose=False)
        except:
            pass
        else:
            break
    device._set_queued_cmd_clear()
    device.clear_all_alarms_state()
    device.rotate_to(90, 0, 0, 0, wait=True)
    store = {
        "l1": [False, "jiji"],
        "l2": [False, "ssd"],
        "l3": [True, ""],
        "l4": [True, ""],
        "r1": [False, "dada"],
        "r2": [False, "didi"],
        "r3": [True, ""],
        "r4": [True, ""],
    }

    drop_by_name(device, store, "dada")

if __name__ == "__main__":
    drop_test()
