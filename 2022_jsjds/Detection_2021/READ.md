# 流程及机械臂阅读手册


## 流程


## 机械臂

### 主要函数

```
# 机械臂底盘转动，PTPMode.MOVJ_ANGLE
rotate_to(-90.0,0.0,0.0,0.0,True)

# 机械臂上肢运动 PTPMode.MOVL_XYZ
move_to(0 , -200,0, -90, wait=True)

# 吸盘控制
suck(True)  # 开
suck(False)  # 关
```