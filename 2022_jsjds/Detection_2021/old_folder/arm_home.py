from serial.tools import list_ports
from pydobotkeo.dobot import Dobot

for i in range(0,4):
    try:
        port = list_ports.comports()[i].device
        device = Dobot(port=port, verbose=False)
    except:
        pass
    else:
        break

device.clear_all_alarms_state()
device.go_home()