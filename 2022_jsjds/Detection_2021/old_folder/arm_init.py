from serial.tools import list_ports
from pydobotkeo.dobot import Dobot

for i in range(0,4):
    try:
        port = list_ports.comports()[i].device
        device = Dobot(port=port, verbose=False)
    except:
        pass
    else:
        break

def init():
    """机械臂初始化"""
    device._set_queued_cmd_clear()
    device.clear_all_alarms_state()
    device.suck(False)
    device.go_home()
    
    # device.move_to(234.8227081298828, 0, 169.0938690185547, 0)
    # device.rotate_to(0,-5,-15,40,wait=True) #highest position
    device.move_to(220.30555725097656,0.0,170.3561553955078,0,wait=True) #highest position

    print(device.pose())

if __name__=='__main__':
    init()
