#!/bin/bash
rostopic pub -1 /move_base_simple/goal move_base_msgs/MoveBaseGoal "
target_pose:
    header:
        seq: 0
        stamp:
            secs: 1608877072
            nsecs: 564020392
        frame_id: 'map'
    pose:
        position:
            x: 1.06864976883
            y: 0.13583651185
            z: 0.0
        orientation:
            x: 0.0
            y: 0.0
            z: 0.497782833881
            w: 0.86730170"