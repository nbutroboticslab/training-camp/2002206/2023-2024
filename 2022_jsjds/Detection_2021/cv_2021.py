import sys
import os

from numpy.core.fromnumeric import searchsorted

try:
    sys.path.remove("/opt/ros/kinetic/lib/python2.7/dist-packages")
except:
    pass
import cv2 as cv
import numpy as np
import threading
import queue
import pyrealsense2 as rs
import time

from paddleocr.paddleocr import PaddleOCR, main
from util.keys_prov import *
from util.utils_cv import sorted_boxes, test_imshow, crop_rotate_box

q = queue.Queue()  # 存储识别结果队列
depth_l = [False] * 2  # 获取深度信息信号量

paddle = PaddleOCR()

pipeline = rs.pipeline()
config = rs.config()
config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 6)  # 不设置像素会导致无法识别近距离内容
config.enable_stream(rs.stream.color, 1280, 720, rs.format.bgr8, 6)

pipeline.start(config)
for i in range(20):
    _ = pipeline.wait_for_frames()  # 预读取帧以消耗缓存中摄像头白平衡尚未完成自适应调整的帧
    print("depth camera preprocess " + str(i))

recongnize_task = threading.Thread()


def get_boxes_info():
    # TODO:盒子紧靠时的识别
    # TODO:多次检测以获取平均值
    # Start streaming
    depth_sensor = profile.get_device().first_depth_sensor()
    depth_scale = depth_sensor.get_depth_scale()
    depth_profile = rs.video_stream_profile(profile.get_stream(rs.stream.depth))
    depth_intrinsics = depth_profile.get_intrinsics()
    print("Depth Scale is: ", depth_scale)
    # We will be removing the background of objects more than
    #  clipping_distance_in_meters meters away
    clipping_distance_in_meters = 0.305  # 1 meter
    clipping_distance = clipping_distance_in_meters / depth_scale

    # Create an align object
    # rs.align allows us to perform alignment of depth frames to others frames
    # The "align_to" is the stream type to which we plan to align depth frames.
    align_to = rs.stream.color
    align = rs.align(align_to)
    try:
        # for i in range(20):
        # Get frameset of color and depth
        frames = pipeline.wait_for_frames()
        # frames.get_depth_frame() is a 640x360 depth image

        # Align the depth frame to color frame
        aligned_frames = align.process(frames)

        # Get aligned frames
        # aligned_depth_frame is a 640x480 depth image
        aligned_depth_frame = aligned_frames.get_depth_frame()
        color_frame = aligned_frames.get_color_frame()
        color_image = np.asanyarray(color_frame.get_data())

        depth_intrinsics = (
            color_frame.profile.as_video_stream_profile().intrinsics
        )  # 需要使用彩色相机的内参
        # Validate that both frames are valid
        if not aligned_depth_frame or not color_frame:
            pass
        depth_image = np.asanyarray(aligned_depth_frame.get_data())

        # Remove background - Set pixels further than clipping_distance to black
        bg_removed = np.where(
            (depth_image > clipping_distance) | (depth_image <= 0), 0, 255
        )
        bg_removed = np.asfarray(bg_removed).astype(np.uint8)

        # 获取每个盒子的坐标
        contours, _ = cv.findContours(
            bg_removed, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE
        )
        # print("processing : " + str(i)
    finally:
        lock = threading.Lock()
        test_imshow(bg_removed, save_img=True, name="bg_removed")
        boxes = []
        item_count = 0
        for c in contours:
            rect = cv.minAreaRect(c)
            # rect[0]:中心点坐标xy
            # rect[1]:width 与 height
            # rect[2]:角度
            if rect[1][0] > 450 and rect[1][1] > 450:

                item_count += 1

                lock.acquire()
                depth_l[1] = True  # 检测到盒子
                lock.release()

                box = cv.boxPoints(rect)
                box = np.int0(box)

                global recongnize_task
                if item_count != 1:
                    recongnize_task.join()

                if q.empty() or (item_count!=1):
                    # 识别任务,采用线程处理
                    recongnize_task = threading.Thread(
                        target=get_address_crop, args=(color_image, box, q,)
                    )
                    recongnize_task.start()

                coord = rs.rs2_deproject_pixel_to_point(
                    depth_intrinsics,
                    rect[0],
                    (depth_image[int(rect[0][1]), int(rect[0][0])]),
                )
                print(coord)
                boxes.append([coord, rect[2]])

    lock.acquire()
    depth_l[0] = True  # 得到深度值
    lock.release()

    return boxes


def pipeline_stop():
    global pipeline
    pipeline.stop()


def get_address_crop(ori_img, box, q):
    """
    从深度相机的colour图像中裁减盒子的图像以摆正,进行识别
    """
    global result_store
    # TODO:现在区分收件与寄件地址是通过大小进行的,可能存在潜在的错误事件
    true_result = []
    for angle in range(0, 361, 90):
        # print("angle = " + str(angle))
        img = crop_rotate_box(box, ori_img, angle=angle)
        test_imshow(img, save_img=True, name="angled")
        dt_boxes, rec_res, angle_list = paddle(img)
        txts = [rec_res[i][0] for i in range(len(rec_res))]
        dt_boxes, txts = sorted_boxes(dt_boxes, txts)  # 按面积大小排序
        results = txts[:2]
        print(txts[:2])
        flag = False
        for result in txts:
            if flag:
                break
            for value in province.values():
                if result.find(value) > -1:

                    true_result.append(value)
                    flag = True
                    break

        true_result.reverse()  # 防止误识别为上一个盒子
        if len(true_result) > 0:
            true_result = max(true_result)
            q.put(true_result)
            break
            # return true_result
        test_imshow(img, save_img=True)
    # return 'unknown'


def get_address():
    # TODO:解决收件地址和计件地址冲突的问题(现在利用分辨率较低的特性暂时解决问题)
    # TODO:识别存在误识别的可能
    # TODO:倾斜情况下几乎无法识别
    # TODO:识别的for循环会导致错误的情况
    profile = pipeline.start(config)
    results = []
    true_result = []
    i = 0
    try:
        while len(true_result) < 3 and i < 5:
            i += 1
            frames = pipeline.wait_for_frames()
            color_frame = frames.get_color_frame()
            color_image = np.asanyarray(color_frame.get_data())
            x, y = color_image.shape[1], color_image.shape[0]
            color_image = cv.copyMakeBorder(
                color_image,
                int(y / 4),
                int(y / 4),
                int(x / 4),
                int(x / 4),
                cv.BORDER_REPLICATE,
            )

            dt_boxes, rec_res, angle_list = paddle(color_image)
            txts = [rec_res[i][0] for i in range(len(rec_res))]
            dt_boxes, txts = sorted_boxes(dt_boxes, txts)
            results = txts[:4]
            print(txts[:4])
            flag = False
            for key in province:
                if flag:
                    break
                for result in results:
                    if result.find(province[key]) > -1:
                        true_result.append(province[key])
                        print(true_result)
                        flag = True
                        break
    finally:
        pipeline.stop()
    if len(true_result) > 0:
        true_result = max(true_result, key=true_result.count)
        print(true_result)
        return true_result
    else:
        test_imshow(color_image, save_img=True)
        return "unknown"


cap = cv.VideoCapture(6)  #  侧边摄像机端口固定为6
level = 0


def get_box_name():
    """
    对当前位置投递箱标签内容进行识别
    """

    # 识别过程
    for _ in range(30):
        _, frame = cap.read()
    # test_imshow(frame, save_img=True, name="box_name")
    dt_boxes, rec_res, angle_list = paddle(frame)

    # 取出结果
    txts = [rec_res[i][0] for i in range(len(rec_res))]
    true_result = ["" for i in range(0, 5)]

    # 将结果与省份列表一一比较配对
    for i, result in enumerate(txts):
        for value in province.values():
            if result.find(value) > -1:
                print(value)
                true_result[i] = value

    # count--对栈的每一层做个标记
    global level
    level += 1
    count = level

    # 若仅有一个结果则递归处理
    if true_result.count("") != 3:
        for i, result in enumerate(true_result):
            if result != "":
                if dt_boxes[i][0][0] < 300:
                    print("go forward a little...")
                    os.system(
                        ". ~/anaconda3/etc/profile.d/conda.sh && conda deactivate && conda deactivate && rosrun dashgo_tools move.py 0.05"
                    )
                    true_result = get_box_name()
                elif dt_boxes[i][0][0] > 390:
                    print("go backward a little...")
                    os.system(
                        ". ~/anaconda3/etc/profile.d/conda.sh && conda deactivate && conda deactivate && rosrun dashgo_tools move.py -0.05"
                    )

                    true_result = get_box_name()
    else:
        results = ["" for i in range(2)]
        for i, result in enumerate(true_result):
            if result != "":
                if dt_boxes[i][0][0] < 300:
                    results[0] = result
                else:
                    results[1] = result
        true_result = results
    # 只对栈顶结果进行处理
    if count == 1:
        while "" in true_result:
            true_result.remove("")
        print(true_result)

    level = 0
    return true_result


if __name__ == "__main__":
    # origin_img = cv.imread("./img/019.jpg")
    # get_address_new(origin_img,True)
    # get_address()
    get_boxes_info()
    # get_box_name()
