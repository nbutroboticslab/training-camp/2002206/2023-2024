from serial.tools import list_ports
from pydobotkeo.dobot import Dobot
import time
import math
import sys
#sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
port = list_ports.comports()[1].device
device = Dobot(port=port, verbose=False)
device._set_queued_cmd_clear()
device.clear_all_alarms_state()
print(device.pose())
