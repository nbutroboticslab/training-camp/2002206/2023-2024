from serial.tools import list_ports
import time

import sys
sys.path.append("..")
from pydobotkeo.dobot import Dobot

# port = list_ports.comports()[1].device
# for op in list_ports.comports():
#     print(op.device)
port="/dev/dobot_arm"
device = Dobot(port=port, verbose=False)
device._set_queued_cmd_clear()
device.clear_all_alarms_state()

time.sleep(2)
print("go_home")
device.go_home()
print("adjust 0")
device.rotate_to(-90.0,0.0,0.0,0.0,True)
print("adjust 1")
device.rotate_to(90.0,0.0,0.0,0.0,True)
print("move ")
device.move_to(0 , -200,0, -90, wait=True)