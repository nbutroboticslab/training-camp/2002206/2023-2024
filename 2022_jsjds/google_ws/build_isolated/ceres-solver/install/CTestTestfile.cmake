# CMake generated Testfile for 
# Source directory: /home/eaibot/2022_jsjds/google_ws/src/ceres-solver
# Build directory: /home/eaibot/2022_jsjds/google_ws/build_isolated/ceres-solver/install
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(internal/ceres)
subdirs(examples)
