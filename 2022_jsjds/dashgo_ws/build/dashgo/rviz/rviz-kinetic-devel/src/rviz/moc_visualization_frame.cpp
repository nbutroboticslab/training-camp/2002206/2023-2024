/****************************************************************************
** Meta object code from reading C++ file 'visualization_frame.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../../src/dashgo/rviz/rviz-kinetic-devel/src/rviz/visualization_frame.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'visualization_frame.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_rviz__VisualizationFrame_t {
    QByteArrayData data[46];
    char stringdata0[621];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_rviz__VisualizationFrame_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_rviz__VisualizationFrame_t qt_meta_stringdata_rviz__VisualizationFrame = {
    {
QT_MOC_LITERAL(0, 0, 24), // "rviz::VisualizationFrame"
QT_MOC_LITERAL(1, 25, 12), // "statusUpdate"
QT_MOC_LITERAL(2, 38, 0), // ""
QT_MOC_LITERAL(3, 39, 7), // "message"
QT_MOC_LITERAL(4, 47, 16), // "fullScreenChange"
QT_MOC_LITERAL(5, 64, 6), // "hidden"
QT_MOC_LITERAL(6, 71, 24), // "setDisplayConfigModified"
QT_MOC_LITERAL(7, 96, 9), // "setStatus"
QT_MOC_LITERAL(8, 106, 6), // "onOpen"
QT_MOC_LITERAL(9, 113, 6), // "onSave"
QT_MOC_LITERAL(10, 120, 8), // "onSaveAs"
QT_MOC_LITERAL(11, 129, 11), // "onSaveImage"
QT_MOC_LITERAL(12, 141, 22), // "onRecentConfigSelected"
QT_MOC_LITERAL(13, 164, 10), // "onHelpWiki"
QT_MOC_LITERAL(14, 175, 11), // "onHelpAbout"
QT_MOC_LITERAL(15, 187, 18), // "openNewPanelDialog"
QT_MOC_LITERAL(16, 206, 17), // "openNewToolDialog"
QT_MOC_LITERAL(17, 224, 13), // "showHelpPanel"
QT_MOC_LITERAL(18, 238, 17), // "onDockPanelChange"
QT_MOC_LITERAL(19, 256, 19), // "onToolbarRemoveTool"
QT_MOC_LITERAL(20, 276, 8), // "QAction*"
QT_MOC_LITERAL(21, 285, 23), // "remove_tool_menu_action"
QT_MOC_LITERAL(22, 309, 24), // "onToolbarActionTriggered"
QT_MOC_LITERAL(23, 334, 6), // "action"
QT_MOC_LITERAL(24, 341, 7), // "addTool"
QT_MOC_LITERAL(25, 349, 5), // "Tool*"
QT_MOC_LITERAL(26, 355, 4), // "tool"
QT_MOC_LITERAL(27, 360, 10), // "removeTool"
QT_MOC_LITERAL(28, 371, 11), // "refreshTool"
QT_MOC_LITERAL(29, 383, 21), // "indicateToolIsCurrent"
QT_MOC_LITERAL(30, 405, 12), // "changeMaster"
QT_MOC_LITERAL(31, 418, 13), // "onDeletePanel"
QT_MOC_LITERAL(32, 432, 13), // "setFullScreen"
QT_MOC_LITERAL(33, 446, 11), // "full_screen"
QT_MOC_LITERAL(34, 458, 14), // "exitFullScreen"
QT_MOC_LITERAL(35, 473, 15), // "markLoadingDone"
QT_MOC_LITERAL(36, 489, 21), // "setImageSaveDirectory"
QT_MOC_LITERAL(37, 511, 9), // "directory"
QT_MOC_LITERAL(38, 521, 5), // "reset"
QT_MOC_LITERAL(39, 527, 15), // "onHelpDestroyed"
QT_MOC_LITERAL(40, 543, 12), // "hideLeftDock"
QT_MOC_LITERAL(41, 556, 4), // "hide"
QT_MOC_LITERAL(42, 561, 13), // "hideRightDock"
QT_MOC_LITERAL(43, 575, 27), // "onDockPanelVisibilityChange"
QT_MOC_LITERAL(44, 603, 7), // "visible"
QT_MOC_LITERAL(45, 611, 9) // "updateFps"

    },
    "rviz::VisualizationFrame\0statusUpdate\0"
    "\0message\0fullScreenChange\0hidden\0"
    "setDisplayConfigModified\0setStatus\0"
    "onOpen\0onSave\0onSaveAs\0onSaveImage\0"
    "onRecentConfigSelected\0onHelpWiki\0"
    "onHelpAbout\0openNewPanelDialog\0"
    "openNewToolDialog\0showHelpPanel\0"
    "onDockPanelChange\0onToolbarRemoveTool\0"
    "QAction*\0remove_tool_menu_action\0"
    "onToolbarActionTriggered\0action\0addTool\0"
    "Tool*\0tool\0removeTool\0refreshTool\0"
    "indicateToolIsCurrent\0changeMaster\0"
    "onDeletePanel\0setFullScreen\0full_screen\0"
    "exitFullScreen\0markLoadingDone\0"
    "setImageSaveDirectory\0directory\0reset\0"
    "onHelpDestroyed\0hideLeftDock\0hide\0"
    "hideRightDock\0onDockPanelVisibilityChange\0"
    "visible\0updateFps"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_rviz__VisualizationFrame[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      33,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  179,    2, 0x06 /* Public */,
       4,    1,  182,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    0,  185,    2, 0x0a /* Public */,
       7,    1,  186,    2, 0x0a /* Public */,
       8,    0,  189,    2, 0x09 /* Protected */,
       9,    0,  190,    2, 0x09 /* Protected */,
      10,    0,  191,    2, 0x09 /* Protected */,
      11,    0,  192,    2, 0x09 /* Protected */,
      12,    0,  193,    2, 0x09 /* Protected */,
      13,    0,  194,    2, 0x09 /* Protected */,
      14,    0,  195,    2, 0x09 /* Protected */,
      15,    0,  196,    2, 0x09 /* Protected */,
      16,    0,  197,    2, 0x09 /* Protected */,
      17,    0,  198,    2, 0x09 /* Protected */,
      18,    0,  199,    2, 0x09 /* Protected */,
      19,    1,  200,    2, 0x09 /* Protected */,
      22,    1,  203,    2, 0x09 /* Protected */,
      24,    1,  206,    2, 0x09 /* Protected */,
      27,    1,  209,    2, 0x09 /* Protected */,
      28,    1,  212,    2, 0x09 /* Protected */,
      29,    1,  215,    2, 0x09 /* Protected */,
      30,    0,  218,    2, 0x09 /* Protected */,
      31,    0,  219,    2, 0x09 /* Protected */,
      32,    1,  220,    2, 0x09 /* Protected */,
      34,    0,  223,    2, 0x09 /* Protected */,
      35,    0,  224,    2, 0x09 /* Protected */,
      36,    1,  225,    2, 0x09 /* Protected */,
      38,    0,  228,    2, 0x09 /* Protected */,
      39,    0,  229,    2, 0x09 /* Protected */,
      40,    1,  230,    2, 0x09 /* Protected */,
      42,    1,  233,    2, 0x09 /* Protected */,
      43,    1,  236,    2, 0x09 /* Protected */,
      45,    0,  239,    2, 0x09 /* Protected */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::Bool,    5,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 20,   21,
    QMetaType::Void, 0x80000000 | 20,   23,
    QMetaType::Void, 0x80000000 | 25,   26,
    QMetaType::Void, 0x80000000 | 25,   26,
    QMetaType::Void, 0x80000000 | 25,   26,
    QMetaType::Void, 0x80000000 | 25,   26,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   33,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   37,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   41,
    QMetaType::Void, QMetaType::Bool,   41,
    QMetaType::Void, QMetaType::Bool,   44,
    QMetaType::Void,

       0        // eod
};

void rviz::VisualizationFrame::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        VisualizationFrame *_t = static_cast<VisualizationFrame *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->statusUpdate((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->fullScreenChange((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->setDisplayConfigModified(); break;
        case 3: _t->setStatus((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->onOpen(); break;
        case 5: _t->onSave(); break;
        case 6: _t->onSaveAs(); break;
        case 7: _t->onSaveImage(); break;
        case 8: _t->onRecentConfigSelected(); break;
        case 9: _t->onHelpWiki(); break;
        case 10: _t->onHelpAbout(); break;
        case 11: _t->openNewPanelDialog(); break;
        case 12: _t->openNewToolDialog(); break;
        case 13: _t->showHelpPanel(); break;
        case 14: _t->onDockPanelChange(); break;
        case 15: _t->onToolbarRemoveTool((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 16: _t->onToolbarActionTriggered((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 17: _t->addTool((*reinterpret_cast< Tool*(*)>(_a[1]))); break;
        case 18: _t->removeTool((*reinterpret_cast< Tool*(*)>(_a[1]))); break;
        case 19: _t->refreshTool((*reinterpret_cast< Tool*(*)>(_a[1]))); break;
        case 20: _t->indicateToolIsCurrent((*reinterpret_cast< Tool*(*)>(_a[1]))); break;
        case 21: _t->changeMaster(); break;
        case 22: _t->onDeletePanel(); break;
        case 23: _t->setFullScreen((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 24: _t->exitFullScreen(); break;
        case 25: _t->markLoadingDone(); break;
        case 26: _t->setImageSaveDirectory((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 27: _t->reset(); break;
        case 28: _t->onHelpDestroyed(); break;
        case 29: _t->hideLeftDock((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 30: _t->hideRightDock((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 31: _t->onDockPanelVisibilityChange((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 32: _t->updateFps(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (VisualizationFrame::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&VisualizationFrame::statusUpdate)) {
                *result = 0;
            }
        }
        {
            typedef void (VisualizationFrame::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&VisualizationFrame::fullScreenChange)) {
                *result = 1;
            }
        }
    }
}

const QMetaObject rviz::VisualizationFrame::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_rviz__VisualizationFrame.data,
      qt_meta_data_rviz__VisualizationFrame,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *rviz::VisualizationFrame::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *rviz::VisualizationFrame::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_rviz__VisualizationFrame.stringdata0))
        return static_cast<void*>(const_cast< VisualizationFrame*>(this));
    if (!strcmp(_clname, "WindowManagerInterface"))
        return static_cast< WindowManagerInterface*>(const_cast< VisualizationFrame*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int rviz::VisualizationFrame::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 33)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 33;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 33)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 33;
    }
    return _id;
}

// SIGNAL 0
void rviz::VisualizationFrame::statusUpdate(const QString & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void rviz::VisualizationFrame::fullScreenChange(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
