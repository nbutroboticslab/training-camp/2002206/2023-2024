set(_CATKIN_CURRENT_PACKAGE "move_to_grab")
set(move_to_grab_VERSION "0.0.0")
set(move_to_grab_MAINTAINER "feibot <feibot@todo.todo>")
set(move_to_grab_PACKAGE_FORMAT "2")
set(move_to_grab_BUILD_DEPENDS "roscpp" "rviz" "std_msgs" "geometry_msgs" "message_generation" "message_runtime")
set(move_to_grab_BUILD_EXPORT_DEPENDS "roscpp" "rviz" "std_msgs" "geometry_msgs" "message_generation" "message_runtime")
set(move_to_grab_BUILDTOOL_DEPENDS "catkin")
set(move_to_grab_BUILDTOOL_EXPORT_DEPENDS )
set(move_to_grab_EXEC_DEPENDS "roscpp" "rviz" "std_msgs" "geometry_msgs" "message_runtime")
set(move_to_grab_RUN_DEPENDS "roscpp" "rviz" "std_msgs" "geometry_msgs" "message_runtime" "message_generation")
set(move_to_grab_TEST_DEPENDS )
set(move_to_grab_DOC_DEPENDS )
set(move_to_grab_URL_WEBSITE "")
set(move_to_grab_URL_BUGTRACKER "")
set(move_to_grab_URL_REPOSITORY "")
set(move_to_grab_DEPRECATED "")