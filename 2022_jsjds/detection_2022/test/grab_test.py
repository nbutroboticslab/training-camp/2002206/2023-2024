'''
Author: Meroke
Date: 2022-07-01 21:03:28
LastEditors: Meroke
LastEditTime: 2022-07-01 21:21:11
Description: file content
FilePath: /2022_jsjds/detection_2022/test/grab_test.py
'''
from serial.tools import list_ports
import sys
sys.path.append('..')
from pydobot.dobot import Dobot
import time



def print_pose():
    (x, y, z, r, j1, j2, j3, j4) = device.pose()
    print(f'x:{x} y:{y} z:{z} j1:{j1} j2:{j2} j3:{j3} j4:{j4}')

port = list_ports.comports()[0].device
device = Dobot(port=port, verbose=False)
device._set_queued_cmd_clear()
device.clear_all_alarms_state()
device.rotate_to(-90, 0, 0, 0, wait=True) # 机械臂初始化归位
device.go_home()
# device.move_to(-191 ,0, 172, 0, wait=True)
# while(1):
# (x, y, z, r, j1, j2, j3, j4) = device.pose()
# device
# device.move_to(191 ,0, 172, 0, wait=False)
# device.suck(0)
# print(device.pose()[:])
# print(f'x:{x} y:{y} z:{z}')
base_x = 200
grab_position_adjust = 27
grab_x = 100 + grab_position_adjust + base_x
grab_y = 100
grab_z = 150#-31

while 1:
    print_pose()

(x, y, z, r, j1, j2, j3, j4) = device.pose()
# device.go_grab_home()  # 200, 0
# device.move_to(300 , -83, grab_z, r, wait=True) #first box relative -101,247


# Home 260.440, 0.0, -8.5498 
# Left  89.3715 ,-252.409, -38.961
# Middle  -18 ,-245.656 , -38.354

# store -4.5543, 201.145 ,42.769
#        203.029 , 29.0021 ,60.5831, 0.0
# for i in range(2):
device.move_to(89.3715 ,-252.409,  -38.961, r, wait=True) # 移向抓取目标
device.suck(True)  # 吸取
device.rotate_to(-90 ,0, 0 ,0, wait=True) # 抬升
time.sleep(0.3)
device.rotate_to(90, 30, 45 ,0.0 ,  wait=True) # 回到存储区
device.suck(False) # 气泵停止
device.rotate_to(-90, 0,0 ,0.0 ,  wait=True) # 回到分拣区

device.move_to( -18 ,-245.656 , -38.354, r, wait=True)
device.suck(True)
device.rotate_to(-90 ,0, 0 ,0, wait=True) # 抬升
time.sleep(0.3)
device.rotate_to(90, 30, 45 ,0.0 ,  wait=True) # 放下
device.suck(False)
device.rotate_to(-90, 0,0 ,0.0 ,  wait=True)

    # device.mvoe_to()
    # device.suck(False)
    
    # device.suck(True)
    # time.sleep(1)
    # device.go_grab_home()
    # time.sleep(0.5)
    # device.move_to(13.063 ,-254.1425 , -47.449, r, wait=True)
    # device.suck(True)
    # device.move_to(13.063 ,-254.1425 , 70, r, wait=True)
    # time.sleep(0.5)
    # device.rotate_to(-90 , 0.0,0.0 ,0.0 ,  wait=True)
    # device.suck(False)


# device.move_to(80 , 200, 150, r, wait=True)
# device.move_to(180 , 180, 150, r, wait=True)
# time.sleep(3)
# device.go_grab_home()
# device.move_to(grab_x, grab_y, grab_z, r, wait=True)
# device.move_to(300 , -83, , r, wait=True)
# device.suck(False)
# device.go_grab_home()
# device.move_to(x, y, z, r, wait=True)  # we wait until this movement is done before continuing

device.close()
