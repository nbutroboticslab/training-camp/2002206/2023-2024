
from IPython import get_ipython

import os
import tensorflow as tf
import cv2 as cv
import numpy as np
import sys
#sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
from util.utils_cv import test_imshow,find_box,crop_rotate_box,rotate_img
from util.grab import *
from util.keys_prov import *
from util.utils_recg import *
from angle.predict import predict as angle_detect
from ctpn.text_detect import text_detect
from imutils import perspective

import matplotlib.pyplot as plt

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

# get_ipython().run_line_magic('matplotlib', 'inline')
# plt.rcParams['figure.figsize'] = (16.0,9.0) # set default size of plots
# plt.rcParams['figure.dpi']=300
# plt.rcParams['image.interpolation'] = 'nearest'
# plt.rcParams['image.cmap'] = 'gray'


def camera_activate(show_img):
    cap = cv.VideoCapture(0)
    i = 1
    while(i<40):
        _,origin_img = cap.read()
        i+=1
    hsv = cv.cvtColor(origin_img, cv.COLOR_BGR2HSV)
    lower_red = np.array([0,60, 100])
    upper_red = np.array([10, 255, 255])
    mask = cv.inRange(hsv, lower_red, upper_red)
    test_imshow(mask,cv_show=False)


    gray_img = cv.cvtColor(origin_img, cv.COLOR_BGR2GRAY)
    # line_img = cv.Canny(gray_img, 100, 200, apertureSize=5)
    # lines = cv.HoughLinesP(line_img, 1.5, np.pi/180, 78,
    #                            minLineLength=65, maxLineGap=130)
    # test_imshow(line_img,cv_show=True)
    # blur_img = cv.GaussianBlur(gray_img, (7, 7), 1)
    # _, thresh_img = cv.threshold(
    #     gray_img, 0, 255, cv.THRESH_BINARY+cv.THRESH_OTSU)
    _, thresh_img = cv.threshold(
        gray_img, 190, 255, cv.THRESH_BINARY)
    thresh_img = cv.bitwise_or(thresh_img,mask)
    # test_imshow(thresh_img, cv_show=False)
    # blur_img = cv.erode(thresh_img,(31,31),6)
    blur_img = cv.medianBlur(thresh_img,1)
    blur_img = cv.dilate(blur_img, (7, 7), 1)
    # test_imshow(blur_img)


    img,contours,_= cv.findContours(blur_img, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
    # contours = contours[::-1]
    labels =[]
    test_img = origin_img.copy()
    boxes,positions,angles = find_box(contours)
    for box in boxes:
        cv.drawContours(test_img,[box],0,(0,0,255),3)
        labels.append(crop_rotate_box(box,origin_img))

    # test_imshow(test_img, cv_show=False)
    if show_img :
        test_imshow(origin_img,cv_show=False)
        test_imshow(blur_img,cv_show=False)
        test_imshow(test_img,cv_show=False)
        # test_imshow(,cv_show=False)
    # print(positions)
    relative_positions_to_grab = []
    scale_positions = []
    y,x = origin_img.shape[:2]
    for position in positions:
        # print(position,x/2,y/2)
        relative_y = (position[1]-y/2)/y*200
        relative_x = -(position[0]-x/2)/x*200
        relative_positions_to_grab.append([relative_x,relative_y])
        # scale_positions.append([(position[0]-x/2)/x*100,(position[1]-y/2)/y*100])
    # print(relative_positions)
    # print(relative_positions_to_grab)
    # # print(scale_positions)
    # print(test_img.shape)



    for label in labels:
        if angle_detect(img=label)==180:
            label = rotate_img(label)
        # test_imshow(label,cv_show=False)
        y1=(int)(0.15*len(label[0][:]))
        y2=(int)(0.36*len(label[0][:]))
        label = label[:][y1:y2][:]
        # test_imshow(label,cv_show=False)
        text_recs,temp,img = text_detect(label)
        # test_imshow(temp,cv_show=False)
        text_recs = sort_box(text_recs)[0:1]
        # print(text_recs)
        result = crnnRec(img,text_recs,adjust=True)
        for key in result:
            print(result[key][1])
    return relative_positions_to_grab,angles

