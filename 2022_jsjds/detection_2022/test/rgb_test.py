'''
Author: Meroke
Date: 2022-06-30 20:39:52
LastEditors: Meroke
LastEditTime: 2022-07-05 18:36:19
Description: file content
FilePath: /2022_jsjds/detection_2022/test/rgb_test.py
'''
import sys
try:
    sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
except:
    pass
import cv2 #导入opencv包
import numpy as np #导入numpy 取别名np

# # 4,6 可打开
# name = "/dev/rgb_camera"

# for index in range(10):
#     # cap = cv2.VideoCapture(name)
#     cap = cv2.VideoCapture(index)
#     print(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
#     if cap.isOpened():
#         for i in range(50):
#             success, image = cap.read()  # 图片读进来是BGR
#             if not success:
#                 print("摄像头被占用！")
#                 break
#         # print(name + str(index) + " open")
#         cv2.imwrite("./rgb_test{}.jpg".format(index),image)
#         # cv2.imwrite("./rgb_test.jpg",image)
#     else:
#         print(name + str(index) + " open failed")
#         # print(" open failed")
# cap.release()

# 4,6 可打开
# name = "/dev/video6"

def catch_img2(cap):
    print(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    if cap.isOpened():
        for i in range(50):
            success, image = cap.read()  # 图片读进来是BGR
            if not success:
                print("摄像头被占用！")
                break
        # print(name + str(index) + " open")
        # show("img",image)
        cv2.imwrite("./rgb_test.jpg",image)
    else:
        print(" open failed")
    cap.release()

def catch_img(cap):
    i = 0
    while True:
        success, image = cap.read()  # 图片读进来是BGR
        if not success:
            print("摄像头被占用！")
            break
        # print(name + str(index) + " open")
        k = cv2.waitKey(1) & 0xFF
        if k == ord('q'):
            break
        elif k == ord('s'):
            cv2.imwrite("{}.jpg".format(i),image)
            i+=1
        cv2.imshow("img",image)
        # cv2.imwrite("./rgb_test.jpg",image)
    else:
        print(" open failed")
    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    # port = "/dev/video2"
    # cap = cv2.VideoCapture(port)
    # print(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    # catch_img2(cap)
    img = cv2.imread("/home/meroke/code/detection_2022/ocr/image/test/1.jpg")
    img = cv2.resize(img,(1080,720))
    cv2.imshow("img",img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
