#-*- coding: UTF-8 -*- 
#!/usr/bin/python
import sys
import os
import sys
import rospy
import time

# 移除ros默认python路径避免python版本错误
os.system("rm -rf ./images/results/*")
sys.path.remove("/opt/ros/kinetic/lib/python2.7/dist-packages")
sys.path.insert(0, "/home/eaibot/anaconda3/envs/pac_2021/lib/python3.6/site-packages")
# from pydobot.dobot import Dobot # 机械臂
sys.path.append("../")
from ros.navigation import Navigation # 导航
# from ros.slide import Slide # 平移
# from cv_2021 import *
# from drop_2021 import *
# from grab_2021 import *
# from util.utils_cv import *
# from util.utils_process import *
import threading
from threading import Thread


navigation = Navigation()
navigation.send_goal("middle")
time.sleep(1)