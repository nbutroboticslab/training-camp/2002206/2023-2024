#! /usr/bin/env python
import rospy
from std_msgs.msg import String
# import time
from geometry_msgs.msg import Twist
from threading import Thread
from log.pylog import log
# from ocr.log.pylog import log
LOGS_INFO = log.info





class laser_dis:
    def __init__(self):
        self.dis = 0
    def set_dis(self,distance):
        self.dis = distance
    def get_dis(self):
        return self.dis

single_laser = laser_dis()


def doMsg(msg):
    global single_laser
    single_laser.set_dis(float(msg.data))
    # distance_real=float(msg.data)
    # # print(distance_real)
    # print(single_laser.dis)

def judge():
    laser_dis = single_laser.get_dis()
    print(laser_dis)
    if laser_dis>0:
        print("111")
    else:
        print("222")

def forwrd_p(cmd_vel,move_cmd):
    rate = 50
    r = rospy.Rate(rate)
    while True:
        if move_cmd.linear.x >= -0.01:
            break
        cmd_vel.publish(move_cmd)
        r.sleep()

def adopt_forward(speed=-0.30,target_dis=0.2):
    global single_laser
    cmd_vel = rospy.Publisher('/cmd_vel', Twist, queue_size=5)
    move_cmd = Twist()
    move_cmd.linear.x = speed
    laser_dis = single_laser.get_dis()
    # print(laser_dis)
    init_minus_val = laser_dis - target_dis
    forward_thread = Thread(target=forwrd_p,args=[cmd_vel,move_cmd])
    forward_thread.start()
    while laser_dis > target_dis :
        rospy.loginfo('distance of the obstacle : %f', laser_dis)
        laser_dis = single_laser.get_dis()
        if speed < -0.05:
            speed = -0.05
            minus_val = laser_dis - target_dis
            speed *= minus_val/init_minus_val 
            LOGS_INFO("distance of the obstacle : {}".format(laser_dis))

        move_cmd.linear.x = speed 
        # rospy.loginfo("speed:{} ".format(move_cmd.linear.x))
        if speed > -0.02:
            break
        rospy.sleep(0.05)
    move_cmd.linear.x = 0
    cmd_vel.publish(Twist())
    if forward_thread.isAlive():
        forward_thread.join()

def subscribe_main():
    # rospy.init_node("subscribe_single_laser")
    sub = rospy.Subscriber("chatter",String,doMsg,queue_size=10)
    rospy.spin()


if __name__ == "__main__":
    rospy.init_node("subscribe_single_laser")
    sub = rospy.Subscriber("chatter",String,doMsg,queue_size=2)
    # print(single_laser2.dis)
    # print(single_laser)
    rospy.spin()
    # print(single_laser2.dis)