#!/usr/bin/env python3
#-*- coding: UTF-8 -*- 
from sys import argv
import sys
sys.path.insert(0, "/home/eaibot/anaconda3/envs/pac_2021/lib/python3.6/site-packages")
from actionlib_msgs.msg import *
from geometry_msgs.msg import Pose, Point, Quaternion, Twist
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from math import sin, cos
import math
import rospy
import actionlib
import sys

# 欧拉数转四元数
def euler_to_quaternion(yaw, pitch, roll):
    qx = sin(roll / 2) * cos(pitch / 2) * cos(yaw / 2) - cos(roll / 2) * sin(
        pitch / 2
    ) * sin(yaw / 2)
    qy = cos(roll / 2) * sin(pitch / 2) * cos(yaw / 2) + sin(roll / 2) * cos(
        pitch / 2
    ) * sin(yaw / 2)
    qz = cos(roll / 2) * cos(pitch / 2) * sin(yaw / 2) - sin(roll / 2) * sin(
        pitch / 2
    ) * cos(yaw / 2)
    qw = cos(roll / 2) * cos(pitch / 2) * cos(yaw / 2) + sin(roll / 2) * sin(
        pitch / 2
    ) * sin(yaw / 2)

    return [qx, qy, qz, qw]

# 截断函数
def trunc(f, n):
    slen = len("%.*f" % (n, f))
    return float(str(f)[:slen])

class Navigation:
    """导航类"""
    def affect(self, coordinate):
        """投递点到map坐标系的变换"""
        coordinate[0] -= 0.67
        coordinate[1] -= 0.79

    def __init__(self):
        self.succeed = False
        self.locations = dict()

        #####################
        # 准备阶段
        #####################

        # # ros相关节点设置
        # disable_signals=True,否则ros会单独处理中断信号,而导致无法使得process处理键盘中断信号
        # rospy.init_node("navigation", anonymous=True,disable_signals=True)

        # 调用move_base前的准备工作
        self.move_base_prepare()

        # 初始化各点坐标
        self.coordinates_init()

        # 初始化各点位姿
        self.locations_init()

        # 确保有初始位置
        # while self.initial_pose.header.stamp == "":
        #     rospy.sleep(1)


    def update_initial_pose(self, initial_pose):
        self.initial_pose = initial_pose

    def shutdown(self):
        rospy.loginfo("Stopping the robot...")
        if not self.succeed:
            self.move_base.cancel_goal()
        # rospy.sleep(2)
        # self.cmd_vel_pub.publish(Twist())


    '''
    x <-----| (0,0)
            |
            |
            |
            v y
    以小车初始点为（0，0）

    |  -90度    ^  90度      0度 
    |           |           <-------
    |           |
    v           |

    '''
    def coordinates_init(self):

        #######################################
        #   导航过程中各点的绝对坐标
        #######################################

        # 各投递点初始坐标
        self.coordinates = list()

        #每个分组第一个坐标为左侧箱子

        self.coordinates.append([3.645, -0.175])  # p0 #左上角   3.665, -0.173
        self.coordinates.append([3.020, -0.160])  # p1  3.023, -0.260

        self.coordinates.append([1.970, -0.175])  # p2 #中间  1.948, -0.280
        self.coordinates.append([1.365, -0.185])  # p3   1.357, -0.270

        self.coordinates.append([0.340, -0.185])  # p4     0.347, -0.357
        self.coordinates.append([-0.310,-0.175])  # p5  #右上角-0.284,-0.345

        self.coordinates.append([-0.300, 0.870])  # p6 # 右下角   -0.254, 0.951

        self.coordinates.append([3.6150, 0.870])  # p7 #左下角  3.609, 0.951


    
        # 识别点
        # self.coordinates.append([0.9251, 1.325])  # r0: 0 1
        # self.coordinates.append([0.925, 1.325])  # r4: 8 9
        # self.coordinates.append([2.500, 1.325])  # r3: 6 7
        # self.coordinates.append([4.1701, 1.325])  # r2: 4 5
        # self.coordinates.append([4.170, 1.325])  # r1: 2 3
        

        # 抓取点 1.5 ~ 3.5
        self.coordinates.append([0.45, 1.20])  # g0 *
        self.coordinates.append([0.80, 1.20])  # g1
        self.coordinates.append([1.15, 1.20])  # g2
        self.coordinates.append([1.50, 1.20])  # g3
        self.coordinates.append([1.85, 1.20])  # g4
        self.coordinates.append([2.2, 1.20])  # g5
        self.coordinates.append([2.55, 1.43])  # g6 *

        # self.coordinates.append([3.10, 1.95])    # start
        self.coordinates.append([0, 0])    # start
        self.coordinates.append([1.573,0.9])    # middle

        # 对每个绝对坐标逐个施加变换,使之变成map坐标系下的坐标
        # for i in self.coordinates:
        #     self.affect(i)

    def locations_init(self):

        #######################################
        #   导航过程中各点的绝对位姿
        #######################################

        # 0~5投递点位姿确定
        for i in range(6):
            self.locations["p" + str(i)] = Pose(
                Point(self.coordinates[i][0], self.coordinates[i][1], 0.000),
                Quaternion(
                    0.000,
                    0.000,
                    euler_to_quaternion(math.radians(90), 0, 0)[2],
                    euler_to_quaternion(math.radians(90), 0, 0)[3],
                ),
            )
        # 6~7投递点位姿确定
        for i in range(6,8):
            self.locations["p" + str(i)] = Pose(
                Point(self.coordinates[i][0], self.coordinates[i][1], 0.000),
                Quaternion(
                    0.000,
                    0.000,
                    euler_to_quaternion(math.radians(-90), 0, 0)[2],
                    euler_to_quaternion(math.radians(-90), 0, 0)[3],
                ),
            )
      
        # 0识别点位姿确定
        # for i in range(1):
        #     self.locations["r" + str(i)] = Pose(
        #         Point(self.coordinates[i + 10][0], self.coordinates[i + 10][1], 0.000),
        #         Quaternion(
        #             0.000,
        #             0.000,
        #             euler_to_quaternion(math.radians(90), 0, 0)[2],
        #             euler_to_quaternion(math.radians(90), 0, 0)[3],
        #         ),
        #     )
        # # 1~3识别点位姿确定
        # for i in range(1,4):
        #     self.locations["r" + str(i)] = Pose(
        #         Point(self.coordinates[i + 10][0], self.coordinates[i + 10][1], 0.000),
        #         Quaternion(
        #             0.000,
        #             0.000,
        #             euler_to_quaternion(math.radians(-90), 0, 0)[2],
        #             euler_to_quaternion(math.radians(-90), 0, 0)[3],
        #         ),
        #     )
        # # 4识别点位姿确定
        # for i in range(4,5):
        #     self.locations["r" + str(i)] = Pose(
        #         Point(self.coordinates[i + 10][0], self.coordinates[i + 10][1], 0.000),
        #         Quaternion(
        #             0.000,
        #             0.000,
        #             euler_to_quaternion(math.radians(90), 0, 0)[2],
        #             euler_to_quaternion(math.radians(90), 0, 0)[3],
        #         ),
        #     )

        # 0~6抓取点位姿确定
        for i in range(7):
            self.locations["g" + str(i)] = Pose(
                Point(self.coordinates[i + 8][0], self.coordinates[i + 8][1], 0.000),
                Quaternion(
                    0.000,
                    0.000,
                    euler_to_quaternion(math.radians(90), 0, 0)[2],
                    euler_to_quaternion(math.radians(90), 0, 0)[3],
                ),
            )
            # print("g"+str(i))
            # print(self.coordinates[i + 14][0])
            # print(self.coordinates[i + 14][1])
        # 开始点
        for i in range(1):
            self.locations["start"] = Pose(
                Point(self.coordinates[i + 15][0], self.coordinates[i + 15][1], 0.000),
                Quaternion(
                    0.000,
                    0.000,
                    euler_to_quaternion(math.radians(0), 0, 0)[2],  # 机械臂靠着分捡台
                    euler_to_quaternion(math.radians(0), 0, 0)[3],
                ),
            )
        for i in range(1):
            self.locations["middle"] = Pose(
                Point(self.coordinates[i + 16][0], self.coordinates[i + 16][1], 0.000),
                Quaternion(
                    0.000,
                    0.000,
                    euler_to_quaternion(math.radians(90), 0, 0)[2],
                    euler_to_quaternion(math.radians(90), 0, 0)[3],
                ),
            )
 
    def send_position(self,position,angle=90):
                # 到达目标时的状态
        goal_states = [
            "PENDING",
            "ACTIVE",
            "PREEMPTED",
            "SUCCEEDED",
            "ABORTED",
            "REJECTED",
            "PREEMPTING",
            "RECALLING",
            "RECALLED",
            "LOST",
        ]
        location = Pose(
            Point(position[0], position[1], 0.000),
            Quaternion(
                0.000,
                0.000,
                euler_to_quaternion(math.radians(angle), 0, 0)[2],
                euler_to_quaternion(math.radians(angle), 0, 0)[3],
            ),
        )
        # 决定要前往的位置
        # location = self.locations[target]

        # 设定当前目标点
        self.goal = MoveBaseGoal()
        self.goal.target_pose.pose = location
        self.goal.target_pose.header.frame_id = "map"
        self.goal.target_pose.header.stamp = rospy.Time.now()
        self.succeed = False

        # 向当前位置进发
        self.move_base.send_goal(self.goal)

        # 五分钟时间限制
        finished_within_time = self.move_base.wait_for_result(rospy.Duration(1200))

        # 查看是否成功到达
        if not finished_within_time:
            self.move_base.cancel_goal()
            rospy.loginfo("Timed out achieving goal")
        else:
            state = self.move_base.get_state()
            if state == GoalStatus.SUCCEEDED:
                rospy.loginfo("Goal succeeded!")
                self.succeed=True
            else:
                rospy.loginfo("Goal failed with error code:" + str(goal_states[state]))
                self.send_position([position[0],position[1] + 0.2] ,angle)
        

    def send_goal(self,target):

        """
        前往目标点:
        p(投递点):0-9
        g(抓取点):0-6
        r(投递盒识别点):0-4
        start: 起始点
        middle: 中点(定位用)
        """

        # 到达目标时的状态
        goal_states = [
            "PENDING",
            "ACTIVE",
            "PREEMPTED",
            "SUCCEEDED",
            "ABORTED",
            "REJECTED",
            "PREEMPTING",
            "RECALLING",
            "RECALLED",
            "LOST",
        ]

        # 决定要前往的位置
        location = self.locations[target]

        # 设定当前目标点
        self.goal = MoveBaseGoal()
        self.goal.target_pose.pose = location
        self.goal.target_pose.header.frame_id = "map"
        self.goal.target_pose.header.stamp = rospy.Time.now()
        self.succeed = False

        # 向当前位置进发
        self.move_base.send_goal(self.goal)

        # 五分钟时间限制
        finished_within_time = self.move_base.wait_for_result(rospy.Duration(1200))

        # 查看是否成功到达
        if not finished_within_time:
            self.move_base.cancel_goal()
            rospy.loginfo("Timed out achieving goal")
        else:
            state = self.move_base.get_state()
            if state == GoalStatus.SUCCEEDED:
                rospy.loginfo("Goal succeeded!")
                self.succeed=True
            else:
                rospy.loginfo("Goal failed with error code: " + str(goal_states[state]))
                # co.forward(-0.3)
                self.move_base.send_goal(self.goal)

    def move_base_prepare(self):

        # 订阅move_base服务器的消息
        self.move_base = actionlib.SimpleActionClient("move_base", MoveBaseAction)

        rospy.loginfo("Waiting for move_base action server...")

        # 60s等待时间限制
        self.move_base.wait_for_server(rospy.Duration(60))

# 退出时的回调函数
def shutdown():
    rospy.loginfo("navigation finished")

if __name__ == "__main__":
    try:
        rospy.init_node("navigation_demo")
        na = Navigation()
        na.send_goal("start")
        # na.send_goal("middle")
        # na.send_goal("start")

        start_pos = 0.77
        end_pos = 2.47
        mius = abs(start_pos - end_pos) / 10
        count = 0
        
        while(start_pos < end_pos):
            target = 'p' + str(count)
            select_plat_position = [start_pos,1.43]
            if(end_pos - start_pos < mius):
                break
            start_pos += mius
            rospy.loginfo("select_plat target: (%s,%s)",
                format(select_plat_position[0],'.3f'),
                format(select_plat_position[1],'.3f'))
            na.send_position(select_plat_position)
            rospy.loginfo("main target: %s",target)
            na.send_goal(target)
            count += 1


        # na.send_goal('p6')
        # na.send_goal('p7')
        na.send_goal("start")

        # rospy.on_shutdown(shutdown)

    except rospy.ROSInterruptException:
        rospy.loginfo("Random navigation finished.")