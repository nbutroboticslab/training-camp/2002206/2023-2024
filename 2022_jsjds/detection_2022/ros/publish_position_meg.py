'''
Description: 
Version: 2.0
Date: 2022-04-23 10:45:49
LastEditors: Meroke
LastEditTime: 2022-04-23 16:48:17
'''
from nav_msgs.msg import Odometry

import rospy


def print_odom():
  while(1):
      # (topic,topic_type,timeout)
      var = rospy.wait_for_message('/odom', Odometry, timeout=5)
      print("/odom: " + format(var.pose.pose.position.x, '.9f'),
            format(var.pose.pose.position.y, '.9f'))

      rospy.sleep(1.)
        
if __name__ == "__main__":
  rospy.init_node("publish_odom")
  print_odom()

