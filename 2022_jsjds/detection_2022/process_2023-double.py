#-*- coding: UTF-8 -*-
#!/usr/bin/python
import rospy
# 导航
# from ros.navigation import Navigation
from ros.navigation import Navigation
# 简单位移
from ros.Control import Control
# from util.utils_cv import *
# from util.utils_process import *
# 多线程
import threading
from threading import Thread
# 拍摄图片
from cv_2023 import *
# 文字识别
# from ocr.detect_test import *
# 雷达测距
# from ros.get_laser2 import adopt_forward
from single_laser_copy import *
from arm_control_2023 import *
# 日志记录模块
# from log.pylog import log
# LOGS_INFO = log.info
# 机械臂抓取动作
# from dobot_arm import *
import time

from pydobot.dobot import Dobot # 机械臂

sentinel = threading.Event()
sentinel.set()
rospy.init_node("process", anonymous=True,disable_signals=True)
navigation = Navigation()
control = Control()
control_forward = Thread(target=control.forward,args=[] )
subscribe_main_thread = Thread(target=subscribe_main,args=[])
subscribe_main_thread.start()
# looking_up_thread = Thread(target=looking_up,args=[])
# looking__down_thread = Thread(target=looking_down,args=[])
# subscribe_main_thread = Thread(target=subscribe_main,args=[])
# subscribe_main_thread.start()

# 机械臂动作线程
moving_thread = Thread(target=moving,args=[])
good2shelf_main_Point = [0.886, -0.440,25]
def good2shelf():
    navigation.send_position([good2shelf_main_Point[0], good2shelf_main_Point[1]], good2shelf_main_Point[2])
    dis_to_shelf = 1.5
    control.forward(dis_to_shelf)
    target_x, target_y, target_ang = goods[8][0], shelves[5][1], 0
    send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
    send_position_thread.start()
    rospy.sleep(3.5)
    # control.forward(0.5)

def shelf2good():
    navigation.send_position([goods[8][0]-(shelves[4][1] - shelves[3][1]), (shelves[6][1]*3+shelves[5][1])/4],-160 )
    dis_to_goods = 2.0
    control.forward(dis_to_goods)

    target_x, target_y, target_ang = goods[1][0], goods[3][1], 180
    send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
    send_position_thread.start()
    rospy.sleep(3)

def shelf2mid():
    navigation.send_position([goods[8][0]-(shelves[4][1] - shelves[3][1]), shelves[5][1]],-180)
    control.forward(1.1)
    # navigation.send_position([1.324, 0.85],120)
    target_x, target_y, target_ang = 1.324, 0.85, 120
    send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
    send_position_thread.start()
    rospy.sleep(5)
def L2mid():
    navigation.send_position([1.324, 0.85],-59)
    control.forward(1)
    # target_x, target_y, target_ang = goods[8][0], shelves[4][1], 0
    # send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
    # send_position_thread.start()
    # rospy.sleep(4.2)

# 投放点
goods = [[0.990, -0.331, 95], [0.330 ,-0.161, 90],[-0.482, -0.140,90],# 0,1,2
                [-0.370, -0.604, 0], [-0.404, 0.505, 0],# 3,4
                [-0.471, 0.813, -90], [0.315, 0.746, -90],[1.071, 0.802,-80]# 5,6,7
                 ,[3.051, 0.745, -90]# 失效邮件
                ]
# # 利用其他点，计算出失效邮件的位置
# goods[8][1] = goods[7][1] - (goods[5][1] - goods[7][1])/3*4

# 货架点（主要更改y）
# shelves = [[3.139, -0.499,-180],[3.139, -0.370,-180],[3.139, -0.238,-180],[3.139, -0.060,-180],[3.139, 0.057,-180],
#            [3.120, 0.455,-180],[3.120, 0.606,-180],[3.120, 0.772,-180],[3.120, 0.912,-180],[3.120, 1.204,-180]]

# 7.30 将倒数第二列放在了最前面，用于快速投放一次
# 8(随便投的货架，倒数第二列),    0
# 0,1,2,3,4,                  1，2，3，4，5
# 5,6,7,9                     6，7，8，9
shelves = [[3.120, 0.920,-180],
[3.139, -0.500,-180],[3.139, -0.380,-180],[3.139, -0.238,-180],[3.139, -0.060,-180],[3.139, 0.057,-180],
           [3.120, 0.400,-180],[3.120, 0.575,-180],[3.120, 0.740,-180],[3.200, 1.204,-180]]
box_detect_list=["河南","安徽","湖南","江苏","福建","广东","四川","浙江","失效","错误"]

# 载物台初始化
left_index = -1
right_index = -1

# moving()
# tmp = 1
# navigation.send_position([goods[tmp][0], goods[tmp][1]],goods[tmp][2])
# tmp = 0
# navigation.send_position([goods[tmp][0], goods[tmp][1]],goods[tmp][2])
# rospy.sleep(20)
# moving()
# L2mid()

# shelf2mid()
# for x,y, ang in shelves:
#     good2shelf()
#     navigation.send_position([x, y],ang)
#     # rospy.sleep(5)
#     shelf2good()
#     navigation.send_position([goods[3][0],goods[3][1]],goods[3][2])
# for x,y, ang in goods:
#     navigation.send_position([x, y],ang)
#     rospy.sleep(5)


# P1从投放点到货架侧（当前货架点， 前一次投放中的左/右载物台， 偏移量）
def Part01(dot_position = -1, left_index = -1, right_index = -1):
    moving_thread = Thread(target=moving,args=[])
    moving_thread.start()
    # 特殊情况A：开局一列失效邮件（直接抵达下一货架点）
    if dot_position == 1:
        pass
    # # 特殊情况B:零号+七号
    # elif left_index == 0 and right_index == 7 or left_index == 7 and right_index == 0:
    #     # 此时投放完的末状态是在0号投放箱的位置
    #     print("调整方向后进行传送")
    #     # navigation.send_position([goods[0][0],goods[0][1]],goods[0][2])# 0号点
    #     navigation.send_position([goods[0][0],goods[0][1]],-206)
    #     # control.forward(1.5)
    #     forward_thread = Thread(target = control.forward,args = [1.5])
    #     forward_thread.start()
    #     rospy.sleep(1.5)
    #     # navigation.send_position([shelves[dot_position][0],shelves[dot_position][1]],shelves[dot_position][2])   
    # 特殊情况C：第二个投放了零号
    elif left_index == 0 or right_index == 0:
        target_x, target_y, target_ang = (goods[8][0] + goods[7][0])/2, (goods[3][1]+goods[4][1])/2, -180
        send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
        send_position_thread.start()
        rospy.sleep(11)

        target_x, target_y, target_ang = goods[8][0], shelves[5][1], 0
        send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
        send_position_thread.start()
        rospy.sleep(3.5)
    # 1.失效点到货架
    elif left_index == 8 and right_index == 8 :
        print("失效点到货架")
        # 直接抵达货架即可
    # 2.出发点和下面六个点到货架
    elif left_index <= 5 or right_index <= 5:
        print("斜角侧传送到货架侧")
        good2shelf()
    # 3.其他情况
    else:
        print("六号或七号邮件到当前货架点")
        L2mid()
    print("抵达当前货架点"+str(dot_position))
    pos_x, pos_y, pos_ang = shelves[dot_position]
    navigation.send_position([pos_x,pos_y],pos_ang)


# P3直角/斜角判断部分
def Part03A(index):
    if index >= 5:
        # 走直角侧
        return "直"
    else:
        # 走斜角侧
        return "斜"

# P3 判断绕过圆盘方式部分
def Part03A_2(small_index, big_index):
    # small_index = int(small_index)
    # big_index = int(big_index)
    if small_index > big_index:
        small_index, big_index = big_index, small_index
    res = ""
    if big_index == 7 and small_index in [1,2,3]:
        res = res + "绕二"
    if big_index in [4,5,6] and small_index == 0:
        res = res + "绕一"
    if small_index == 0:
        res = res + "绕三"
    print("绕的顺序" + res)
    return res

# P3路径转换为字符串部分
def Part03B(left_index, right_index):
    res = ""
    # 投放两个失效邮件
    if left_index == 8 and right_index == 8:
        # "停"表示两个邮件相同，不需要移动
        res = "L8停R8"
    # 投放其中一个失效邮件并后续投放有效邮件
    elif left_index == 8 or right_index == 8:
        if left_index == 8:
            print("先投左边的失效邮件")
            res = res +"L8"

            tmp = Part03A(right_index)
            print("走"+ tmp +"侧")
            res = res + tmp

            print("再投右边的"+ str(right_index)+"号邮件")
            res = res +"R"+str(right_index)
        elif right_index == 8:
            print("先投右边的失效邮件")
            res = res +"R8"

            tmp = Part03A(left_index)
            print("走"+ tmp +"侧")
            res = res + tmp

            print("再投左边的"+ str(left_index)+"号邮件")
            res = res +"L"+str(left_index)
    # 两个正常邮件
    else:
        # 特殊情况：0和7
        if left_index == 7 and right_index == 0 or left_index == 0 and right_index == 7:
            res = res + "穿"
            if left_index > right_index:
                res = res + "LR"
            else:
                res = res + "RL"
        # 两个邮件序号相同
        elif right_index == left_index:
            tmp = Part03A(right_index)
            print("走"+ tmp +"侧")
            res = res + tmp

            print("投"+ str(left_index)+"号邮件两次")
            res = res + "L" + str(left_index) + "停" + "R" + str(right_index)

        # 左<右
        elif left_index < right_index:
            tmp = Part03A(right_index)
            print("走"+ tmp +"侧")
            res = res + tmp
            # if right_index - left_index >= 4:
            #     res = res +"R"+str(right_index)
                # res = res + Part03A_2(left_index,right_index)
            #     res = res +"L"+str(left_index)
            # else:
            res = res + "R"+str(right_index)+ Part03A_2(left_index,right_index) +"L"+str(left_index)
        # 左>右
        else:
            tmp = Part03A(left_index)
            print("走"+ tmp +"侧")
            res = res + tmp
            # if left_index - right_index >= 4:
            #     res = res +"L"+str(left_index)
            #     res = res + Part03A_2(right_index,left_index)
            #     res = res +"R"+str(right_index)
            # else:
            res = res + "L"+str(left_index)+ Part03A_2(left_index,right_index) +"R"+str(right_index)

    print(res)
    return res

# 删除字符串第一个字符并返回(第一个字符，剩余的字符)
def cut(s):
    f_c = s[0]
    re_cs = s[1:]
    return f_c, re_cs

# P3字符串转动作部分/投放部分
def Part03C(acts):
    while len(acts) > 0:
        act, acts = cut(acts)
        if act == "L" or act == "R":
            # 获取对应投放点act2
            tmp, acts = cut(acts)
            act2 = int(tmp)
            target_x, target_y, target_ang = goods[act2][0], goods[act2][1], goods[act2][2]
            # 先开启投放点
            send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
            send_position_thread.start()
            # 前往投放的时候，从对应一侧获取
            if act == "L":
                get_from_left_thread = Thread(target=get_from_left,args=[])
                get_from_left_thread.start()
            elif act == "R":
                get_from_right_thread = Thread(target=get_from_right,args=[])
                get_from_right_thread.start()
            # 等待到达点位后，调整位置
            send_position_thread.join()
            adopt_forward_thread = Thread(target=adopt_forward,args=[-0.5,0.17])
            adopt_forward_thread.start()
            # 调整好位置后，打开投放
            adopt_forward_thread.join()

            # 防止机械臂没来得及吸起邮件
            if act == "L":
                get_from_left_thread.join()
            elif act == "R":
                get_from_right_thread.join()

            put_into_thread = Thread(target=put_into,args=[])
            put_into_thread.start()
            # 等待投放结束后，再进行其他动作
            put_into_thread.join()
        elif act == "斜":
            # 机械臂调整到默认位置
            moving_thread = Thread(target=moving,args=[])
            moving_thread.start()
            # 斜角传送
            shelf2good()
        elif act == "直":
            # 机械臂调整到默认位置
            moving_thread = Thread(target=moving,args=[])
            moving_thread.start()
            # 直角传送
            shelf2mid()
        elif act == "停":
            pass
        elif act == "绕":
            """
            第一种情况为  0 + 456 记为 “绕一”
            第二种清况为  123 + 7 记为 “绕二”
            第三种情况为  需要投放0 记为 “绕三”
            """
            act2, acts = cut(acts)
            if act2 == "一":
                target_x, target_y, target_ang = (goods[1][0]+goods[2][0])/2, goods[3][1], -180
                send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
                send_position_thread.start()
                rospy.sleep(7)
            elif act2 == "二":
                target_x, target_y, target_ang = (goods[5][0]+goods[6][0])/2, (goods[3][1] + goods[4][1]*2)/3,  -90
                send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
                send_position_thread.start()
                rospy.sleep(10)
            elif act2 == "三":
                # target_x, target_y, target_ang = (goods[0][0]+goods[1][0])/2, goods[0][1], -180
                # send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
                send_position_thread = Thread(target = navigation.send_position,args = [[good2shelf_main_Point[0], good2shelf_main_Point[1]], good2shelf_main_Point[2]])
                send_position_thread.start()
                rospy.sleep(12)
            else:
                print("Part3 绕 的部分有问题")
            # -0.400不知道什么作用
            # target_x, target_y, target_ang = (goods[3][0]+goods[4][0])/2, -0.400, -90
            # send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
            # send_position_thread.start()
        elif act == "穿":
            # 获取对应投放左右act2， act3
            tmp, acts = cut(acts)
            act2 = str(tmp)
            tmp, acts = cut(acts)
            act3 = str(tmp)

            # 调整到默认位置
            moving_thread = Thread(target=moving,args=[])
            moving_thread.start()
            # 先传送到中间
            shelf2mid()
            
            print("先去投放七号")
            target_x, target_y, target_ang = goods[7][0] ,goods[7][1], goods[7][2]
            # 开启投放点
            send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
            send_position_thread.start()
            # 从对应侧获取
            if act2 == "L":
                get_from_left_thread = Thread(target=get_from_left,args=[])
                get_from_left_thread.start()
            else:
                get_from_right_thread = Thread(target=get_from_right,args=[])
                get_from_right_thread.start()
            
            # 到达投放点后，开始调整位置
            send_position_thread.join()
            adopt_forward_thread = Thread(target=adopt_forward,args=[-0.5,0.17])
            adopt_forward_thread.start()

            # 调整完距离后 + 已经吸取到邮件后
            adopt_forward_thread.join()
            if act2 == "L":
                get_from_left_thread.join()
            else:
                get_from_right_thread.join()

            put_into_thread = Thread(target=put_into, args=[])
            put_into_thread.start()
            # 等待投放结束后，再进行其他动作
            put_into_thread.join()
            print("准备传送到零号投放点")

            target_x, target_y, target_ang = (goods[7][0]*3 + goods[8][0])/4 ,goods[4][1] ,-90
            send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
            send_position_thread.start()
            if act3 == "L":
                get_from_left_thread = Thread(target=get_from_left,args=[])
                get_from_left_thread.start()
            else:
                get_from_right_thread = Thread(target=get_from_right,args=[])
                get_from_right_thread.start()

            rospy.sleep(8)
            # control.forward(0.5)
            target_x, target_y, target_ang = (goods[7][0]*3 + goods[8][0])/4 ,goods[3][1] ,-90
            send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
            send_position_thread.start()
            rospy.sleep(4.3)
            
            target_x, target_y, target_ang = goods[0][0], goods[0][1], goods[0][2]
            send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
            send_position_thread.start()
            rospy.sleep(5)
            
            send_position_thread.join()
            adopt_forward_thread = Thread(target=adopt_forward,args=[-0.5,0.17])
            adopt_forward_thread.start()

            adopt_forward_thread.join()
            put_into_thread = Thread(target=put_into,args=[])
            put_into_thread.start()
            # 等待投放结束后，再进行其他动作
            put_into_thread.join()
        else:
            print("Part3有问题")

# TODO:1.如果没识别到，及时止损的情况-》回传9
# device.go_home()
time_start = time.time()
global the_dis
for dot_position ,[pos_x,pos_y,pos_ang] in enumerate(shelves):
    
    ##### PartA:从投放点到货架侧————————————————————
    Part01(dot_position, left_index, right_index)
    """
    需要判断小车当前在哪里，走哪一边合适
    特殊情况A:开局一列失效邮件（直接抵达下一货架点）
    特殊情况B:零号+七号
    1.失效点到货架
    2.出发点和下面六个点到货架
    3.其他情况
    """
    #### PartB:从到达货架侧之后，获取邮件————————————————————
    # 到达目标点附近的话，那就自己检测自己的处境
    messages=[]
    start1_time=time.time()
    while(1):
        start2_time=time.time()
        if(start2_time-start1_time>2):
            break
        print(single_laser.get_dis())
        a=float(single_laser.get_dis())
    # print("jiesu")
    # # print(type(a))
    # print(a)
    if a>0.40:
        print("处于位置1")
        # looking_up()
        if a>0.42 and a <0.4:
            control_forward = Thread(target=control.forward,args=[0.2] )
            control_forward.start()
        else:
            #先抓取上面的商品
            looking_up_thread = Thread(target=looking_up,args=[])
            looking_up_thread.start()
            adopt_forward_thread = Thread(target=adopt_forward,args=[-0.3,0.47])
            adopt_forward_thread.start()
            a,b,c=find_QRCode()
            messages.append(a)
            print(a)
            adapt_grip_new(b,c)


            control_forward = Thread(target=control.forward,args=[0.35] )
            control_forward.start()
            time.sleep(1)
            put_to_left()
            #重新定位
            time.sleep(1)
            looking__down_thread=Thread(target=looking_down,args=[])
            looking__down_thread.start()
            time.sleep(1)
            adopt_forward_thread = Thread(target=adopt_forward,args=[-0.3,0.48])
            adopt_forward_thread.start()
            a,b,c=find_QRCode()
            messages.append(a)
            adapt_grip_new(b,c,1)
            control_forward = Thread(target=control.forward,args=[0.30] )
            control_forward.start()
            time.sleep(1)
            put_to_right_thread=Thread(target=put_to_right,args=[])
            put_to_right_thread.start()

    else:
        print("处于位置零")
        adopt_forward_thread = Thread(target=adopt_forward,args=[-0.3,0.3])
        adopt_forward_thread.start()
        # 同样是先抓上面的盒子
        looking_up_thread = Thread(target=looking_up,args=[])
        looking_up_thread.start()
        a,b,c=find_QRCode()
        messages.append(a)
        print(a)
        adapt_grip_new(b,c)
            # put_to_left_thread=Thread(target=put_to_left,args=[])
            # put_to_left_thread.start()
        control_forward = Thread(target=control.forward,args=[0.35] )
        control_forward.start()
        time.sleep(1)
        put_to_left()
        
        adopt_forward_thread = Thread(target=adopt_forward,args=[-0.3,0.30])
        adopt_forward_thread.start()
        looking_down_new()
        a,b,c=find_QRCode()
        messages.append(a)
        raking_grep_new(b,c,1)
        control_forward = Thread(target=control.forward,args=[0.30] )
        control_forward.start()
        time.sleep(1)
        put_to_right_thread=Thread(target=put_to_right,args=[])
        put_to_right_thread.start()

    #投放程序
    left_index=box_detect_list.index(messages[0])
    right_index=box_detect_list.index(messages[1])
    print("准备投送"+str(left_index)+"和"+str(right_index))

    put_to_right_thread.join()
    ## PartB-2:测试使用的随机数————————————————————————————————————————
    # tmp = input("卡住程序")
    # import random
    # import time
    # random.seed(time.time())
    # # left_index = random.randint(0,8)
    # # right_index = random.randint(0,8)
    # left_index = 6
    # right_index = 0

    ##### PartC:从货架侧到投放点侧————————————————————
    # 先处理成字符串
    if dot_position == 0:
        acts = "L8R8"
        control.forward(0.45)
        target_x, target_y, target_ang = goods[8][0], goods[8][1]-1, goods[8][2]
        send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
        send_position_thread.start()
        rospy.sleep(3)
    else:
        acts = Part03B(left_index, right_index)
    # acts = Part03B(left_index, right_index)
    # 动作执行
    Part03C(acts)
    """
    #投放两个失效邮件

    #投放一个失效邮件
    ##左边为失效
    ###走L侧
    ###走斜角侧

    #投放两个正常邮件
    ##特殊情况1:0和7
    ##L=R
    ##L<R
    ###走L侧
    ###走斜角侧
    ##L>R
    ###走L侧
    ###走斜角侧
    ##如果差值大于5,则添加投放途径点
    """
    print("投放完成")
print("任务完成")
navigation.send_position([0,0],0)
time_finish = time.time() - time_start
print("花费时间："+ str(time_finish // 60) +"分" + str(time_finish % 60) +"秒")


##########
    # print("准备投送"+str(left_index)+"和"+str(right_index))
    # # 投放两个失效邮件
    # if left_index == 8 and right_index == 8:
    #     print("投放两个失效邮件")
    #     navigation.send_position([goods[left_index][0] ,goods[left_index][1] ],goods[left_index][2] )

    # # 投放其中一个失效邮件并后续投放有效邮件
    # elif left_index == 8 or right_index == 8:
    #     print("投送一个失效邮件")
    #     if left_index == 8:
    #         navigation.send_position([goods[left_index][0] ,goods[left_index][1] ],goods[left_index][2] )


    #         print("前往"+str(right_index))
    #         if right_index >= 5:
    #             print("走L侧")
    #             shelf2mid(drift)
    #             navigation.send_position([goods[right_index][0] ,goods[right_index][1] ],goods[right_index][2] )

    #         else:
    #             print("走斜角侧")
    #             shelf2good(drift)
    #             if right_index == 0:
    #                 # 如果刚好在投放侧到货架侧的传送点，则使用另一个投放点
    #                 navigation.send_position([good_0[2][0] ,good_0[2][1] ],good_0[2][2] )
    #                 # 防止卡死
    #                 control.forward(0.4)
    #             else:
    #                 navigation.send_position([goods[right_index][0] ,goods[right_index][1] ],goods[right_index][2] )

    #     elif right_index == 8:
    #         navigation.send_position([goods[right_index][0] ,goods[right_index][1] ],goods[right_index][2] )

    #         print("前往"+str(right_index))
    #         if left_index >= 5:
    #             print("走L侧")
    #             shelf2mid(drift)
    #             navigation.send_position([goods[left_index][0] ,goods[left_index][1] ],goods[left_index][2] )

    #         else:
    #             print("走斜角侧")
    #             shelf2good(drift)
    #             if left_index == 0:
    #                 # 如果刚好在投放侧到货架侧的传送点，则使用另一个投放点
    #                 navigation.send_position([good_0[2][0] ,good_0[2][1] ],good_0[2][2] )
    #                 # 防止卡死
    #                 control.forward(0.4)
    #             else:
    #                 navigation.send_position([goods[left_index][0] ,goods[left_index][1] ],goods[left_index][2] )
                
    # # 投送两个正常邮件
    # else:
    #     # 两个邮件序号相同
    #     if right_index == left_index:
    #         print("前往" + str(left_index))
    #         if right_index >= 5:
    #             print("走L侧")
    #             shelf2mid(drift)
    #             navigation.send_position([goods[right_index][0] ,goods[right_index][1] ],goods[right_index][2] )

    #         else:
    #             print("走斜角侧")
    #             shelf2good(drift)
    #             if left_index == 0:
    #                 # 如果刚好在投放侧到货架侧的传送点，则使用另一个投放点
    #                 navigation.send_position([good_0[2][0] ,good_0[2][1] ],good_0[2][2] )
    #                 # 防止卡死
    #                 control.forward(0.4)
    #             else:
    #                 navigation.send_position([goods[left_index][0] ,goods[left_index][1] ],goods[left_index][2] )
    #     elif left_index == 7 and right_index == 0 or left_index == 0 and right_index == 7:
    #         print("走L侧")
    #         shelf2mid(drift)
    #         print("先去投放七号")
    #         navigation.send_position([good_7[0][0] ,good_7[0][1] ],good_7[0][2] )

    #         print("准备传送到零号投放点")
    #         navigation.send_position([-0.889 ,-2.060 ],0 )
    #         control.forward(1.2)
    #         navigation.send_position([good_0[0][0] ,good_0[0][1] ],good_0[0][2] )


    #     # 两个邮件序号不相同-A
    #     elif left_index < right_index:
    #         # 如果right_index(大的)比5小
    #         if right_index < 5:
    #             shelf2good(drift)
    #             print("走斜角侧")
    #             print(str(left_index)+"和" + str(right_index))
                
    #             # 如果小的为0，则后投放
    #             if left_index == 0:
    #                 navigation.send_position([goods[right_index][0] ,goods[right_index][1] ],goods[right_index][2] ) 
    #                 # 使用零号投放点距离传送点较近的那个
    #                 navigation.send_position([good_0[2][0] ,good_0[2][1] ],good_0[2][2] )
    #                 # 防止卡死
    #                 control.forward(0.4)
    #             else:
    #                 navigation.send_position([goods[left_index][0] ,goods[left_index][1] ],goods[left_index][2] )
    #                 navigation.send_position([goods[right_index][0] ,goods[right_index][1] ],goods[right_index][2] )    
    #         # 另外两种情况都走L
    #         else:
    #             shelf2mid(drift)
    #             print("走L侧")
    #             print(str(left_index)+"和" + str(right_index))

    #             # 如果小的为0，则后投放
    #             if left_index == 0:
    #                 navigation.send_position([goods[right_index][0] ,goods[right_index][1] ],goods[right_index][2] ) 
    #                 # 使用零号投放点距离传送点较近的那个
    #                 navigation.send_position([good_0[2][0] ,good_0[2][1] ],good_0[2][2] )
    #                 # 防止卡死
    #                 control.forward(0.4)
    #             else:
    #                 navigation.send_position([goods[right_index][0] ,goods[right_index][1] ],goods[right_index][2] )
    #                 navigation.send_position([goods[left_index][0] ,goods[left_index][1] ],goods[left_index][2] )
    #     # 两个邮件序号不相同-B
    #     elif left_index > right_index:
    #         # 如果left_index(大的)比5小
    #         if left_index < 5:
    #             shelf2good(drift)
    #             print("走斜角侧")
    #             print("先去" + str(right_index)+"再去" + str(left_index))
    #             if right_index == 0:
    #                 navigation.send_position([goods[left_index][0] ,goods[left_index][1] ],goods[left_index][2] )
    #                 # 使用零号投放点距离传送点较近的那个
    #                 navigation.send_position([good_0[2][0] ,good_0[2][1] ],good_0[2][2] )
    #                 # 防止卡死
    #                 control.forward(0.4)
    #             else:
    #                 navigation.send_position([goods[right_index][0] ,goods[right_index][1] ],goods[right_index][2] )
    #                 navigation.send_position([goods[left_index][0] ,goods[left_index][1] ],goods[left_index][2] )
    #         # 另外两种情况都走L
    #         else:
    #             shelf2mid(drift)
    #             print("走L侧")
    #             print("先去" + str(right_index)+"再去" + str(left_index))
    #             if right_index == 0:
    #                 navigation.send_position([goods[left_index][0] ,goods[left_index][1] ],goods[left_index][2] )
    #                 # 使用零号投放点距离传送点较近的那个
    #                 navigation.send_position([good_0[2][0] ,good_0[2][1] ],good_0[2][2] )
    #                 # 防止卡死
    #                 control.forward(0.4)
    #             else:
    #                 navigation.send_position([goods[left_index][0] ,goods[left_index][1] ],goods[left_index][2] )
    #                 navigation.send_position([goods[right_index][0] ,goods[right_index][1] ],goods[right_index][2] )


