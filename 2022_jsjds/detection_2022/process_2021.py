#-*- coding: UTF-8 -*- 
#!/usr/bin/python
import sys
import os
import sys
import rospy
import time

# 移除ros默认python路径避免python版本错误
os.system("rm -rf ./images/results/*")
# sys.path.append('../ros_packages_2022/devel/lib/python3/dist-packages/')

from pydobot.dobot import Dobot # 机械臂
from ros.navigation import Navigation # 导航
from ros.slide import Slide # 平移
from cv_2021 import *
from drop_2021 import *
from grab_2021 import *
from util.utils_cv import *
from util.utils_process import *
import threading
from threading import Thread


##################################################################
# 寻找机械臂端口
##################################################################
device = Dobot(port="/dev/dobot_arm", verbose=False)
device._set_queued_cmd_clear()
device.clear_all_alarms_state()
device.rotate_to(90, 0, 0, 0, wait=True) # 机械臂初始化归位
sentinel = threading.Event()
sentinel.set()
rospy.init_node("process", anonymous=True,disable_signals=True)

navigation = Navigation()
slide = Slide()
in_start = True

rotate_to_inside = Thread(target=device.rotate_to,args=[-90, 0.0, 0.0, 0.0, True]) # 机械臂转向内测
rotate_to_outside = Thread(target=device.rotate_to,args=[90, 0.0, 0.0, 0.0, True]) # 机械臂转向外侧，实际使用需重复定义

# 抓取点
grab_p = 0


for _ in range(0, 8):
    # 初始化车载空间，True表示为空
    store = {
        "l1": [True, ""],
        "l2": [True, ""],
        "l3": [True, ""],
        "l4": [True, ""],
        # "m1": [True, ""],
        # "m2": [True, ""],
        # "m3": [True, ""],
        # "m4": [True, ""],
        "r1": [True, ""],
        "r2": [True, ""],
        "r3": [True, ""],
        "r4": [True, ""],
    }
    full = False   # 是否装满
    stored = True  # 是否还有盒子
    name = None    # 当前的目标投递盒
    first_point = True
    if in_start: # 是否是一开始
        start_time=time.clock()
        # 底盘相机识别地域盒
        thread_first_box_name = Thread(target=get_box_name,args=[0,drop_boxes])
        thread_first_box_name.daemon = True
        thread_first_box_name.start()
        input("press enter to start")
        in_start = False
    
    print("[MOVE] to middle")
    navigation.send_goal("middle")
    time.sleep(1)
    # 抓取步骤


    ##########################################
    #抓取流程
    # grab_p 抓取位置
    if grab_p==6 or grab_p==0:
        grab_finished = [False] * 6
        grab_p = 0

    last =0
    while not store_full(store) and not grab_finished[grab_p]:
        rotate_to_outside = Thread(target=device.rotate_to,args=[-90, 0.0, 0.0, 0.0, True]) # 机械臂转向分拣台
        rotate_to_outside.start()
        # device.rotate_to(-90, 0.0, 0.0, 0.0, False) 
        print("[MOVE] to grab " + str(grab_p))
        # os.system("rosrun tools to_point.py g" + str(grab_p))  # 到第一个抓取点,正对
        # navigation.send_goal("g"+ str(grab_p))
        slide.to(grab_p) # 平移到分拣台上对应位置
        if rotate_to_outside.isAlive(): #线程存活
            rotate_to_outside.join() # 结束线程
        # first_point = False
        store ,last = grab(device, store,last)# 识别到多个目标,但货舱已满,保留未抓取的最后一个快递盒对应的角度
        # TODO 接下去继续读 
        #机械臂归位
        # device.rotate_to(90, 0.0, 0.0, 0.0, True)
        print(store)
        
        if last >= 2: # 当last等于2时
            grab_finished[grab_p] = True
            grab_p += 1
            
        if grab_p >= 6:
            grab_p = 0
            break
    
    ####################################################
    rotate_to_inside = Thread(target=device.rotate_to,args=[90, 0.0, 0.0, 0.0, True])
    rotate_to_inside.start()
    # device.rotate_to(90, 0.0, 0.0, 0.0, False)

    ####################################################
    # 投递流程
    if store_full(store):
        print("store fulled")
    # 投递步骤
    just_finished = False
    while not store_empty(store):
        name = None            # 当前目标投递盒
        drop_box_index = None  # 目标投递盒序号
    
        # positionIndexs=determine_area(store,drop_boxes)
        # for positionIndex in positionIndexs:
        #     os.system("rosrun tools to_point.py p"+str(positionIndex))
        # 投递盒的标签
        for i, box in enumerate(drop_boxes): # i 对应地上投递盒的标号
            if box != "unknown" and check_store(box, store):  # 检测 投递盒地名box 和 快递盒列表store 对应
                name, drop_box_index = box, i   # 如果投递盒标签已知且载有对应盒子
                break
        
        if drop_box_index is None:              # 没有对应的投递盒, 随机投放到附近的投递盒
            if "unknown" not in drop_boxes:
                just_finished = False
                for j in range(1,3): # 1 2
                    if not store["l"+str(j)][0]:
                        store["l"+str(j)][1] = drop_boxes[i] 
                    if not store["r"+str(j)][0]: 
                        store["r"+str(j)][1] = drop_boxes[i] 
            for i, box in enumerate(drop_boxes):
                if box == "unknown":
                    # os.system(
                    #     "rosrun tools to_point.py r" + str(i // 2)
                    # )  # 到第i个识别点侧对
                    print("[MOVE] to recon " + str(i // 2))
                    navigation.send_goal("r"+ str(i // 2))
                    box_left, box_right = "unknown","unknown"
                    box_left, box_right = get_box_name(i // 2,drop_boxes)
                    while box_left == "unknown" or box_right == "unknown":
                        navigation.send_goal("r"+ str(i // 2))
                        box_left, box_right = get_box_name(i // 2,drop_boxes)
                    print(
                        "[RESULT] get box name left:"
                        + str(box_left)
                        + " and right:"
                        + str(box_right)
                        + " in position "
                        + str(i)
                        + " "
                        + str(i + 1)
                    )
                    # drop_boxes[i : i + 2] = box_left, box_right  # 标签赋值
                    break
            # 没有找到对应盒子,说明出现识别错误的情况,设置所有剩余邮件为当前最近邮件放置盒的标签

        else:
            # os.system("rosrun tools to_point.py p" + str(i))  # 到第i个抓取点侧对
            navigation_thread = Thread(target=navigation.send_goal,args=["p" + str(i)])
            navigation_thread.daemon = True
            navigation_thread.start()
            if rotate_to_inside.isAlive():
                rotate_to_inside.join() # 等待线程结束
            print("[MOVE] to p " + str(i))
            # navigation.send_goal("p" + str(i))
            
            current_box =i
            finished = False
            restore_thread = Thread() # 先定义空线程，便于if循环判断
            while True:
                if restore_thread.isAlive():
                    restore_thread.join()
                store,finished = drop_by_name(device, store, name) # 一次投一个，全部投完才会返回True
                if finished:
                    break
                navigation_thread.join()
                drop_process(device) # 气泵停止，投放快递盒，机械臂归位
                restore_thread = Thread(target=restore,args=[device,store]) # 如果有一货舱有三层盒子，则将顶部复原成两个
                restore_thread.daemon = True
                restore_thread.start()

            
            pass

print("[INFO] process passed "+str((time.clock() - start_time)/60)+" minutes")
sentinel.clear()
pipeline_stop()