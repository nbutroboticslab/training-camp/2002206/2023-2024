
查看usb具体参数
udevadm info --attribute-walk --path=/sys/bus/usb-serial/devices/ttyUSB0

获得具体编号
写入 sudo imim /etc/udev/rules.d/usb.rules 
‵‵`
KERNEL=="ttyUSB*", KERNELS=="1-2.3", MODE:="0777",SYMLINK+="dobot_arm"
```

重启设备
```
sudo service udev reload
sudo service udev restart
```

最终查看
ls -l  /dev |grep ttyUSB*
```
lrwxrwxrwx  1 root root           7 6月  22 23:23 dobot_arm -> ttyUSB0
lrwxrwxrwx  1 root root           7 6月  22 23:23 gps0 -> ttyUSB0
crwxrwxrwx  1 root dialout 188,   0 6月  22 23:23 ttyUSB0
```