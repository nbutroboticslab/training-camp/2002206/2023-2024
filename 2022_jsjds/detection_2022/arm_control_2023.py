#-*- coding: UTF-8 -*-
#!/usr/bin/python
import rospy
from pydobot.dobot import Dobot # 机械臂
import time
# from cv_2023 import *
from single_laser import *
# 机械臂的初始化
# 设备定义
device = Dobot(port="/dev/dobot_arm", verbose=False)
# 关闭气泵
device.suck(False)
device._set_queued_cmd_clear()
# device.rotate_to(90,0,0,100,wait=True)
# device.go_home()
# device.rotate_to(90,0,0,100,wait=True)
# device.set_home_cmd(wait=True)
device.clear_all_alarms_state()

# 机械臂在移动过程中，需要旋转到背侧
def moving():
    j1,j2,j3,j4 = device.pose()[4:8]
    if [j1,j2,j3] != [90,0,60]:
        # 防止碰到载物台
        if int(j1) < 50:
            device.rotate_to(j1,0,0,50,wait=True)
        # elif int(j1) < 90:
        #     device.rotate_to(90,j2,j3,50,wait=True)
        # 转到后方
        device.rotate_to(90,0,0,50,wait=True)
    # # 往下耷拉
    # device.rotate_to(90,0,60,50,wait=True)

# 转到左侧的载物台进行抓取
# 合适的抓取点
# (-92.33574676513672, 302.12738037109375, 35.8466682434082, 106.9941177368164, 106.9941177368164, 56.78650665283203, 15.022022247314453, 0.0)
def get_from_left():
    # 防止碰到载物台
    # j1,j2,j3,j4 = device.pose()[4:8]
    # if int(j1) < 60:
    #     device.rotate_to(j1,j2,j3,0,wait=True)
        
    # 跑到左侧载物台的上方
    device.rotate_to(107,0,0,0,wait=True)
    
    
    device.suck(True)#吸取
    device.rotate_to(107,20,0,0,wait=False)
    # time.sleep(2)# 停留
    # 跑到左侧载物台的点
    # device.move_to(-92.33574676513672, 302.12738037109375, 35.8466682434082, 106.9941177368164,wait=True)
    # 7.28  (-67.83075714111328, 300.19842529296875, 56.51369857788086, 102.73234558105469, 102.73235321044922, 49.73646545410156, 12.069485664367676, -7.62939453125e-06)
    # device.move_to(-67.83075714111328, 300.19842529296875, 56.51369857788086, 102.73234558105469,wait=True)
    #8月6号更改位置
    device.move_to(-68.0386734008789, 301.7669677734375, 48.02861404418945, 102.70587158203125,wait=True)
    
    rospy.sleep(1)
    # 抬升
    device.rotate_to(107,0,-10,0,wait=True)
    # # 放置
    device.rotate_to(-90,50,0,100,wait=True)


# 转到右侧侧的载物台进行抓取
# 合适的抓取点
# (107.63591003417969, 293.8485107421875, 47.94981384277344, 92.88235473632812, 69.88235473632812, 53.513668060302734, 12.703187942504883, 23.0)
def get_from_right():
    # 防止碰到载物台
    j1,j2,j3,j4 = device.pose()[4:8]
    if int(j1) < 60:
        device.rotate_to(j1,j2,j3,0,wait=True)
    # 跑到右侧载物台的上方
    device.suck(True)#吸取
    device.rotate_to(70,0,0,0,wait=False)
    device.rotate_to(67,20,0,0,wait=False)
    
    # # 跑到右侧侧载物台的点

    # (71.14017486572266, 195.45606994628906, 135.0, 70.0
    # device.move_to(107.32537078857422, 287.2344970703125, 38.41877365112305,69.51176452636719,wait=True)
    # 7.25->(100.3282470703125, 308.2938537597656, 41.46633529663086, 71.97352600097656, 71.97352600097656, 61.070213317871094, 9.33257007598877, 0.0)
    device.move_to(100.3282470703125, 302.12738037109375, 35.8466682434082, 106.9941177368164,wait=True)
    # # time.sleep(3)# 停留
    # time.sleep(1)
    # # 抬升
    device.rotate_to(70,0,-10,0,wait=True)
    # # 放置
    device.rotate_to(-90,50,0,100,wait=True)
    # device.rotate_to(-90,0,0,100,wait=True)
    # device.suck(False)

# 放置到左侧
def put_to_left():
    j1,j2,j3,j4 = device.pose()[4:8]
    # 抬升到当前位置的较高点
    device.rotate_to(j1,j2,j3,23,wait=False)
    main_j1 = 105
    # 旋转到物料台的上方
    device.rotate_to(main_j1,0,0,23,wait=False)
    # 向下
    device.rotate_to(main_j1,0,52,23,wait=False)
    # 放置
    device.rotate_to(main_j1,0,52,60,wait=False)
    # 继续下沉
    device.rotate_to(main_j1,0,68,60,wait=False)
    #从中间绕出
    device.rotate_to(86,0,68,60,wait=False)
    device.rotate_to(86,0,0,60,wait=False)

# 放置到右侧
def put_to_right():
    j1,j2,j3,j4 = device.pose()[4:8]
    # 抬升到当前位置的较高点
    device.rotate_to(j1,j2,j3,23,wait=False)
    main_j1 = 69
    # 旋转到物料台的上方
    device.rotate_to(main_j1,0,0,23,wait=False)
    # 向下
    device.rotate_to(main_j1,0,50,23,wait=False)
    # 放置
    device.rotate_to(main_j1,0,50,60,wait=False)
    # 继续下沉
    device.rotate_to(main_j1,0,60,60,wait=False)
    #从中间绕出
    device.rotate_to(86,0,68,60,wait=False)
    device.rotate_to(86,0,0,60,wait=False)

# 正面识别(上)(3.18978214263916, -230.12921142578125, -35.61735534667969, 12.170600891113281, -89.20587921142578, 46.482078552246094, 61.00542449951172, 100)
def looking_up():
    # device.rotate_to(90,0,0,100,wait=True)
    time.sleep(1)
    # 防止碰到载物台
    j1 = device.pose()[4]
    if int(j1) > 0:
        device.rotate_to(j1,0,0,50,wait=False)
        device.rotate_to(0,40,40,50,wait=False)
    # device.rotate_to(-90,0,0,100,wait=True)
    device.rotate_to(-90,40,50,50,wait=False)
    # device.rotate_to(-90,53,58,100,wait=True)
    # 1  -25.395612716674805, -235.00662231445312, -30.60987091064453, 5.208839416503906, 
    # device.move_to(3.18978214263916, -230.12921142578125, -35.61735534667969, 12.170600891113281,wait=True)
    device.move_to(-25.395612716674805, -235.00662231445312, -30.60987091064453, 5.208839416503906, wait=False)
    # print(device.pose())

# 正面识别(下)(-1.0230492989649065e-05, -234.04640197753906, -141.99110412597656, 10.0, -90.0, 90.0, 75.0, 100.0)
def looking_down():
    # device.rotate_to(90,0,0,100,wait=True)
    # time.sleep(1)
    # 防止碰到载物台
    j1 = device.pose()[4]
    if int(j1) > 0:
        device.rotate_to(j1,0,0,65,wait=False)
        device.rotate_to(0,40,40,65,wait=False)
    # device.rotate_to(-90,0,0,100,wait=True)
    device.rotate_to(-90,60,60,65,wait=False)
    device.rotate_to(-90,90,75,65,wait=False)
    # -1.0230492989649065e-05, -234.04640197753906, -141.99110412597656,

    # device.move_to(-1.0230492989649065e-05, -234.04640197753906, -141.99110412597656, 100.0,wait=True)
    # 1 -42.550235748291016, -243.80169677734375, -131.77706909179688, -49.900001525878906,
    # 2 -38.360023498535156, -253.55584716796875, -128.891357421875, -48.602935791015625,
    # 3 -2.644080638885498, -272.5205383300781, -122.44325256347656, -40.555885314941406,
    # -8.8247652053833, -272.7786865234375, -115.72584533691406, -41.852943420410156,
    device.move_to(-31.962081909179688, -236.30532836914062, -140.64047241210938, -32.70294189453125,wait=False)
    # print(device.pose())

# 抓取(上)
def catch_up():
    # 准备夹取
    device.rotate_to(-90, 5.497984409332275, 43.97998046875, 60.0,wait=True)
    # 伸入
    device.rotate_to(-90.688232421875, 51.040714263916016, 21.97551918029785, 60.0,wait=True)
    # 夹取
    device.rotate_to(-90.688232421875, 51.040714263916016, 21.97551918029785, 23.0,wait=True)
    # 离开柜子
    device.rotate_to(-90, 5.497984409332275, 43.97998046875, 23.0,wait=True)

# 抓取(下)(-3.221076726913452, -303.11676025390625, -86.41521453857422, -90.60882568359375, -90.60882568359375, 84.25537872314453, 42.826622009277344, 0.0)
def catch_down():

    ### 需要先保持一定距离,张开爪子
    # 车向前移动
    device.rotate_to(-90.60882568359375, 84.25537872314453, 42.826622009277344, 100.0,wait=True)
    # 抓取
    device.rotate_to(-90.60882568359375, 84.25537872314453, 42.826622009277344, 23.0,wait=True)
    # 车向后移动


def adapt_grip_new(Qr_positioon_center,Qr_w=1,targer=0):
    origin_centre = [320,240]#图像中心点的位置
    offset_coef = 0.109375#每个像素代表的具体的长度
    focuses=5.5#这里的单位为mm
    real_w=26
    offset_x=0
    offest_z=0
    offset_y2=0
    # -1.0230492989649065e-05, -234.04640197753906, -141.99110412597656
    UP_Down_list=[[-25.395612716674805, -235.00662231445312, -30.60987091064453],[-31.962081909179688, -236.30532836914062, -140.64047241210938]]
    offset_x = (Qr_positioon_center[0]- origin_centre[0]) * offset_coef 
    offset_y2 = (Qr_positioon_center[1] - origin_centre[1]) * offset_coef
    if Qr_w==0:
        Qr_w=1
    offest_z=(real_w/Qr_w)*focuses*10
    if targer==0:
        device.move_to(UP_Down_list[0][0]-offset_x+10 ,UP_Down_list[0][1]-offest_z*6, UP_Down_list[0][2]-offset_y2+56+10,40,wait=True)
        j1,j2,j3,j4=device.pose()[4:8]
        device.rotate_to(j1,j2,j3,23.0,wait=True)
    if targer==1:
        device.move_to(UP_Down_list[1][0]-offset_x+10+10 ,UP_Down_list[1][1]-offest_z*6, UP_Down_list[1][2]-offset_y2+56+10-4,40,wait=True)
        j1,j2,j3,j4=device.pose()[4:8]
        device.rotate_to(j1,j2,j3,23.0,wait=True)
    # device.move_to(-1.023049298964-offset_x+10 ,-234.04640197-offest_z*6, -141.991104125-offset_y2+56,0,wait=True)
    # device.move_to(3.1897547245025635-offset_x-3,-230.1292266845703-offest_z*6, -35.617332458496094-offset_y2+58,45,wait=True)
    # device.move_to(-1.023049298964-offset_x+10 ,-234.04640197-offest_z*6, -141.991104125-offset_y2+56,40,wait=True)
    # j1,j2,j3,j4=device.pose()[4:8]
    # device.rotate_to(j1,j2,j3,23,wait=True)
    # device.move_to(3.1897547245025635-offset_x-3,-230.1292266845703-offest_z*6, -35.617332458496094-offset_y2+58,23,wait=True)
    # device.move_to(-1.023049298964-offset_x+10 ,-234.04640197-offest_z*6, -141.991104125-offset_y2+56,40,wait=True)
    # print (dev)
    

# 放置
def put_into():
    # device.rotate_to(-90,50,0,100,wait=True)
    device.suck(False)
    # 配合后续动作
    # device.rotate_to(90,0,0,50,wait=True)

# 展示用
def for_show1():
    moving()
    device.rotate_to(90,0,0,60,wait=True)
    device.rotate_to(90,20,20,60,wait=True)
    device.rotate_to(-90,40,50,60,wait=True)
    rospy.sleep(1)
    #(0.28097233176231384, -304.09979248046875, 34.39897155761719, -29.947059631347656, -89.94705963134766, 51.07133102416992, 20.062910079956055, 60.0)
    device.rotate_to(-89.94705963134766, 51.07133102416992, 20.062910079956055, 60.0,wait=True)
    device.rotate_to(-89.94705963134766, 51.07133102416992, 20.062910079956055, 20.0,wait=True)
    device.rotate_to(-90,0,33,20,wait=True)
    put_to_right()

def for_show2():
    # 跑到左侧载物台的上方
    device.rotate_to(107,0,0,60,wait=True)
    
    device.suck(True)#吸取
    
    # 跑到左侧载物台的点
    # (-102.7784652709961, 265.66607666015625, 39.29216003417969, 111.14999389648438, 111.1500015258789, 41.915157318115234, 24.58833122253418, -7.62939453125e-06)
    device.rotate_to(111.1500015258789, 42.915157318115234, 26.58833122253418,60,wait=True)
  
    time.sleep(1)
    # 抬升
    device.rotate_to(107,0,0,60,wait=True)
    # 放置
    device.rotate_to(-90,50,0,60,wait=True)
    device.suck(False)

def raking():
    # device.rotate_to(90,0,0,100,wait=True)
    time.sleep(1)
    # 防止碰到载物台
    j1 = device.pose()[4]
    if int(j1) > 0:
        device.rotate_to(j1,0,0,50,wait=True)
        device.rotate_to(0,40,40,50,wait=True)
    # device.rotate_to(-90,0,0,100,wait=True)
    device.rotate_to(-90,40,50,50,wait=True)
    # device.rotate_to(-90,53,58,100,wait=True)
    # device.move_to(3.18978214263916, -230.12921142578125, -35.61735534667969, 12.170600891113281,wait=True)
    # -85.15752410888672, -234.4925537109375, -24.13593292236328, -8.58233642578125,
    # -91.8214111328125, -252.47891235351562, -23.026443481445312, -86.98529052734375,
    # -58.60606002807617, -232.2605438232422, -30.000152587890625, -2.7852783203125
    device.move_to(-85.15752410888672, -234.4925537109375, -24.13593292236328, -8.58233642578125,wait=True)
    # print(device.pose())
def raking_grep_new(Qr_positioon_center,Qr_w=1,targer=0):
    origin_centre = [320,240]#图像中心点的位置
    offset_coef = 0.109375#每个像素代表的具体的长度
    focuses=5.5#这里的单位为mm
    real_w=26
    offset_x=0
    offest_z=0
    offset_y2=0
    #带修改的是下面的盒子代码
    UP_Down_list=[[-85.15752410888672, -234.4925537109375, -24.13593292236328, -8.58233642578125],[-51.2989501953125, -259.1482849121094, -129.80601501464844]]
    #计算出QR二维码的中心坐标偏差
    offset_x = (Qr_positioon_center[0]- origin_centre[0]) * offset_coef 
    offset_y2 = (Qr_positioon_center[1] - origin_centre[1]) * offset_coef
    if Qr_w == 0:
        Qr_w = 1
    offest_z=(real_w/Qr_w)*focuses*10
    if targer==0:
        device.move_to(UP_Down_list[0][0]-offset_x-10 ,UP_Down_list[0][1]-offest_z*6, UP_Down_list[0][2]-offset_y2+56+10,40,wait=True)
        j1,j2,j3,j4=device.pose()[4:8]
        device.rotate_to(j1,j2,j3,23.0,wait=True)
    if targer==1:
        # device.move_to(UP_Down_list[1][0]-offset_x+20-20 ,UP_Down_list[1][1]-offest_z*6+20, UP_Down_list[1][2]-offset_y2+46,40,wait=True)
        device.move_to(UP_Down_list[1][0]-offset_x+20-20 ,UP_Down_list[1][1]-offest_z*6+20, UP_Down_list[1][2]-offset_y2+46+10-10,40,wait=True)
        j1,j2,j3,j4=device.pose()[4:8]
        device.rotate_to(j1,j2,j3,23.0,wait=True)
#这是一个新的关于下方货物的机械臂识别货物的程序
def looking_down_new():
    time.sleep(1)
    # 防止碰到载物台
    j1 = device.pose()[4]
    if int(j1) > 0:
        device.rotate_to(j1,0,0,50,wait=True)
        device.rotate_to(0,40,40,50,wait=True)
    # device.rotate_to(-90,0,0,100,wait=True)
    device.rotate_to(-90,40,50,50,wait=True)
    # 1 -82.66400909423828, -242.53822326660156, -118.69119262695312, -58.820579528808594,
   #1    -67.15767669677734, -248.64356994628906, -117.56354522705078, -55.11470031738281,
#    3 -47.540348052978516, -260.9882507324219, -122.9727783203125, -50.323509216308594, 
# 2-51.381439208984375, -257.6894226074219, -122.73085021972656, -51.276458740234375,

    # device.move_to(-49.646507263183594, -264.9077453613281, -122.63227081298828, -50.61468505859375, wait=True)
    # 8月7号 -51.2989501953125, -259.1482849121094, -129.80601501464844, -51.197044372558594, 
    device.move_to(-51.2989501953125, -259.1482849121094, -129.80601501464844, -51.197044372558594, wait=True)

# #8.7号机械臂动作更改
# def left_suck():
#     # 跑到左侧载物台的上方
#     device.rotate_to(107,0,0,0,wait=True)
    
    
#     device.suck(True)#吸取
#     device.rotate_to(107,20,0,0,wait=False)
#     # time.sleep(2)# 停留
#     # 跑到左侧载物台的点
#     # device.move_to(-92.33574676513672, 302.12738037109375, 35.8466682434082, 106.9941177368164,wait=True)
#     # 7.28  (-67.83075714111328, 300.19842529296875, 56.51369857788086, 102.73234558105469, 102.73235321044922, 49.73646545410156, 12.069485664367676, -7.62939453125e-06)
#     # device.move_to(-67.83075714111328, 300.19842529296875, 56.51369857788086, 102.73234558105469,wait=True)
#     #8月6号更改位置
#     device.move_to(-68.0386734008789, 301.7669677734375, 48.02861404418945, 102.70587158203125,wait=True)
    
#     rospy.sleep(1)
#     # 抬升
#     device.rotate_to(107,0,-10,0,wait=True)
#     # 放置
#     device.rotate_to(-90,50,0,100,wait=True)
#     device.suck(False)
#     # 117.52269744873047, 312.3232116699219, 19.91802406311035, 119.37940979003906,
# def left2right():
#     moving()
#     device.move_to(100.3282470703125, 302.12738037109375, 35.8466682434082, 106.9941177368164,wait=True)
#     device.suck(True)
#     time.sleep(3)
# def right_suck():
#     device.rotate_to(70,0,-10,0,wait=True)
#     # # 放置
#     device.rotate_to(-90,50,0,100,wait=True)
def test_grip():
     device.move_to(-58.60606002807617, -232.2605438232422, -30.000152587890625, -2.7852783203125,wait=True)
     device.move_to(-58.60606002807617, -232.2605438232422, -30.000152587890625+40, -2.7852783203125,wait=True)

# 左边投放（待考虑）
def threw_L():
    device.rotate_to(0,70,20 ,30,wait=True)

# 调整邮件在载物台中的位置
def adjust():
    pass
if __name__ == "__main__":
    device.go_home()
    moving()
    # looking_down_new()
    # left_suck()
    # left2right()
   
    # right_suck()
    


    # catch_up()
    # put_to_left()
    # catch_up()
    # put_to_right()

    # device.rotate_to(123,0,0,85,wait=True)
    # device.rotate_to(123,56,5,85,wait=True)
    # device.rotate_to(116,56,5,85,wait=True)

    # device.rotate_to(70, 0, -10, 30.0,wait=True)

    # device.rotate_to(95,60,30,50,wait=True)

    # device.rotate_to(90,0,0,27,wait=True)
    # device.move_to(-1.0230492989649065e-05, -234.04640197753906, -141.99110412597656,40,wait=True)
    # go_home()
    # a,b,c=find_QRCode()
    # adapt_grip_new(b,c)
    # test_grip()
    # device.go_home()
    # looking_up()
    # device.move_to(-1.0230492989649065e-05, -234.04640197753906, -141.99110412597656,40,wait=True)
    # catch_up()
    # looking_up()
    # raking()
    # a,b,c=find_QRCode()
    # adapt_grip_new(b,c)
    # looking_down_new()
    # test_grip()
    # looking_down()
    # test_grip()
    # looking_down()
    # device.rotate_to(-90,0,0,100,wait=True)

    # moving()
    # get_from_right()
    # get_from_right()
    
    # put_to_right()
    # put_into()
    # get_from_left()
    # put_into()
    # moving()
    # time.sleep(2)
    # get_from_right()
    # put_into()
    # put_into()
    # moving()
    # get_from_left()
    # time.sleep(1)
    # put_into()
    # time.sleep(1)
    # moving()
    # get_from_right()
    # put_into()
    # device.rotate_to(107,0,0,0,wait=False)
    # device.rotate_to(107,20,0,0,wait=False)
    # device.move_to(-92.33574676513672, 302.12738037109375, 35.8466682434082, 106.9941177368164,wait=True)

    # device.move_to(-90,30,30,50,wait=True)
    # time.sleep(3)
    # device.move_to(device.pose()[0]+10,device.pose()[1],device.pose()[2],50,wait=True)
    # device.rotate_to(-90,60,60,100,wait=True)

    # looking_down()
    # looking_up()
    # time.sleep(2)
    # device.rotate_to(device.pose()[4],device.pose()[5],device.pose()[6],23,wait=True)

    # j1,j2,j3,j4=device.pose()[4:8]
    # device.rotate_to(j1,j2,j3,50,wait=True)
    # device.go_home()
    # rospy.sleep(3)
    # device.suck(True)
    # distance=single_laser.get_dis()
  
 
    # print(device.pose())
    # device.rotate_to(0,0,0,23.0,wait=True)
    # device.rotate_to(0,0,0,50.0,wait=True)


    # (-1.078420700650895e-05, -246.71389770507812, -43.41804504394531, 10.0, -90.0, 53.0, 58.0, 100.0)
    # j1,j2,j3,j4=device.pose()[4:8]
    # device.rotate_to(j1,j2,j3,50,wait=True)
    # device.rotate_to(j1,j2,j3,23,wait=True)

    # device.go_home()
    # for_show2()
    
    # device.rotate_to(67,0,0,0,wait=True)
    # device.rotate_to(67,20,0,0,wait=True)
    # get_from_left()
    # put_into()
    # get_from_right()
    # put_into()
    ########## 关闭吸盘
    device.suck(False)
    ########## 答应当前机械臂状态
    print(device.pose())
    