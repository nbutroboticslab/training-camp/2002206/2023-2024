from serial.tools import list_ports

from pydobot.dobot import Dobot
import time

drop_position = [None, -32, 4, 38, 55, 55, 55]
drop_j2 = 40
drop_j3 = 0
drop_y = 181
drop_x = 70
storage_position = {'l':[100,100],'m':[100,100],'r':[100,100]} # drop_x, drop_y
storage_angle = {'l':90,'m':45,'r':0}


def pre_drop_process(device, position):
    direction,i = position
    device.rotate_to(storage_angle[direction], 0.0, 0.0, -90.0, False)  
    drop_x,drop_y = storage_position[direction]
    # print(drop_position[i+1])
    # print(drop_position[i])
    device.move_to(drop_x, drop_y, drop_position[i+1], 0, True)
    device.suck(True)
    device.move_to(drop_x, drop_y, drop_position[i], 0, True)
    
    # time.sleep()
    device.move_to(drop_x, drop_y, 55, 0, True)
    device.rotate_to(storage_angle[direction], 0.0, 0.0, -90.0, False)

def drop_process(device):
    device.rotate_to(-90.0, 50.0, 0.0, 0.0, True)
    time.sleep(0.2)
    device.suck(False)
    device.rotate_to(90.0, 0.0, 0.0, -90.0, True)


def pre_grab_process(device, position,store):
    direction,i = position
    device.rotate_to(storage_angle[direction], 0.0, 0.0, -90.0, False)  
    drop_x,drop_y = storage_position[direction]
    ##################################################################
    # 转移过程
    ##################################################################
    device.move_to(drop_x, drop_y , drop_position[i + 2], 0, True)
    device.move_to(drop_x, drop_y , drop_position[i+1], 0, True)

    device.suck(True)
    # time.sleep(0.5)
    device.move_to(drop_x, drop_y , 60, 0, True)
    # device.move_to(0, drop_y+30 , 60, 0, True)
    device.move_to(storage_position['m'][0], storage_position['m'][1], 60, 0, True)
    time.sleep(0.2)
    device.suck(False)
    # device.move_to(0, drop_y+30 , 60, 0, True)
    device.move_to(drop_x, drop_y , 60, 0, True)
    ##################################################################
    # 抓取过程
    ##################################################################
    pre_drop_process(device, position)

def restore(device,store,position):
    direction,i = position
    device.rotate_to(storage_angle[direction], 0.0, 0.0, -90.0, False)  
    drop_x,drop_y = storage_position[direction]
    # towards, i = position
    # if towards == "l":
    #     x = -drop_x
    #     reverse_towards = "r"
    # else:
    #     x = drop_x
    #     reverse_towards = "l"

    for i in range(1, 4):
        if i==3 or store[reverse_towards + str(i)][0]:
            device.move_to(-x, drop_y , drop_position[i+1], 0, True)
            device.suck(True)
            device.move_to(-x, drop_y , drop_position[i], 0, True)
            device.move_to(-x, drop_y , 60, 0, True)
            # device.move_to(0, drop_y+30 , 60, 0, True)
            device.move_to(x, drop_y , 60, 0, True)
            time.sleep(0.2)
            device.suck(False)
            # 清理存储顺序
            store[towards + str(1)][1] = store[towards + str(2)][1]
            store[towards + str(1)][0] = False
            store[towards + str(2)][0] = True
            print("move job done with " + str(store))
            # print("#############################################")
            break
    
    return store


def drop_by_name(device, store, name):
    '''
    store:当前车载box列表
    name:投递目标
    按照从上到下,从左到右的顺序将一个投递盒移动到预投递位置
    need_restore表示是否需要调用restore函数
    '''
    finished = True
    need_restore = False
    position = None
    device.rotate_to(90.0, 0.0, 0.0, -90.0, True)
    for i in range(2, 0, -1):
        if not store["r" + str(i)][0] and (
            store["r" + str(i)][1] == name or name == "test" or store["r" + str(i)][1] == "unknown"
        ):
            print("drop job with r" + str(i)+" "+store["r" + str(i)][1])
            if i < 2 and not store["r" + str(i + 1)][0]:  # 如果上面有盒子
                pre_grab_process(device, ["r", i],store)  # 搬開上面的盒子
                need_restore = True
            else:
                pre_drop_process(device, ["r", i])
                store["r" + str(i)][0] = True
            position = ["r", i]
            finished = False
            
            break
            # print("#############################################")

        if not store["m" + str(i)][0] and (
            store["m" + str(i)][1] == name or name == "test" or store["m" + str(i)][1] == "unknown"
        ):
            print("drop job with m" + str(i)+" "+store["m" + str(i)][1])
            if i < 2 and not store["m" + str(i + 1)][0]:  # 如果上面有盒子
                pre_grab_process(device, ["m", i],store)  # 搬開上面的盒子
                need_restore = True
            else:
                pre_drop_process(device, ["m", i])
                store["m" + str(i)][0] = True
            position = ["m", i]
            finished = False
            
            break
            # print("#############################################")

        if not store["l" + str(i)][0] and (
            store["l" + str(i)][1] == name or name == "test" or store["l" + str(i)][1] == "unknown"
        ):
            print("drop job with l" + str(i)+" "+store["l" + str(i)][1])
            if i < 2 and not store["l" + str(i + 1)][0]:  # 如果上面有盒子
                pre_grab_process(device, ["l", i],store)  # 搬開上面的盒子
                need_restore = True
            else:
                pre_drop_process(device, ["l", i])
                store["l" + str(i)][0] = True
            position = ["l", i]
            finished = False
            break
            # print("#############################################")
    # device.rotate_to(90.0, 0.0, 0.0, -90.0, True)
    return store,finished,need_restore,position


def drop_test():
    for i in range(0, 4):
        try:
            port = list_ports.comports()[i].device
            device = Dobot(port=port, verbose=False)
        except:
            pass
        else:
            break
    device._set_queued_cmd_clear()
    device.clear_all_alarms_state()
    device.rotate_to(90, 0, 0, 0, wait=True)
    store = {
        "l1": [False, "unknown"],
        "l2": [False, "湖南"],
        "l3": [True, ""],
        "l4": [True, ""],
        "m1": [True, ""],
        "m2": [True, ""],
        "m3": [True, ""],
        "m4": [True, ""],
        "r1": [False, "河南"],
        "r2": [False, "山西"],
        "r3": [True, ""],
        "r4": [True, ""],
    }
    # pre_grab_process(device,store,["l",2])
    drop_by_name(device=device,store=store,name="河南")
    # drop(device)


if __name__ == "__main__":
    drop_test()
