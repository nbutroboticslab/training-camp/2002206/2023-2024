#!/home/eaibot/anaconda3/envs/pro/bin/python

from threading import Thread

import math
import os
import sys
import time

try:
    sys.path.remove("/opt/ros/melodic/lib/python2.7/dist-packages")
except:
    pass
from serial.tools import list_ports

from cv_2021 import  get_boxes_info,get_boxes_address
from pydobot.dobot import Dobot
from util.keys_prov import province  # 省份信息字典

store_position = [None, -31, 5, 46, 55,55,55]
# 存储函数
def locate(device, store, result):
    full = False
    # device.rotate_to(90.0, 0.0, 0.0, -90, True)  # 90是放置方向,r=相反

    for i in range(1, 3):
        if store["l" + str(i)][0]:
            device.rotate_to(110.0, 0.0, 0.0, -90, True)
            device.move_to(-70, 181, store_position[i + 2], 0)
            # device.move_to(-128, 170, store_position[i], 0)
            store["l" + str(i)][0] = False
            store["l" + str(i)][1] = result
            print("store " + str(result) + " to l" + str(i))
            break
        
        if store["m" + str(i)][0]:
            device.rotate_to(90.0, 0.0, 0.0, -90, True)
            device.move_to(0, 170, store_position[i + 2], 0)
            # device.move_to(0, 170, store_position[i], 0)
            store["m" + str(i)][0] = False
            store["m" + str(i)][1] = result
            print("store " + str(result) + " to m" + str(i))
            full = i == 2
            break
        if store["r" + str(i)][0]:
            device.rotate_to(70.0, 0.0, 0.0, -90, True)
            device.move_to(70, 181, store_position[i + 2], 0)
            # device.move_to(128, 170, store_position[i], 0)
            store["r" + str(i)][0] = False
            store["r" + str(i)][1] = result
            print("store " + str(result) + " to r" + str(i))
            full = i == 2
            break
    device.suck(False)
    print("--------------grabed-----------------")
    time.sleep(0.5)
    # device.rotate_to(90.0, 0.0, 0.0, 0.0, True)
    return full


def grab(device, store):
    # 以视角正向x轴正方向,机械臂正向y轴正方向为例
    coord_camera_to_sucker_y = 63  # mm
    coord_camera_to_sucker_z = -105  # mm
    coord_camera_inner_regulate_x = 30  # mm #
    coord_endpoint_to_sucker_z = -76  # mm
    coord_camera_to_endpoint_z = (
        coord_camera_to_sucker_z - coord_endpoint_to_sucker_z
    )  # -29 mm

    rot = range(0, 3)
    finished = False
    fulled = False
    base_z = 172.53
    for j in rot:  # 三个角度抓取
        if fulled:
            break
        # while True:
        # 末端执行器坐标
        if j == 2:  # j1=120
            base_y = -165.60
            base_x = -95.61
            grab_angle = -120

        elif j == 1:  # j1=90
            base_y = -191.22
            base_x = 0
            grab_angle = -90
        else:  # j1=60
            base_y = -165.60
            base_x = 95.61
            grab_angle = -60
            finished = True
        device.rotate_to(grab_angle, -5, -15, 0, True)
        time.sleep(0.2)
        # print(device.pose())
        start_time = time.clock()
        boxes,color_image,rects = get_boxes_info()  # 获取每个盒子的相对摄像头左镜头坐标

        results = []
        try:
            thread = Thread(target=get_boxes_address,args=(results,color_image,rects))
            thread.daemon = True
            thread.start()
            print("box detect time passed"+str(time.clock() - start_time))
            i = 0
            for coord, angle in boxes:  # 对于每一个盒子
                start_time = time.clock()
                # box[0]:中心点坐标xy
                # box[1]:width 与 height
                # box[2]:角度
                print("========================================================")
                print("In angle " + str(grab_angle))
                print("coordicate is "+str(coord))

                # print(str(i)+" r_angle "+str(angle))
                # print("position="+str(position))
                x, y, z = coord  # 摄像头坐标系xyz = 实际坐标系方向(对准左侧) x -y -z
                x = -x + coord_camera_inner_regulate_x  # x坐标由左侧镜头变换至末端执行器
                z = base_z - coord_camera_to_endpoint_z - z  # z坐标由摄像头变换至末端执行器
                y = y - coord_camera_to_sucker_y  # y坐标由摄像头变换至末端执行器

                # 计算坐标系旋转
                sin = math.sin(math.radians(-(90 + grab_angle)))
                cos = math.cos(math.radians(-(90 + grab_angle)))
                if j != 1:  # 坐标系旋转30度
                    grab_x = cos * x + sin * y
                    grab_y = cos * y - sin * x
                else:
                    grab_x = x
                    grab_y = y

                grab_x = grab_x + base_x
                grab_y = grab_y + base_y
                grab_z = z - coord_endpoint_to_sucker_z - 13  # 吸盘

                degree = abs(math.degrees(math.atan(grab_y / grab_x)))
                # 最大角度
                if grab_y < 0 and grab_x < 0 and abs(degree) < 55:
                    grab_y = -200
                    grab_x = 0
                    print("[GRAB] Out of angle")
                # 最大距离与最小距离
                elif (grab_x) ** 2 + (grab_y) ** 2 > 314 ** 2:
                    # (grab_x)**2 + (grab_y)**2 < 160**2:
                    # os.system("rosrun dashgo_tools move.py -0.02")
                    grab_y = -200
                    grab_x = 0
                    print("[GRAB] Out of max range")
                elif (grab_x) ** 2 + (grab_y) ** 2 < 160 ** 2:
                    # os.system("rosrun dashgo_tools move.py 0.04")
                    grab_y = -200
                    grab_x = 0
                    print("[GRAB] Out of min range")
                    # out = True
                else:
                    device.rotate_to(grab_angle, 0, 35, 0, True)  # 最佳抓取初始位置,否则会超出可执行空间
                    # device.move_to(grab_x, -200, camera_z, 0, True)


                    
                    # ----------------------------------------
                    # 抓取部分
                    device.move_to(
                        grab_x, grab_y, grab_z +35, grab_angle - angle, wait=True
                    )
                    device.suck(True)
                    device.move_to(
                        grab_x, grab_y, grab_z, grab_angle - angle, wait=True
                    )
                    
                    # time.sleep(0.2)
                    # (x, y, z, r, j1, j2, j3, j4) = device.pose()
                    # device.move_to(
                    #     grab_x, grab_y, grab_z +35, grab_angle - angle, True
                    # )
                    # -----------------------------------------
                    device.rotate_to(grab_angle, 0.0, 0.0, 0.0, True)
                    thread.join()
                    print("result = " + str(results[i]))
                    if locate(device, store, results[i]):
                        fulled = True

                        print("fulled")
                        device.suck(False)
                        break
                    # device.rotate_to(grab_angle, 0.0, 0.0, 0.0, True)
                    device.suck(False)
                    i+=1
                    print("========================================================")
                print("[INFO] move and ocr time passed"+str(time.clock() - start_time))
                # device._set_queued_cmd_clear()
                # device.clear_all_alarms_state()

                # break
                # time.sleep(5)
        except Exception:
            print(Exception)
    return store,finished


def grab_test():
    os.system("rm -rf ./images/results/*")
    for i in range(0, 4):
        try:
            port = list_ports.comports()[i].device
            device = Dobot(port=port, verbose=False)
        except:
            pass
        else:
            break
    device._set_queued_cmd_clear()
    device.clear_all_alarms_state()
    device.rotate_to(90, 0, 0, 0, wait=True)
    store = {
        "l1": [True, ""],
        "l2": [True, ""],
        "l3": [True, ""],
        "l4": [True, ""],
        # "m1": [True, ""],
        # "m2": [True, ""],
        # "m3": [True, ""],
        # "m4": [True, ""],
        "r1": [True, ""],
        "r2": [True, ""],
        "r3": [True, ""],
        "r4": [True, ""],
    }
    grab(device, store)
    device.rotate_to(90, 0.0, 0.0, 0.0, True)


if __name__ == "__main__":
    grab_test()
