from serial.tools import list_ports
from pydobot.dobot import Dobot
device = Dobot(port="/dev/dobot_arm", verbose=False)
device.suck(False)
# device._set_queued_cmd_clear()
# device.clear_all_alarms_state()
# device.go_home()
# device.rotate_to(90,-5,-15,50,wait=True) #highest position
device.rotate_to(-90,0,35,10,wait=True) #middle position
device.rotate_to(-90,0,35,50,wait=True) #middle position
# device.rotate_to(-90,40,0,40,wait=True) #long position
# device.move_to(326,0,0,0)
print(device.pose())
  
