#!/home/eaibot/anaconda3/envs/pro/bin/python
# from IPython import get_ipython
import time
import os
import tensorflow as tf
import cv2 as cv
import numpy as np
import sys
import joblib
from util.keys_prov import province #省份信息字典
# sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
from util.utils_cv import *
from util.keys_prov import *
from util.utils_recg import *
from ctpn.text_detect import text_detect
from imutils import perspective
from serial.tools import list_ports
from pydobot.dobot import Dobot
from drop_test import *

# import matplotlib.pyplot as plt
##################################################################
#寻找机械臂端口
##################################################################
for i in range(0,4):
    try:
        port = list_ports.comports()[i].device
        device = Dobot(port=port, verbose=False)
    except:
        pass
    else:
        break
device._set_queued_cmd_clear()
device.clear_all_alarms_state()


##################################################################
#寻找机械臂端口
##################################################################
drop_position = [-27.0,10,44.0,60.0,50.0]
store = joblib.load('./depository/store.pkl')
try:
    delivery_box = joblib.load('./depository/deliery_box.pkl')
except:
    delivery_box = []

def get_box_name(i):
    '''
    获取当前位置投递箱标签内容
    没有记录的，则进行识别
    '''
    global delivery_box
    # if delivery_box[0] is None:
    #     delivery_box = []
    try:
        if delivery_box[i-1] :
            return delivery_box[i-1]
    except:
        pass
    for j in range(1,10):
        try:
            cap = cv.VideoCapture(j)
            _,frame = cap.read()
            cv.imwrite('./test.jpg', frame)
            break
        except :
            print("camera not find")
    for j in range(30):
        _,frame = cap.read()
    # frame = frame[:][180:][:]
    # frame = cv.resize(frame,(213,160),interpolation=cv.INTER_AREA)
    frame = cv.copyMakeBorder(frame,200,200,200,200,cv.BORDER_CONSTANT,value=(0,0,0))
    text_recs, temp, img = text_detect(frame)
    cv.imwrite("./fox.jpg",temp)
    text_recs = sort_box(text_recs)
    result = crnnRec(img, text_recs, adjust=True)
    results= []
    for key in result:
        # print(result[key][1])
        results.append(result[key][1])
    true_result = None
    print(results)
    for key in province:
        for result in results:
            if result.find(province[key])>-1:
                true_result = province[key]
                break
    # for key in result:
    #     # print(result[key][1])
    #     results.append(result[key][1])
    if true_result is not None:
        delivery_box.append(true_result)

    print(delivery_box)
    # joblib.dump(delivery_box,'./depository/deliery_box.pkl')
    return true_result

def check_store(name):
    global delivery_box
    '''
    检查当前是否有匹配的盒子
    '''
    for i in range(4,0,-1):
        if not store['r'+ str(i)][0] and store['r'+ str(i)][1]==name:
            return True
        if not store['l'+ str(i)][0] and store['l'+ str(i)][1]==name:
            return True
    return False


box_name = joblib.load('./box_name.pkl')

if box_name !='VERTIVAL' and box_name != 'ROTATE':
    os.system("rosrun mrobot_navigation to_delivery_1.py")
    for i in range(1,5):
        os.system("rosrun mrobot_navigation rotate_self.py")
        os.system("rosrun dashgo_tools move.py 0.3")
        name = None
        # i = 0
        name = get_box_name(i)
        while name is None:
            os.system("rosrun dashgo_tools move.py -0.2")
            # os.system("rosrun mrobot_navigation to_delivery_"+str(i)+".py")
            name = get_box_name(i)
            # os.system("rosrun mrobot_navigation to_delivery_"+str(i)+".py")
        if check_store(name):
            os.system("rosrun dashgo_tools rotate.py -90")
            drop_by_name(name)
            print("done")
            if i !=4:
                os.system("rosrun dashgo_tools rotate.py 90")
                os.system("rosrun mrobot_navigation rotate_self.py")
            # else:
            #     os.system("rosrun dashgo_tools rotate.py -90")
else:
    os.system("rosrun mrobot_navigation to_delivery_2.py")
    for i in range(1,4):
        os.system("rosrun mrobot_navigation rotate_self.py")
        os.system("rosrun dashgo_tools move.py 0.3")
        name = None
        # i = 0
        name = get_box_name(i)
        while name is None:
            os.system("rosrun dashgo_tools move.py -0.05")
            # os.system("rosrun mrobot_navigation to_delivery_"+str(i)+".py")
            name = get_box_name(i)
            # os.system("rosrun mrobot_navigation to_delivery_"+str(i)+".py")
        if check_store(name):
            os.system("rosrun dashgo_tools rotate.py -90")
            drop_by_name(name)
            print("done")
            if i !=4:
                os.system("rosrun dashgo_tools rotate.py 90")
                os.system("rosrun mrobot_navigation rotate_self.py")
            # else:
            #     os.system("rosrun dashgo_tools rotate.py -90"
    drop_by_name("test")

joblib.dump(delivery_box,'./depository/deliery_box.pkl')




