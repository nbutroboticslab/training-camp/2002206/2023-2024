#!/home/eaibot/anaconda3/envs/pro/bin/python
import cv2 as cv
import math
import time
import joblib
from serial.tools import list_ports
from pydobot.dobot import Dobot
import numpy as np
import os
import sys

for i in range(0,4):
    try:
        port = list_ports.comports()[i].device
        device = Dobot(port=port, verbose=False)
    except:
        pass
    else:
        break
device._set_queued_cmd_clear()
device.clear_all_alarms_state()

# 仓库空间和放置高度
store = {"l1":True, "l2":True, "r1":True, "r2":True}
# print(store)
store_position = [None, 30, 30, 30, 55]


# device.rotate_to(-90.0, 0.0, 0.0, 0, True)
def locate():
    cap = cv.VideoCapture(0)
    for i in range(0,30):
        _, origin_img = cap.read()
    #origin_img = cv.imread("./test2.jpg")
    blur_img = cv.blur(origin_img, (4, 4))
    # 直方图均值化
    lab = cv.cvtColor(blur_img, cv.COLOR_BGR2LAB)
    lab_planes = cv.split(lab)
    clahe = cv.createCLAHE(clipLimit=3.0, tileGridSize=(8, 8))
    lab_planes[0] = clahe.apply(lab_planes[0])
    lab = cv.merge(lab_planes)
    clahe_img = cv.cvtColor(lab, cv.COLOR_LAB2BGR)
    # hsv的下红色范围
    hsv = cv.cvtColor(clahe_img, cv.COLOR_BGR2HSV)

    ################################################33
    lower_blue = np.array([101, 79, 75])
    upper_blue = np.array([139, 255, 255])
    mask_blue = cv.inRange(hsv, lower_blue, upper_blue)
    lower_green = np.array([49, 78, 88])
    upper_green = np.array([92, 255, 255])
    mask_green = cv.inRange(hsv, lower_green, upper_green)
    lower_red = np.array([0, 176, 40])
    upper_red = np.array([69, 255, 255])
    mask_red = cv.inRange(hsv, lower_red, upper_red)
    #############################################

    img, contours, _ = cv.findContours(
            mask_red, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    all = cv.bitwise_or(mask_blue,mask_green)
    all = cv.bitwise_or(all,mask_red)
    cv.imwrite("test_bwimg.jpg",all)
    rects = list()
    for c in contours:
        rect = cv.minAreaRect(c)
        width, height = rect[1][0], rect[1][1]
        if width*height >200:
            rects.append(rect)

    results = list()
    y, x = origin_img.shape[:2]
    # test_imshow(test_img, cv_show=False)
    for rec in rects:
        position = rec[0]
        relative_y = (position[1]-y/2)/y*200
        relative_x = -(position[0]-x/2)/x*200
        result.append([relative_x, relative_y])


    return results

def store():
    # device.rotate_to(90.0, 0.0, 0.0, -90, True)  # 90是放置方向,r=相反

    for i in range(1, 3):
        if store['l' + str(i)][0]:
            device.rotate_to(110.0, 0.0, 0.0, -90, True)
            device.move_to(-68, 167, store_position[i+1], 0)
            device.move_to(-68, 167, store_position[i], 0)
            store['l' + str(i)] = False
            print("store to l"+str(i))
            break
        if store['r' + str(i)]:
            device.rotate_to(70.0, 0.0, 0.0, -90, True)
            device.move_to(68, 167, store_position[i+1], 0)
            device.move_to(68, 167, store_position[i], 0)
            store['r' + str(i)]= False
            print("store to r"+str(i))
            break
    device.suck(False)
    print("--------------grabed-----------------")
    # time.sleep(3)
    device.rotate_to( 90.0, 0.0, 0.0, 0.0, True)

grab_z = -45
camera_z = 35
adjust_coefficient_x = 1.095
adjust_coefficient_y = 1.05

base_y = -206.70 - 15
base_x = 2
grab_angle = -90
device.rotate_to(grab_angle, 0.0, 0.0, 0, True)

positions= locate()  # 拍照，寻找标签
print("find "+str(len(angles))+" box")
for position in position:  # 对于每一个标签
    print("========================================================")
    print("Test of "+str(grab_angle)+" in simple "+str(i))
    device.rotate_to(-90, 0.0, 0.0, 0, True)  # 初始位置
    position = positions[i]  # 标签位置

    # print(str(i)+" r_angle "+str(angle))
    # print("position="+str(position))
    x, y = position
    grab_x = x
    grab_y = y

    grab_x = grab_x*adjust_coefficient_x + base_x  # 加权
    grab_y = grab_y*adjust_coefficient_y + base_y
    # ---------注意，base值不需要加权，因为已经在笛卡尔坐标系内了

    # 调整摄像头与吸盘相对距离用角度值
    camera_degree = abs(math.degrees(math.atan(grab_y/grab_x)))
    if grab_x < 0:
        grab_position_adjust_x = -math.cos(math.radians(camera_degree))*67
    else:
        grab_position_adjust_x = math.cos(math.radians(camera_degree))*67
    grab_position_adjust_y = -math.sin(math.radians(camera_degree))*67

    print("angle="+str(camera_degree))
    print("grabx= "+str(grab_x)+" graby="+ str(grab_y))
    print("true_grabx= "+str(grab_x+grab_position_adjust_x)+" true_graby="+ str(grab_y+grab_position_adjust_y))
    print("true_distance= "+str(math.sqrt( (grab_x+grab_position_adjust_x)**2 + (grab_y+grab_position_adjust_y)**2)))
    # 最大角度
    if grab_y < 0 and grab_x < 0 and abs(camera_degree) < 55:
        grab_y = -200
        grab_x = 0
        print("Out of angle")
        # out = True
    # 最大距离与最小距离
    elif (grab_x+grab_position_adjust_x)**2 + (grab_y+grab_position_adjust_y)**2 > 314**2 :
            # (grab_x)**2 + (grab_y)**2 < 160**2:
        # os.system("rosrun dashgo_tools move.py -0.02")
        grab_y = -200
        grab_x = 0
        print("Out of range")
    elif (grab_x)**2 + (grab_y)**2 <160**2:
        os.system("rosrun dashgo_tools move.py 0.04")
        grab_y = -200
        grab_x = 0
        print("Out of range")
        # out = True
    else:
        # print("grabx,graby = "+str(grab_x)+str(grab_y))
        # print(grab_x,grab_y)
        if angle > 45:  # 因为没有必要保证标签水平，因此尽量减小旋转的角度
            angle = angle - 90
        # (x, y, z, r,  j1, j2, j3, j4) = device.pose()
        # print("-grab_angle+angle="+str(grab_angle+angle))
        device.move_to(grab_x, -200, camera_z, 0, True)
        device.move_to(grab_x, grab_y, camera_z, grab_angle+angle, wait=True)
        time.sleep(0.5)# 稳定机械臂
        # print(device.pose()[:3])
        # true_result = "test"
        # ----------------------------------------
        # 抓取部分
        device.move_to(grab_x+grab_position_adjust_x, grab_y +
                    grab_position_adjust_y, camera_z, grab_angle+angle, wait=True)
        device.move_to(grab_x+grab_position_adjust_x+2, grab_y +
                    grab_position_adjust_y-9, grab_z, grab_angle+angle, wait=True)
        device.suck(True)
        time.sleep(0.5)
        # (x, y, z, r, j1, j2, j3, j4) = device.pose()
        device.move_to(grab_x+grab_position_adjust_x, grab_y +
                    grab_position_adjust_y, camera_z, grab_angle+angle, True)
        # -----------------------------------------
        device.rotate_to(grab_angle, 0.0, 0.0, 0.0, True)
        store()
        keep = True
        # device.rotate_to(grab_angle, 0.0, 0.0, 0.0, True)
        device.suck(False)
    device._set_queued_cmd_clear()
    device.clear_all_alarms_state()


# device.rotate_to(90.0, 0.0, 0.0, 0.0, True)
device.rotate_to(90.0, 0.0, 0.0, 0.0, True)
# joblib.dump(store, './depository/store.pkl')
