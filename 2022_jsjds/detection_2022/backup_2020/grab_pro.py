#!/home/eaibot/anaconda3/envs/pro/bin/python

from 2021_cv import get_info, get_address
import math
import time
import joblib
from util.keys_prov import province #省份信息字典
from serial.tools import list_ports
from pydobot.dobot import Dobot
import os
import sys
os.system('rosnode kill /DobotServer ')
os.system('rosnode kill /usb_cam ')

for i in range(0,4):
    try:
        port = list_ports.comports()[i].device
        device = Dobot(port=port, verbose=False)
    except:
        pass
    else:
        break
device._set_queued_cmd_clear()
device.clear_all_alarms_state()

# 仓库空间和放置高度
if len(sys.argv)>1 and sys.argv[1] == 'left':
    store = joblib.load('./depository/store.pkl')
    # delivery_box = []
else:
    store = {"l1": [True, ""], "l2": [True, ""], "l3": [True, ""], "l4": [
        True, ""], "r1": [True, ""], "r2": [True, ""], "r3": [True, ""], "r4": [True, ""]}
print(store)
# store = {"l1": [True, ""], "l2": [True, ""], "l3": [True, ""], "l4": [
#     True, ""], "r1": [True, ""], "r2": [True, ""], "r3": [True, ""], "r4": [True, ""]}
store_position = [None, 30, 30, 30, 55]
# device.go_home()
# print(device.pose())


# device.rotate_to(-90.0, 0.0, 0.0, 0, True)

def locate(result):
    full = False
    # device.rotate_to(90.0, 0.0, 0.0, -90, True)  # 90是放置方向,r=相反

    for i in range(1, 4):
        if store['l' + str(i)][0]:
            device.rotate_to(110.0, 0.0, 0.0, -90, True)
            device.move_to(-68, 167, store_position[i+1], 0)
            device.move_to(-68, 167, store_position[i], 0)
            store['l' + str(i)][0] = False
            store['l' + str(i)][1] = result
            print("store "+ str(result)+" to l"+str(i))
            break
        if store['r' + str(i)][0]:
            device.rotate_to(70.0, 0.0, 0.0, -90, True)
            device.move_to(68, 167, store_position[i+1], 0)
            device.move_to(68, 167, store_position[i], 0)
            store['r' + str(i)][0] = False
            store['r' + str(i)][1] = result
            print("store "+ str(result)+" to r"+str(i))
            full = (i==2)
            break
    device.suck(False)
    print("--------------grabed-----------------")
    # time.sleep(3)
    device.rotate_to( 90.0, 0.0, 0.0, 0.0, True)
    return full

grab_z = -45
camera_z = 35
adjust_coefficient_x = 1.095
adjust_coefficient_y = 1.05
keep  = True
out = False
while keep :
    keep = False
    out = False
    if len(sys.argv)>1 :
        if str(sys.argv[1]) == 'right':
            rot = range(1,3)
        else:
            rot = range(0,2)
    else:
        rot = range(0,3)
    for j in rot:  # 三个角度抓取
        # while True:
        # j = 1
        if j == 0:
            # 注意，base值需要考虑摄像头和吸盘的相对位置
            base_y = -179.00 - 11  # 初始位置Y值 -9
            base_x = -103.34 - 7  # 初始位置X值  -6
            grab_angle = -120

        elif j == 1:
            base_y = -206.70 - 15
            base_x = 2
            grab_angle = -90
        else:
            base_y = -179.00 - 10     #-18
            base_x = 103.34 + 10      #+10
            grab_angle = -60
        device.rotate_to(grab_angle, 0.0, 0.0, 0, True)

        positions, angles = get_info(show_img=True)  # 拍照，寻找标签
        print("find "+str(len(angles))+" box")
        for i in range(0, len(angles)):  # 对于每一个标签
            print("========================================================")
            print("Test of "+str(grab_angle)+" in simple "+str(i))
            device.rotate_to(grab_angle, 0.0, 0.0, 0, True)  # 初始位置
            position = positions[i]  # 标签位置
            angle = -angles[i]  # 标签与水平线夹角，返回值为负

            # print(str(i)+" r_angle "+str(angle))
            # print("position="+str(position))
            x, y = position
            sin = math.sin(math.radians(-(90+grab_angle)))
            cos = math.cos(math.radians(-(90+grab_angle)))
            if j != 1:  # 坐标系旋转30度
                grab_x = cos*x + sin*y
                grab_y = cos*y - sin*x
            else:
                grab_x = x
                grab_y = y

            grab_x = grab_x*adjust_coefficient_x + base_x  # 加权
            grab_y = grab_y*adjust_coefficient_y + base_y
            # ---------注意，base值不需要加权，因为已经在笛卡尔坐标系内了

            # 调整摄像头与吸盘相对距离用角度值
            camera_degree = abs(math.degrees(math.atan(grab_y/grab_x)))
            if grab_x < 0:
                grab_position_adjust_x = -math.cos(math.radians(camera_degree))*67
            else:
                grab_position_adjust_x = math.cos(math.radians(camera_degree))*67
            grab_position_adjust_y = -math.sin(math.radians(camera_degree))*67

            print("angle="+str(camera_degree))
            print("grabx= "+str(grab_x)+" graby="+ str(grab_y))
            print("true_grabx= "+str(grab_x+grab_position_adjust_x)+" true_graby="+ str(grab_y+grab_position_adjust_y))
            print("true_distance= "+str(math.sqrt( (grab_x+grab_position_adjust_x)**2 + (grab_y+grab_position_adjust_y)**2)))
            # 最大角度
            if grab_y < 0 and grab_x < 0 and abs(camera_degree) < 55:
                grab_y = -200
                grab_x = 0
                print("Out of angle")
                # out = True
            # 最大距离与最小距离
            elif (grab_x+grab_position_adjust_x)**2 + (grab_y+grab_position_adjust_y)**2 > 314**2 :
                    # (grab_x)**2 + (grab_y)**2 < 160**2:
                # os.system("rosrun dashgo_tools move.py -0.02")
                grab_y = -200
                grab_x = 0
                print("Out of range")
            elif (grab_x)**2 + (grab_y)**2 <160**2:
                os.system("rosrun dashgo_tools move.py 0.04")
                grab_y = -200
                grab_x = 0
                print("Out of range")
                # out = True
            else:
                # print("grabx,graby = "+str(grab_x)+str(grab_y))
                # print(grab_x,grab_y)
                if angle > 45:  # 因为没有必要保证标签水平，因此尽量减小旋转的角度
                    angle = angle - 90
                # (x, y, z, r,  j1, j2, j3, j4) = device.pose()
                # print("-grab_angle+angle="+str(grab_angle+angle))
                device.move_to(grab_x, -200, camera_z, 0, True)
                device.move_to(grab_x, grab_y, camera_z, grab_angle+angle, wait=True)
                time.sleep(0.5)# 稳定机械臂
                # print(device.pose()[:3])
                results = get_address(True)
                true_result = None
                for key in province:
                    for result in results:
                        if result.find(key)>-1:
                            true_result = province[key]
                            break
                # true_result = "test"
                if true_result is not None:

                    print("result = "+str(true_result))
                    # ----------------------------------------
                    # 抓取部分
                    device.move_to(grab_x+grab_position_adjust_x, grab_y +
                                grab_position_adjust_y, camera_z, grab_angle+angle, wait=True)
                    device.move_to(grab_x+grab_position_adjust_x+2, grab_y +
                                grab_position_adjust_y-9, grab_z, grab_angle+angle, wait=True)
                    device.suck(True)
                    time.sleep(0.5)
                    # (x, y, z, r, j1, j2, j3, j4) = device.pose()
                    device.move_to(grab_x+grab_position_adjust_x, grab_y +
                                grab_position_adjust_y, camera_z, grab_angle+angle, True)
                    # -----------------------------------------
                    device.rotate_to(grab_angle, 0.0, 0.0, 0.0, True)
                    if  locate(true_result):
                        print("fulled")
                        device.suck(False)
                        # keep = False
                        break
                    keep = True
                    # device.rotate_to(grab_angle, 0.0, 0.0, 0.0, True)
                    device.suck(False)
                else:
                    i -=1
            device._set_queued_cmd_clear()
            device.clear_all_alarms_state()

            # break
            # time.sleep(5)
        # device.rotate_to(grab_angle, 0.0, 0.0, 0.0, True)

# device.rotate_to(90.0, 0.0, 0.0, 0.0, True)
device.rotate_to(90.0, 0.0, 0.0, 0.0, True)
joblib.dump(store, './depository/store.pkl')
