
from IPython import get_ipython
import time
import os
import tensorflow as tf
import cv2 as cv
import numpy as np
import sys
import random
# sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')

from util.utils_cv import *
from util.keys_prov import *
from util.utils_recg import *
from ctpn.text_detect import text_detect
from imutils import perspective
from angle.predict import predict
import matplotlib.pyplot as plt

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

def get_address_new(img,show_img):
    labels = []
    results = []
    result = None
    angle = predict(img=img)
    print(angle)
    img = rotated_img(img,angle)
    text_recs, temp, img = text_detect(img)

    text_recs = sort_box(text_recs)
    result = crnnRec(img, text_recs, adjust=True)
    if show_img:
        # test_imshow(img,cv_show=True,save_img=False)
        # test_imshow(label)
        test_imshow(temp,cv_show=True,save_img=False,wait=1000)
        pass
    for key in result:
        # print(result[key][1])
        results.append(result[key][1])
    true_result = []
    for r in results:
        for key in province:
            if r.find(key)>-1 or r.find(province[key])>-1:
                print(r)
                true_result.append(province[key]) 
    # true_result = "test"
    if true_result is not None:
        print("result = "+str(true_result))
    print(results)
    return results,true_result
# else:
#     print("no result")
#     return ["no result"]
if __name__ == '__main__':
    for i in range(40):
        img = str(random.randint(0,480)).zfill(3)+".jpg"
        print(img)
        origin_img = cv.imread("/home/fox/Documents/DataArea/comp_data/pic/box/" + img)
        get_address_new(origin_img,True)