python解释器： conday3(shell命令) （env：pac_2021)
运行前：source ../ros_base/devle/setup.sh
主要入口代码：process_2021.py
识别代码:cv_2021.py
    文字识别框架:./paddleocr
    识别辅助代码:util/utils_cv.py
    省份信息:util/utils_prov.py
    深度摄像头测试代码:./realsense
抓取执行代码:grab_2021.py
    机械臂控制: ./pydobotkeo

## USB映射：
    插拔USB，通过 lsusb 查看前后的设备对象不同来确定USB设备号

###  三种USB查看方式： 
- lsusb  
- ls /sys/class/tty/ttyUSB* -l  
- udevadm info --attribute-walk --name=/dev/ttyUSB2  

### USB串口重映射流程
1. 在  /etc/udev/rules.d/  创建规则文件  
添加规范如：dobot_arm.rules  
`ACTION=="add",KERNELS=="2-1.2:1.0",SUBSYSTEMS=="usb",MODE:="0777", SYMLINK+="dobot_arm"`  
2-5.1:1.0
SYMLINK 重映射后的名称  
MODE 权限  [解释](https://cloud.tencent.com/developer/article/1796771)  
KERNELS 可当作用于区别设备的一种标志符号，  
2. 通过插拔和指令`lsusb`确认该设备的默认名称，如 ttyUSB0,video6等，  
然后 通过 `udevadm info --attribute-walk --name=/dev/ttyUSB0` ，记得用对应的名称替换 ttyUSB0  
<details>
<summary>展开查看部分指令结果</summary>
<pre><code>
  looking at device '/devices/pci0000:00/0000:00:14.0/usb2/2-1/2-1.2/2-1.2:1.0/ttyUSB0/tty/ttyUSB0':
    KERNEL=="ttyUSB0"
    SUBSYSTEM=="tty"
    DRIVER==""
</code>
</details>

3. 通过第一个looking可看到 `2-1.2:1.0`,即上面的 KERNELS,写入文件保存  
运行以下指令后，   
    `sudo service udev reload  `  
   ` sudo service udev restart `   
并重启机器，使用 `ls /dev/dobot_arm -l `  
看到以下内容，重映射成功  
`lrwxrwxrwx 1 root root 7 4月  18 16:05 /dev/dobot_arm -> ttyUSB0`  

4. 目前针对RGB摄像头的video6,也尝试重映射，但映射对象为 input/event5,无法使用。  
KERNEL=="video*", ATTRS{idVendor}=="10c4", ATTRS{idProduct}=="ea60",ATTRS{index}="0" ,MODE:="0666", SYMLINK+="rgb_camera"

ls /dev/dobot_arm  2-6:1.0
ls /dev/rplidar_laser   2-2:1.0
ls /dev/wheeltec_controller
ls /dev/single_laser

ls /dev/rplidar  2-2.3:1.0  2-5.3:1.0