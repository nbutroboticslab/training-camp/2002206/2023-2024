import sys

try:
    sys.path.remove('/opt/ros/melodic/lib/python2.7/dist-packages')
except:
    pass
import cv2 as cv
import numpy as np
import pyrealsense2.pyrealsense2 as rs

# from paddleocr.paddleocr import PaddleOCR
from ocr import OcrHandle
from util.keys_prov import *
from util.utils_cv import test_imshow,crop_rotate_box,calculate_centre
import json
import cv2
is_save_img = True

# jsonObj = json.load(open("./ShortRangePreset.json"))
# jsonObj = json.load(open("./DefaultPreset_D415.json"))
# jsonObj = json.load(open("./HighAccuracyPreset.json"))

# json_string = str(jsonObj).replace("'",'\"')
pipeline = rs.pipeline()
config = rs.config()
pipeline_wrapper = rs.pipeline_wrapper(pipeline)
pipeline_profile = config.resolve(pipeline_wrapper)
device = pipeline_profile.get_device()
device_product_line = str(device.get_info(rs.camera_info.product_line))

config.enable_stream(rs.stream.depth, 640, 480,
                     rs.format.z16, 6)  # 不设置像素会导致无法识别近距离内容
# config.enable_stream(rs.stream.color, 1920, 1080, rs.format.bgr8, 6)
config.enable_stream(rs.stream.color, 1280, 720, rs.format.bgr8, 6)
clipping_distance_in_meters = 0.325  # 1 meter

profile = pipeline.start(config)
# dev = profile.get_device()
# advnc_mode = rs.rs400_advanced_mode(dev)
# advnc_mode.load_json(json_string) # 加载json文件配置


depth_sensor = profile.get_device().first_depth_sensor()
depth_scale = depth_sensor.get_depth_scale()

depth_profile = rs.video_stream_profile(
    profile.get_stream(rs.stream.depth))
depth_intrinsics = depth_profile.get_intrinsics()
print("Depth Scale is: ", depth_scale)
# We will be removing the background of objects more than
#  clipping_distance_in_meters meters away

clipping_distance = clipping_distance_in_meters / depth_scale

# Create an align object
# rs.align allows us to perform alignment of depth frames to others frames
# The "align_to" is the stream type to which we plan to align depth frames.
align_to = rs.stream.color
align = rs.align(align_to)

ocrhandle = OcrHandle()

cap = cv.VideoCapture("/dev/video2")
for _ in range(30):
    pipeline.wait_for_frames()

pic_num = 0
def get_boxes_info():
    # TODO:盒子紧靠时的识别
    # TODO:多次检测以获取平均值
    # Start streaming
    
    # Get frameset of color and depth
    frames = pipeline.wait_for_frames()
    # frames.get_depth_frame() is a 640x360 depth image

    # Align the depth frame to color frame
    aligned_frames = align.process(frames)

    # Get aligned frames
    # aligned_depth_frame is a 640x480 depth image
    aligned_depth_frame = aligned_frames.get_depth_frame()
    color_frame = aligned_frames.get_color_frame()
    color_image = np.asanyarray(color_frame.get_data())
    
    depth_intrinsics = color_frame.profile.as_video_stream_profile().intrinsics  # 需要使用彩色相机的内参
    # Validate that both frames are valid
    # if not aligned_depth_frame or not color_frame:
    #     continue
    depth_image = np.asanyarray(aligned_depth_frame.get_data())
    test_imshow(depth_image,save_img=is_save_img,name="full depth")
    # Remove background - Set pixels further than clipping_distance to black
    # np.where(condition, x, y) 满足条件(condition)，输出x，不满足输出y。
    bg_removed = np.where(
        (depth_image > clipping_distance) | (depth_image <= 0), 0, 255)
    bg_removed = np.asfarray(bg_removed).astype(np.uint8)
    # test_imshow(bg_removed,save_img=is_save_img,name="erode_origin")
    bg_removed = cv.erode(bg_removed, cv.getStructuringElement(cv.MORPH_RECT,(7,7)),iterations=3)

    #测试用
    bag = np.asanyarray(cv.cvtColor(bg_removed,cv.COLOR_GRAY2BGR)).astype(np.uint8)
    global pic_num
    cv2.imwrite("/home/tdb2/2022_jsjds/detection_2022/test/img/{}_S.jpg".format(pic_num),bag)
    pic_num+=1
    # 获取每个盒子的坐标
    contours, _ = cv.findContours(
        bg_removed, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    print("cont:",len(contours))
    # print("processing : "+ str(i))
    # test_imshow(color_image,save_img=is_save_img,name="origin")
    
    boxes = []
    rects = []

    for c in contours:
        rect = cv.minAreaRect(c)
        # rect[0]:中心点坐标xy
        # rect[1]:width 与 height
        # rect[2]:角度
        if rect[1][0] > 400 and rect[1][1] > 400 and rect[1][0]/rect[1][1]>0.8 and rect[1][0]/rect[1][1]<1.2:
            new_rect = [rect[0],(450,450),rect[2]]
            #在标准1920p图片中,距离约30cm的包装盒在图像中有450像素边长,在这固定此大小以避免深度信息中面积大小不精确的问题.
            # 获取轮廓四角坐标
            box = cv.boxPoints(new_rect)
            box = np.int0(box)
            # 插入首位
            rects.insert(0,box)
            coord = rs.rs2_deproject_pixel_to_point(
            depth_intrinsics, rect[0], (depth_image[int(rect[0][1]), int(rect[0][0])]))
            # print(coord)
            boxes.insert(0,[coord,rect[2]])
            print(coord)
            cv.drawContours(color_image,[box],0,(0,0,255),5)
            cv.circle(color_image,(int(rect[0][0]),int(rect[0][1])),9,(255,0,0),5)
    test_imshow(cv.addWeighted(color_image,1.0,bag,0.5,0),save_img=is_save_img,name="depth ")
    # pipeline.stop()
    # [现实三维坐标，偏差角度], 彩色图像， [所有邮件轮廓四角坐标]
    return boxes,color_image,rects


'''
description: 获取所有轮廓的识别结果列表
param {*} results 存储所有结果
param {*} color_image 原图
param {*} rects 邮件轮廓框列表（可能性列表）
return {*}
'''
def get_boxes_address(results,color_image,rects):
    for box in rects:
        results.append(get_address_crop(color_image,box))
    # pipeline.stop()


def pipeline_stop():
    global pipeline
    pipeline.stop()


'''
description: 获取单个轮廓识别结果
param {*} ori_img
param {*} box  单个轮廓框（可能是邮件）
return {*}
'''
def get_address_crop(ori_img,box):
    '''
    从深度相机的colour图像中裁减盒子的图像以摆正,进行识别
    '''
    #TODO:现在区分收件与寄件地址是通过大小进行的,可能存在潜在的错误事件
    true_result = []
    kernel = np.array([[0, -1,0],[-1,5,-1],[0,-1,0]],np.float32)
    for angle in range(0,361,90):
        # print ("angle = " + str(angle))
        box_img = crop_rotate_box(box,ori_img,angle=angle)
        # 缩减图像，                          截取 中部图像                            截取左侧一部分图像
        img = box_img[int(box_img.shape[0]*0.25):int(box_img.shape[0]*0.5), :int(box_img.shape[0]*0.35)]
        # 图像锐化
        img = cv.filter2D(img,-1,kernel=kernel)
        # 向图像四个方向衍生边框，以镜像模式
        img = cv.copyMakeBorder(img,100,100,100,100,cv.BORDER_REPLICATE)
        # 文字识别 
        dt_boxes, txts,_ = ocrhandle.text_predict(img, short_size=32*(300//32))# short_size 必须为32的倍数
        # dt_boxes, rec_res,angle_list = paddle(img)
        # txts = [rec_res[i][0] for i in range(len(rec_res))]
        # dt_boxes,txts = filte_box_by_coor(dt_boxes,txts,img.shape[0]) #按y坐标高度过滤
        results = txts
        if is_save_img:
            for dt_box in dt_boxes:
                dt_box = np.int0(dt_box)
                cv.drawContours(img,[dt_box],0,(0,0,255),5)
        # print(txts[:2])
        flag = False
        for key in province:
            if flag:
                break
            for result in results:
                if result.find(province[key])>-1 or result.find(key)>-1:
                    true_result.append(province[key])
                    # print(true_result)
                    flag=True
                    break
        if len(true_result)>0:
            # TODO  此处应该优化为可信度最高的结果
            # 取出现次数最多的结果 
            true_result = max(true_result,key=true_result.count)
            test_imshow(img,save_img=is_save_img,name="croped "+str(true_result))
            test_imshow(box_img,save_img=is_save_img,name="box "+str(true_result))
            # print(true_result)
            return true_result
        # test_imshow(img,save_img=is_save_img)
    
    return 'unknown'
    
def get_box_name(recongnize_point:int,drop_boxes):
    #TODO: 未测试
    '''
    获取当前位置投递箱标签内容
    没有记录的，则进行识别
    input: recongnize_point 用于输出调试信息
    '''
    for _ in range(20):
        _,frame = cap.read()
    # mark: frame初始比例是多少？测算short_size
    # print("frame inital size：" +str(frame.shape))  height width channel
    frame = frame[int(frame.shape[0]*0.4):int(frame.shape[0]*0.7),:] # 聚焦目标，截取自x轴 height*0.4～ height*0.7之间的图像
    test_imshow(frame,save_img=is_save_img,name="box_name")
    dt_boxes, txts,angle_list = ocrhandle.text_predict(frame,short_size=32*(250//32))
    print(txts)
    # dt_boxes, rec_res,angle_list = paddle(frame)
    # txts = [rec_res[i][0] for i in range(len(rec_res))]
    left_result = "unknown"
    right_result = "unknown"
    for key in province:
        for i,result in enumerate(txts):
            if result.find(province[key])>-1:  # province 为“北京市”：“北京” -> 键：值, key 为北京市，result.find(“北京”)
                if calculate_centre(dt_boxes[i])[0]>0.5*frame.shape[1]:  # 判断识别文字在左框还是右框
                    right_result = province[key]
                else:
                    left_result = province[key]
                #     true_result.append(province[key]) 
    # print(str(left_result+" "+str(right_result)))
    # 保存底盘相机 识别地域盒(目标点)图片
    test_imshow(frame,save_img=is_save_img,name="box_name_"+str(recongnize_point)+"_"+left_result+"_"+right_result)
    drop_boxes[recongnize_point*2:recongnize_point*2+2] = left_result,right_result
    return left_result,right_result

if __name__ == '__main__':
    results = []
    drop_boxes = ["unknown"] * 10
    # origin_img = cv.imread("./img/019.jpg")
    # get_address_new(origin_img,True)
    # get_address()


    boxes,color_image,rects = get_boxes_info()
    print(boxes)
    get_boxes_address(results,color_image,rects)
    print(results)

    # get_box_name(0,drop_boxes)