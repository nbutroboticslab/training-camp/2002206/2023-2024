#-*- coding: UTF-8 -*-
#!/usr/bin/python

"""
需要预先运行指令：
roslaunch dashgo_nav navigation_2023_original.launch

对应文件在
/home/eaibot/2022_jsjds/dashgo_ws/src/dashgo/dashgo_nav/launch/include

建图指令：
rosmap
开启rviz后,引导车进行建图
建图完成后
新开一个终端，运行
roslaunch dashgo_nav map_save.launch

初始化操作
setros2023

地图文件在
/home/eaibot/2022_jsjds/dashgo_ws/src/dashgo/dashgo_nav/2022_map


"""

import rospy
# 导航
# from ros.navigation import Navigation
from ros.navigation import Navigation
# 简单位移
from ros.Control import Control
# from util.utils_cv import *
# from util.utils_process import *
# 多线程
import threading
from threading import Thread
# 拍摄图片
from cv_2023 import *
# 文字识别
# from ocr.detect_test import *
# 雷达测距
# from ros.get_laser2 import adopt_forward
from single_laser_copy import *
from arm_control_2023 import *
# 日志记录模块
# from log.pylog import log
# LOGS_INFO = log.info
# 机械臂抓取动作
# from dobot_arm import *
import time

from pydobot.dobot import Dobot # 机械臂

sentinel = threading.Event()
sentinel.set()
rospy.init_node("process", anonymous=True,disable_signals=True)
navigation = Navigation()
control = Control()
control_forward = Thread(target=control.forward,args=[] )
subscribe_main_thread = Thread(target=subscribe_main,args=[])
subscribe_main_thread.start()
# looking_up_thread = Thread(target=looking_up,args=[])
# looking__down_thread = Thread(target=looking_down,args=[])
# subscribe_main_thread = Thread(target=subscribe_main,args=[])
# subscribe_main_thread.start()

# 机械臂动作线程
moving_thread = Thread(target=moving,args=[])
# put_to_left_thread = Thread(target=put_to_left,args=[])
# put_to_right_thread=Thread(target=put_to_right,args=[])
# get_from_left_thread = Thread(target=get_from_left,args=[])
# get_from_right_thread = Thread(target=get_from_right,args=[])
# put_into_thread = Thread(target=put_into,args=[])

# goods = [[0.264,-1.850,-135], [0.150,-0.700,183],[0.200,-0.350,-135],
#                 [-0.020,-0.150,-90], [-0.750,-0.150,-90],
#                 [-0.880,-0.500,-45], [-0.800, -0.790,0],[-0.868,-1.220,-45]
#                  ,[-0.750,-3.385,0.000]
#                 ]

# goods0 = [0.190, -1.980, -135]
good2shelf_main_Point = [0.175 ,-1.400, -107.35]
# good2shelf_main_Point = [-0.283, -3.022, 76.29]
# 从投放/出发侧移动到货架侧
def good2shelf():
    # navigation.send_position([0.155 ,-1.000],-90)
    # navigation.send_position([-0.295, -3.000],-90)
    target_x, target_y, target_ang = good2shelf_main_Point
    navigation.send_position([target_x, target_y],target_ang)

    dis_to_shelf = 1.5
    control.forward(dis_to_shelf)

    target_x, target_y, target_ang = she[2][1], goods[8][0], 0
    send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
    send_position_thread.start()

# 从货架侧 移动到 出发侧/投放侧
def shelf2good():
    navigation.send_position([(she[3][0]*3+she[2][0])/4,goods[8][1]-(she[1][0] - she[0][0])], 70 )
    dis_to_goods = 2.0
    control.forward(dis_to_goods)

    target_x, target_y, target_ang = goods[1][0], goods[3][1], 180
    send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
    send_position_thread.start()
    rospy.sleep(3)

# 从货架侧 移动到 园台旁边/地图中间
def shelf2mid():
    target_x, target_y, target_ang = -0.295, -3.000, 90
    navigation.send_position([target_x, target_y],target_ang)

    dis_to_mid = 1.0
    control.forward(dis_to_mid)

    target_x, target_y, target_ang = -0.987, -1.938, 147.2
    send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
    send_position_thread.start()
    rospy.sleep(5)
# 从直角侧 移动到 中心
def L2mid():
    target_x, target_y, target_ang =-0.946, -1.9050, -20
    # target_x, target_y, target_ang = -0.811, -1.697, -39.84
    navigation.send_position([target_x, target_y],target_ang)

    control.forward(0.8)
# good2shelf()
# shelf2mid()
# L2mid()
# rospy.sleep(20)
# goods = [[0.120, -1.638, 180], [0.090 ,-0.790, 177],[0.192, -0.410,-135],# 0,1,2
#                 [-0.032, -0.170,-90], [-0.790,-0.175,-90],# 3,4
#                 [-0.980, -0.480,-45], [-0.8406, -0.830,0],[-0.960, -1.654,0]# 5,6,7
#                  ,[-0.810,-3.650,0.000]# 失效邮件
#                 ]
# 7.22 规则更新后
# goods = [[0.160, -1.638, 180], [0.120 ,-0.830, 180],[0.182, -0.100,180],# 0,1,2
#                 [-0.032, -0.165,-90], [-0.790,-0.195,-90],# 3,4
#                 [-0.900, -0.080,0], [-0.9406, -0.830,0],[-0.960, -1.654,0]# 5,6,7
#                  ,[-0.810,-3.650,0.000]# 失效邮件
#                 ]

# 8.7 晚
goods = [[0.160, -1.69, 180], [0.120 ,-0.840, 180],[0.182, -0.130,180],# 0,1,2
                [-0.032, -0.165,-90], [-0.790,-0.195,-90],# 3,4
                [-0.900, -0.110,0], [-0.9406, -0.90,0],[-0.960, -1.8,0]# 5,6,7
                 ,[-0.810,-3.75,0.000]# 失效邮件
                ]
#8.7夜 无效邮件抓到
# 利用其他点，计算出失效邮件的位置
# goods[8][1] = goods[7][1] - (goods[5][1] - goods[7][1])/3*4


# 针对比较特殊的两个投放点0号和7号投放点，其中分别代表左中右三个位置
# good_0 = [[0.150, -2.222,-135],[0.120, -1.638, 180],[0.255, -1.137, 135]]
# good_7 = [[-1.071, -2.047, -20],[-0.960, -1.654,0],[-0.945, -1.093, 45]]

# shelves = [[0.4605, -3.620,88],[0.308,-3.643, 88],[0.150,-3.620,88],[0.075, -3.620, 89],[-0.155,-3.630,89.5],
#            [-0.525, -3.58,88],[-0.6825, -3.58, 88],[-0.84,-3.58,87.7],[-1.0125, -3.58, 87],[-1.1450,-3.58,83]]
# shelves = [[0.425, -3.617,88],[0.308,-3.640, 88],[0.148,-3.617,88],[0.068, -3.623, 89],[-0.155,-3.627,89.5],
#            [-0.530, -3.57,88],[-0.6825, -3.57, 88],[-0.84,-3.57,87.7],[-1.0125, -3.57, 87],[-1.1450,-3.57,83]]
# 7.20调整角度
# shelves = [[0.42506, -3.617,86],[0.278,-3.640, 88],[0.133,-3.617,88],[0.058, -3.623, 89],[-0.155,-3.627,89.5],
#            [-0.530, -3.57,88],[-0.6825, -3.57, 88],[-0.84,-3.57,87.7],[-1.0125, -3.57, 87],[-1.1450,-3.57,83]]
# 7.22
# shelves = [[0.436315, -3.617,86],[0.273,-3.640, 88],[0.1255,-3.617,88],[0.033, -3.623, 89],[-0.17505,-3.627,89.5],
#            [-0.530, -3.57,88],[-0.6825, -3.57, 88],[-0.84,-3.57,87.7],[-1.0125, -3.57, 87],[-1.1450,-3.57,83]]
# 7.22-2
# shelves = [[0.457, -3.617,86],[0.280,-3.640, 88],[0.1455,-3.617,88],[0.024, -3.623, 89],[-0.19505,-3.627,89.5],
#            [-0.530, -3.57,88],[-0.6825, -3.57, 88],[-0.84,-3.57,87.7],[-1.0125, -3.57, 87],[-1.1450,-3.57,83]]
# # 7-23
# shelves = [[0.465, -3.617,90],[0.280,-3.640, 90],[0.1455,-3.617,90],[0.020, -3.623, 90],[-0.19505,-3.627,90],
#            [-0.530, -3.57,88],[-0.6825, -3.57, 88],[-0.84,-3.57,87.7],[-1.0125, -3.57, 87],[-1.1450,-3.57,83]]

# 23-2
# 7.22-2
# shelves = [[0.457, -3.617,86],[0.280,-3.640, 88],[0.13,-3.617,88],[0.024, -3.623, 89],[-0.19505,-3.627,89.5],
#            [-0.530, -3.57,88],[-0.6825, -3.57, 88],[-0.84,-3.57,87.7],[-1.0125, -3.57, 87],[-1.1450,-3.57,83]]
#24-3
# shelves = [[0.457, -3.617,86],[0.280,-3.640, 88],[0.115,-3.617,88],[0.024, -3.623, 89],[-0.19505,-3.627,89.5],
#            [-0.530, -3.57,88],[-0.6825, -3.57, 88],[-0.84,-3.57,87.7],[-1.0125, -3.57, 87],[-1.1450,-3.57,83]]
# box_detect_list=["浙江","四川","广东","福建","江苏","湖南","安徽","河南","失效"]
box_detect_list=["广东","河南","四川","江苏","浙江","湖南","安徽","福建","失效"]
# 7.30 将倒数第二列放在了最前面，用于快速投放一次
# 8(随便投的货架，倒数第二列),    0
# 0,1,2,3,4,                  1，2，3，4，5
# 5,6,7,9                     6，7，8，9
# shelves = [[-1.0125, -3.57, 87],
#            [ 0.457, -3.617, 86], [0.280,-3.640, 88],[0.115,-3.617,88],[0.024, -3.623, 89],[-0.19505,-3.627,89.5],
#            [-0.530, -3.570, 88],  [-0.6825, -3.57, 88],[-0.84,-3.57,87.7],[-1.1450,-3.57,83]]

# 8.7 晚
# 7(随便投的货架，倒数第三列),    0
# 0,1,2,3,4,                  1，2，3，4，5
# 5,6,8,9                     6，7，8，9
# shelves = [[-0.84,-3.57,87.7],
#            [ 0.457, -3.617, 86], [0.305,-3.640, 88],[0.135,-3.617,88],[0.008, -3.623, 89],[-0.16,-3.627,89.5],
#            [-0.50, -3.570, 88],  [-0.6825, -3.57, 88],[-1.0125, -3.57, 87],[-1.1450,-3.57,83]]

shelves = [
           [ 0.463, -3.617, 86], [0.28,-3.640, 88],[0.135,-3.617,88],[0.008, -3.623, 89],[-0.16,-3.627,89.5],
           [-0.50, -3.570, 88],  [-0.6825, -3.57, 88],[-1.0125, -3.57, 87],[-1.1450,-3.57,83]]

she = [[0.135,-3.617,88],[0.008, -3.623, 89],[-0.16,-3.627,89.5],[-0.50, -3.570, 88]]

# # 机械臂初始化
# device.go_home()

# 载物台初始化
left_index = -1
right_index = -1

# moving()
# tmp = 0
# navigation.send_position([shelves[tmp][0], shelves[tmp][1]],shelves[tmp][2])
# navigation.send_position([goods[tmp][0], goods[tmp][1]],goods[tmp][2])
# target_x, target_y, target_ang = 0.140, goods[1][1], -90
# send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
# send_position_thread.start()
# rospy.sleep(7)
# tmp = 0
# navigation.send_position([goods[tmp][0], goods[tmp][1]],goods[tmp][2])
# navigation.send_position([0, 0],0)
# tmp = 1
# navigation.send_position([goods[tmp][0], goods[tmp][1]],-90)


# 测试定点
# navigation.send_position([good_7[0][0],good_7[0][1]],good_7[0][2])
# navigation.send_position([-0.889,-2.060],0)
# control.forward(1.2)
# navigation.send_position([good_0[0][0],good_0[0][1]],good_0[0][2])
# control.forward(0.8)
# navigation.send_position([-0.889,-2.060],0)
# navigation.send_position([0.430, -3.727],92)

# for x,y, ang in goods:
#     # print("现在是第"+str(tmp))
#     # navigation.send_position([shelves[tmp][0], shelves[tmp][1]],shelves[tmp][2])
    
#     # shelf2good(drift)
#     navigation.send_position([x, y],ang)
#     rospy.sleep(5)
#     # good2shelf(drift)
# navigation.send_position([goods[0][0],goods[0][1]],-116)
# control.forward(1.5)


# navigation.send_position([0, 0],0)


# P1从投放点到货架侧（当前货架点， 前一次投放中的左/右载物台， 偏移量）
def Part01(dot_position = -1, left_index = -1, right_index = -1):
    moving_thread = Thread(target=moving,args=[])
    moving_thread.start()
    # 特殊情况A：开局一列失效邮件（直接抵达下一货架点）
    if dot_position == 1:
        pass
    # 特殊情况B:零号+七号
    elif left_index == 0 and right_index == 7 or left_index == 7 and right_index == 0:
        # 此时投放完的末状态是在0号投放箱的位置
        print("调整方向后进行传送")
        # navigation.send_position([goods[0][0],goods[0][1]],goods[0][2])# 0号点
        navigation.send_position([goods[0][0],goods[0][1]],-116)
        # control.forward(1.5)
        forward_thread = Thread(target = control.forward,args = [1.5])
        forward_thread.start()
        rospy.sleep(1.5)
        # navigation.send_position([shelves[dot_position][0],shelves[dot_position][1]],shelves[dot_position][2])   
    # 特殊情况C：第二个投放了零号
    elif left_index == 0 or right_index == 0:
        target_x, target_y, target_ang = (goods[3][0]+goods[4][0])/2, (goods[8][1] + goods[7][1])/2, -90
        send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
        send_position_thread.start()
        rospy.sleep(9)

        target_x, target_y, target_ang = she[2][0], goods[8][1], 0
        send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
        send_position_thread.start()
        rospy.sleep(3.5)
    # 1.失效点到货架
    elif left_index == 8 and right_index == 8 :
        print("失效点到货架")
        # 直接抵达货架即可
    # 2.出发点和下面六个点到货架
    elif left_index <= 5 or right_index <= 5:
        print("斜角侧传送到货架侧")
        good2shelf()
    # 3.其他情况
    else:
        print("六号或七号邮件到当前货架点")
        L2mid()
    print("抵达当前货架点"+str(dot_position))
    pos_x, pos_y, pos_ang = shelves[dot_position]
    navigation.send_position([pos_x,pos_y],pos_ang)


# P3直角/斜角判断部分
def Part03A(index):
    if index >= 5:
        # 走直角侧
        return "直"
    else:
        # 走斜角侧
        return "斜"

# P3 判断绕过圆盘方式部分
def Part03A_2(small_index, big_index):
    small_index = int(small_index)
    big_index = int(big_index)
    if small_index > big_index:
        small_index, big_index = big_index, small_index
    res = ""
    if big_index == 7 and small_index in [1,2,3]:
        res = res + "绕二"
    if big_index in [4,5,6] and small_index == 0:
        res = res + "绕一"
    if small_index == 0:
        res = res + "绕三"
    print("绕的顺序" + res)
    return res

# P3路径转换为字符串部分
def Part03B(left_index, right_index):
    res = ""
    # 投放两个失效邮件
    if left_index == 8 and right_index == 8:
        # "停"表示两个邮件相同，不需要移动
        res = "L8停R8"
    # 投放其中一个失效邮件并后续投放有效邮件
    elif left_index == 8 or right_index == 8:
        if left_index == 8:
            print("先投左边的失效邮件")
            res = res +"L8"

            tmp = Part03A(right_index)
            print("走"+ tmp +"侧")
            res = res + tmp

            print("再投右边的"+ str(right_index)+"号邮件")
            res = res +"R"+str(right_index)
        elif right_index == 8:
            print("先投右边的失效邮件")
            res = res +"R8"

            tmp = Part03A(left_index)
            print("走"+ tmp +"侧")
            res = res + tmp

            print("再投左边的"+ str(left_index)+"号邮件")
            res = res +"L"+str(left_index)
    # 两个正常邮件
    else:
        # 特殊情况：0和7
        if left_index == 7 and right_index == 0 or left_index == 0 and right_index == 7:
            res = res + "穿"
            if left_index > right_index:
                res = res + "LR"
            else:
                res = res + "RL"
        # 两个邮件序号相同
        elif right_index == left_index:
            tmp = Part03A(right_index)
            print("走"+ tmp +"侧")
            res = res + tmp

            print("投"+ str(left_index)+"号邮件两次")
            res = res + "L" + str(left_index) + "停" + "R" + str(right_index)

        # 左<右
        elif left_index < right_index:
            tmp = Part03A(right_index)
            print("走"+ tmp +"侧")
            res = res + tmp
            # if right_index - left_index >= 4:
            #     res = res +"R"+str(right_index)
            #     res = res + Part03A_2(left_index,right_index)
            #     res = res +"L"+str(left_index)
            # else:
            #     res = res + "R"+str(right_index)+"L"+str(left_index)
            res = res + "R"+str(right_index)+ Part03A_2(left_index,right_index) +"L"+str(left_index)
        # 左>右
        else:
            tmp = Part03A(left_index)
            print("走"+ tmp +"侧")
            res = res + tmp
            # if left_index - right_index >= 4:
            #     res = res +"L"+str(left_index)
            #     res = res + Part03A_2(right_index,left_index)
            #     res = res +"R"+str(right_index)
            # else:
            #     res = res + "L"+str(left_index)+"R"+str(right_index)
            res = res + "L"+str(left_index)+ Part03A_2(left_index,right_index) +"R"+str(right_index)


    print(res)
    return res

# 删除字符串第一个字符并返回(第一个字符，剩余的字符)
def cut(s):
    f_c = s[0]
    re_cs = s[1:]
    return f_c, re_cs

# P3字符串转动作部分/投放部分
def Part03C(acts):
    while len(acts) > 0:
        act, acts = cut(acts)
        if act == "L" or act == "R":
            # 获取对应投放点act2
            tmp, acts = cut(acts)
            act2 = int(tmp)
            target_x, target_y, target_ang = goods[act2][0], goods[act2][1], goods[act2][2]
            # 先开启投放点
            send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
            send_position_thread.start()

            send_position_thread.join()
            # 前往投放的时候，从对应一侧获取
            if act == "L":
                get_from_left_thread = Thread(target=get_from_left,args=[])
                get_from_left_thread.start()
            elif act == "R":
                get_from_right_thread = Thread(target=get_from_right,args=[])
                get_from_right_thread.start()
            # 等待到达点位后，调整位置 
            # 8.7 夜 ----------------------
            
            # send_position_thread.join()
            adopt_forward_thread = Thread(target=adopt_forward,args=[-0.5,0.17])
            adopt_forward_thread.start()
            
            adopt_forward_thread.join()

            # 防止机械臂没来得及吸起邮件
            if act == "L":
                get_from_left_thread.join()
            elif act == "R":
                get_from_right_thread.join()

            put_into_thread = Thread(target=put_into,args=[])
            put_into_thread.start()
            # 调整好位置后，打开投放
            put_into_thread.join()
        elif act == "斜":
            # 机械臂调整到默认位置
            moving_thread = Thread(target=moving,args=[])
            moving_thread.start()
            # 斜角传送
            shelf2good()
        elif act == "直":
            # 机械臂调整到默认位置
            moving_thread = Thread(target=moving,args=[])
            moving_thread.start()
            # 直角传送
            shelf2mid()
        elif act == "停":
            pass
        elif act == "绕":
            """
            第一种情况为  0 + 456 记为 “绕一”
            第二种清况为  123 + 7 记为 “绕二”
            第三种情况为  需要投放0 记为 “绕三”
            """
            act2, acts = cut(acts)
            if act2 == "一":
                #TODO: 在投放 0 的时候，仍会卡在0的投放点附近
                target_x, target_y, target_ang = 0.140, goods[1][1], -90
                send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
                send_position_thread.start()
                rospy.sleep(5)
            elif act == "二":
                target_x, target_y, target_ang = (goods[3][0] + goods[4][0]*2)/3, (goods[5][1]+goods[6][1])/2, 0
                send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
                send_position_thread.start()
                rospy.sleep(10)
            elif act2 == "三":
                # target_x, target_y, target_ang = (goods[0][0]+goods[1][0])/2, goods[0][1], -180
                # send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
                send_position_thread = Thread(target = navigation.send_position,args = [[good2shelf_main_Point[0], good2shelf_main_Point[1]], good2shelf_main_Point[2]])
                send_position_thread.start()
                rospy.sleep(5)
            else:
                print("Part3 绕 的部分有问题")
            # target_x, target_y, target_ang = (goods[3][0]+goods[4][0])/2, -0.400, 0
            # send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
            # send_position_thread.start()
        elif act == "穿":
            # 获取对应投放左右act2， act3
            tmp, acts = cut(acts)
            act2 = str(tmp)
            tmp, acts = cut(acts)
            act3 = str(tmp)

            # 调整到默认位置
            moving_thread = Thread(target=moving,args=[])
            moving_thread.start()
            # 先传送到中间
            shelf2mid()
            
            print("先去投放七号")
            target_x, target_y, target_ang = goods[7][0] ,goods[7][1], goods[7][2]
            # 开启投放点
            send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
            send_position_thread.start()

            # 8.7 夜--------到达投放点后，开始调整位置
            send_position_thread.join()
            # 从对应侧获取
            if act2 == "L":
                get_from_left_thread = Thread(target=get_from_left,args=[])
                get_from_left_thread.start()
            else:
                get_from_right_thread = Thread(target=get_from_right,args=[])
                get_from_right_thread.start()
            
            # # 到达投放点后，开始调整位置
            # send_position_thread.join()


            adopt_forward_thread = Thread(target=adopt_forward,args=[-0.5,0.17])
            adopt_forward_thread.start()

            # 调整完距离后 + 已经吸取到邮件后
            adopt_forward_thread.join()
            if act2 == "L":
                get_from_left_thread.join()
            else:
                get_from_right_thread.join()

            put_into_thread = Thread(target=put_into,args=[])
            put_into_thread.start()
            # 等待投放结束后，再进行其他动作
            put_into_thread.join()

            
            print("准备传送到零号投放点")

            target_x, target_y, target_ang = -0.889 ,-2.060 ,0
            send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
            send_position_thread.start()
            # if act3 == "L":
            #     get_from_left_thread = Thread(target=get_from_left,args=[])
            #     get_from_left_thread.start()
            # else:
            #     get_from_right_thread = Thread(target=get_from_right,args=[])
            #     get_from_right_thread.start()

            send_position_thread.join()
            control.forward(1.2)
            target_x, target_y, target_ang = goods[0][0], goods[0][1], goods[0][2]
            send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
            send_position_thread.start()
            
            send_position_thread.join()
            # 8.7 夜--------
            if act3 == "L":
                get_from_left_thread = Thread(target=get_from_left,args=[])
                get_from_left_thread.start()
            else:
                get_from_right_thread = Thread(target=get_from_right,args=[])
                get_from_right_thread.start()
            adopt_forward_thread = Thread(target=adopt_forward,args=[-0.5,0.17])
            adopt_forward_thread.start()

            adopt_forward_thread.join()

            # 8.7 夜-------- 
            if act3 == "L":
                get_from_left_thread.join()
            else:
                get_from_right_thread.join()
            put_into_thread = Thread(target=put_into,args=[])
            put_into_thread.start()
            # 等待投放结束后，再进行其他动作
            put_into_thread.join()
        else:
            print("Part3有问题")

# TODO:1.如果没识别到，及时止损的情况-》回传9
# device.go_home()
time_start = time.time()
global the_dis
for dot_position ,[pos_x,pos_y,pos_ang] in enumerate(shelves):
    
    ##### PartA:从投放点到货架侧————————————————————
    Part01(dot_position, left_index, right_index)
    """
    需要判断小车当前在哪里，走哪一边合适
    特殊情况A:零号+七号
    1.失效点到货架
    2.出发点和下面六个点到货架
    3.其他情况
    """
    #### PartB:从到达货架侧之后，获取邮件————————————————————
    # 到达目标点附近的话，那就自己检测自己的处境
    messages=[]
    start1_time=time.time()
    while(1):
        start2_time=time.time()
        if(start2_time-start1_time>2):
            break
        print(single_laser.get_dis())
        a=float(single_laser.get_dis())
    # print("jiesu")
    # # print(type(a))
    # print(a)
    if a>0.40:
        print("处于位置1")
        # looking_up()
        if a>0.42 and a <0.4:
            control_forward = Thread(target=control.forward,args=[0.2] )
            control_forward.start()
        else:
            #先抓取上面的商品
            looking_up_thread = Thread(target=looking_up,args=[])
            looking_up_thread.start()
            adopt_forward_thread = Thread(target=adopt_forward,args=[-0.3,0.47])
            adopt_forward_thread.start()
            a,b,c=find_QRCode()
            messages.append(a)
            print(a)
            adapt_grip_new(b,c)


            control_forward = Thread(target=control.forward,args=[0.35] )
            control_forward.start()
            time.sleep(1)
            put_to_left()
            #重新定位
            time.sleep(1)
            looking__down_thread=Thread(target=looking_down,args=[])
            looking__down_thread.start()
            time.sleep(1)
            adopt_forward_thread = Thread(target=adopt_forward,args=[-0.3,0.48])
            adopt_forward_thread.start()
            a,b,c=find_QRCode()
            messages.append(a)
            adapt_grip_new(b,c,1)
            control_forward = Thread(target=control.forward,args=[0.30] )
            control_forward.start()
            time.sleep(1)
            put_to_right_thread=Thread(target=put_to_right,args=[])
            put_to_right_thread.start()

    else:
        print("处于位置零")
        adopt_forward_thread = Thread(target=adopt_forward,args=[-0.3,0.3])
        adopt_forward_thread.start()
        # 同样是先抓上面的盒子
        looking_up_thread = Thread(target=looking_up,args=[])
        looking_up_thread.start()
        a,b,c=find_QRCode()
        messages.append(a)
        print(a)
        adapt_grip_new(b,c)
            # put_to_left_thread=Thread(target=put_to_left,args=[])
            # put_to_left_thread.start()
        control_forward = Thread(target=control.forward,args=[0.35] )
        control_forward.start()
        time.sleep(1)
        put_to_left()
        
        adopt_forward_thread = Thread(target=adopt_forward,args=[-0.3,0.30])
        adopt_forward_thread.start()
        looking_down_new()
        a,b,c=find_QRCode()
        messages.append(a)
        raking_grep_new(b,c,1)
        control_forward = Thread(target=control.forward,args=[0.30] )
        control_forward.start()
        time.sleep(1)
        put_to_right_thread=Thread(target=put_to_right,args=[])
        put_to_right_thread.start()

    #投放程序
    left_index=box_detect_list.index(messages[0])
    right_index=box_detect_list.index(messages[1])
    print("准备投送"+str(left_index)+"和"+str(right_index))

    put_to_right_thread.join()
    ## PartB-2:测试使用的随机数
    # tmp = input("卡住程序")
    # import random
    # import time
    # random.seed(time.time())
    # left_index = random.randint(0,8)
    # right_index = random.randint(0,8)

    # ##### PartC:从货架侧到投放点侧————————————————————
    # 先处理成字符串
    # if dot_position == 0:
    #     acts = "L8R8"
    #     # control.forward(0.5)
    #     target_x, target_y, target_ang = goods[8][0], goods[8][1]-1, goods[8][2]
    #     send_position_thread = Thread(target = navigation.send_position,args = [[target_x,target_y], target_ang])
    #     send_position_thread.start()
    #     rospy.sleep(3)
    # else:
    #     acts = Part03B(left_index, right_index)
    acts = Part03B(left_index, right_index)
    # 动作执行
    Part03C(acts)
    """
    #投放两个失效邮件

    #投放一个失效邮件
    ##左边为失效
    ###走L侧
    ###走斜角侧

    #投放两个正常邮件
    ##特殊情况1:0和7
    ##L=R
    ##L<R
    ###走L侧
    ###走斜角侧
    ##L>R
    ###走L侧
    ###走斜角侧
    ##如果差值大于5,则添加投放途径点
    """
    print("投放完成")
print("任务完成")
navigation.send_position([0,0],0)
time_finish = time.time() - time_start
print("花费时间："+ str(time_finish // 60) +"分" + str(time_finish % 60) +"秒")