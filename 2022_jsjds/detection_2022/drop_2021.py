from serial.tools import list_ports

from pydobot.dobot import Dobot
import time
from threading import Thread

drop_position = [None, -32, 4, 38, 55, 55, 55]
drop_j2 = 40
drop_j3 = 0
drop_y = 181
drop_x = 70

def pre_drop_process(device, position):
    x, i = position
    if x == "l":
        x = -drop_x
    else:
        x = drop_x
    # print(drop_position[i+1])
    # print(drop_position[i])
    device.suck(True)
    device.move_to(x, drop_y, drop_position[i+1], 0, False)
    
    device.move_to(x, drop_y, drop_position[i], 0, True)
    
    # time.sleep()
    device.move_to(x, drop_y, 55, 0, True)
    device.rotate_to(90.0, 0.0, 0.0, -90.0, False)
    device.rotate_to(-90.0, 50.0, 0.0, 0.0, True)

def drop_process(device):
    time.sleep(0.2)
    device.suck(False)
    device.rotate_to(90.0, 0.0, 0.0, -90.0, True)


def pre_grab_process(device, position):
    towards, i = position
    if towards == "l":
        x = -drop_x
        # reverse_towards = "r"
    else:
        x = drop_x
        # reverse_towards = "l"
    ##################################################################
    # 转移过程
    ##################################################################
    device.move_to(x, drop_y , drop_position[i + 2], 0, True)
    device.move_to(x, drop_y , drop_position[i+1], 0, True)

    device.suck(True)
    # time.sleep(0.5)
    device.move_to(x, drop_y , 60, 0, True)
    # device.move_to(0, drop_y+30 , 60, 0, True)
    device.move_to(-x, drop_y , 60, 0, True)
    time.sleep(0.2)
    device.suck(False)
    # device.move_to(0, drop_y+30 , 60, 0, True)
    device.move_to(x, drop_y , 60, 0, True)
    ##################################################################
    # 抓取过程
    ##################################################################
    pre_drop_process(device, position)
    ##################################################################
    # 返回过程
    ##################################################################

def restore(device,store):
    x =0

    if not store['r3'][0]:
        x = -drop_x
        towards = 'r'
        reverse_towards = "l"
    
    elif not store['l3'][0]:
        x = drop_x
        towards = 'l'
        reverse_towards = "r"
    if x !=0:
        device.move_to(-x, drop_y , drop_position[4], 0, True)
        device.suck(True)
        device.move_to(-x, drop_y , drop_position[3], 0, True)
        device.move_to(-x, drop_y , 60, 0, True)
        # device.move_to(0, drop_y+30 , 60, 0, True)
        device.move_to(x, drop_y , 60, 0, True)
        time.sleep(0.2)
        device.suck(False)
        # 清理存储顺序
        level = '1' if store[reverse_towards + "1"][0] else '2'
        store[reverse_towards + level][1] = store[towards + '3'][1]
        store[reverse_towards + level][0] = False
        store[towards + '3'][0] = True
        print("restore job done")



def drop_by_name(device, store, name):
    '''
    store:当前车载box列表
    name:投递目标
    按照从上到下,从左到右的顺序将一个投递盒移动到预投递位置
    need_restore表示是否需要调用restore函数
    '''
    finished = True
    need_restore = False
    position = None
    device.rotate_to(90.0, 0.0, 0.0, -90.0, True)
    for i in range(2, 0, -1):
        if not store["r" + str(i)][0] and (
            store["r" + str(i)][1] == name or name == "test" or store["r" + str(i)][1] == "unknown"
        ):
            print("[DROP] drop job with r" + str(i)+" "+store["r" + str(i)][1])
            if i < 2 and not store["r" + str(i + 1)][0]:  # 如果上面有盒子
                pre_grab_process(device, ["r", i])  # 搬開i上面的盒子

                level = '1' if store["l1"][0] else ('2' if store["l2"][0] else '3')
                store["l"+level][1] = store["r2"][1]
                store["l"+level][0] = False 
                store["r2"][0] = True
            else:
                pre_drop_process(device, ["r", i])
            
            store["r" + str(i)][0] = True
            finished = False
            
            break
            # print("#############################################")

        if not store["l" + str(i)][0] and (
            store["l" + str(i)][1] == name or name == "test" or store["l" + str(i)][1] == "unknown"
        ):
            print("[DROP] drop job with l" + str(i)+" "+store["l" + str(i)][1])
            if i < 2 and not store["l" + str(i + 1)][0]:  # 如果上面有盒子
                pre_grab_process(device, ["l", i])  # 搬開上面的盒子
                level = '1' if store["r1"][0] else ('2' if store["r2"][0] else '3')
                store["r"+level][1] = store["l2"][1]
                store["r"+level][0] = False 
                store["l2"][0] = True
            else:
                pre_drop_process(device, ["l", i])
            store["l" + str(i)][0] = True
            finished = False
            break
            # print("#############################################")
    # device.rotate_to(90.0, 0.0, 0.0, -90.0, True)
    return store,finished


def drop_test():
    for i in range(0, 4):
        try:
            port = list_ports.comports()[i].device
            device = Dobot(port=port, verbose=False)
        except:
            pass
        else:
            break
    device._set_queued_cmd_clear()
    device.clear_all_alarms_state()
    device.rotate_to(90, 0, 0, 0, wait=True)
    store = {
        "l1": [False, "广东"],
        "l2": [False, "江东"],
        "l3": [True, ""],
        "l4": [True, ""],
        "r1": [False, "广东"],
        "r2": [True, "江西"],
        "r3": [True, ""],
        "r4": [True, ""],
    }
    # pre_grab_process(device,store,["l",2])
    restore_thread = Thread()
    while True:
        if restore_thread.isAlive():
            restore_thread.join()
        store,finished = drop_by_name(device=device,store=store,name="广东")
        print(store)
        if finished:
            break
        # navigation_thread.join()
        drop_process(device)
        # restore(device,store)
        restore_thread = Thread(target=restore,args=[device,store])
        restore_thread.daemon = True
        restore_thread.start()
    name = "广东"
    for j in range(1,3):
        if not store["l"+str(j)][0]:
            store["l"+str(j)][1] = name if name else "unknown"
        if not store["r"+str(j)][0]:
            store["r"+str(j)][1] = name if name else "unknown"
    
    while True:
        if restore_thread.isAlive():
            restore_thread.join()
        store,finished = drop_by_name(device=device,store=store,name="广东")
        print(store)
        if finished:
            break
        # navigation_thread.join()
        drop_process(device)
        # restore(device,store)
        restore_thread = Thread(target=restore,args=[device,store])
        restore_thread.daemon = True
        restore_thread.start()


if __name__ == "__main__":
    drop_test()
