#!/home/eaibot/anaconda3/envs/pro/bin/python
import cv2 as cv
import numpy as np
import glob
# import device
# device_list = device.getDeviceList()
# index = 0
# for name in device_list:
#     print(str(index) + ': ' + name)
#     index += 1
# # termination criteria
# criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)

# # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
# objp = np.zeros((4*11, 3), np.float32)
# objp[:, :2] = np.mgrid[0:4, 0:11].T.reshape(-1, 2)

# # Arrays to store object points and image points from all the images.
# objpoints = []  # 3d point in real world space
# imgpoints = []  # 2d points in image plane.
# cap = cv.VideoCapture('/dev/front_camera')
# params = cv.SimpleBlobDetector_Params()
# params.maxArea = 10e4
# params.minArea = 10
# params.minDistBetweenBlobs = 5
# blobDetector = cv.SimpleBlobDetector_create(params)
# i = 0
# while(1):
#     _, frame = cap.read()
#     cv.imshow("test", frame)
#     key = cv.waitKey(30)
#     if key == ord('c'):
#         gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

#         # Find the chess board corners
#         ret, corners = cv.findCirclesGrid(gray, (4, 11), cv.CALIB_CB_ASYMMETRIC_GRID,blobDetector, None)
#         print("test1")
#         # If found, add object points, image points (after refining them)
#         if ret:
#             objpoints.append(objp)

#             corners2 = cv.cornerSubPix(
#                 gray, corners, (4, 11), (-1, -1), criteria)
#             imgpoints.append(corners2)
#             print("test2")
#             # Draw and display the corners
#             img = cv.drawChessboardCorners(frame, (4, 11), corners2, ret)
#             cv.imshow('imgr.jpg', img)
#             cv.waitKey(50)
#     elif key == ord('q'):
#         break

# cv.destroyAllWindows()
# ret, mtx, dist, rvecs, tvecs = cv.calibrateCamera(
#     objpoints, imgpoints, gray.shape[::-1], None, None)
# mtx =np.eye(3,3)
# mtx[0][0]=371.450355
# mtx[0][1]=0.000000
# mtx[0][2]=320.451676
# mtx[1][0]=0.000000
# mtx[1][1]=494.574368
# mtx[1][2]=234.028774
# mtx[2][0]=0.000000
# mtx[2][1]=0.000000
# mtx[2][2]=1.000000
# mtx = np.mat(mtx)
# dist = np.zeros([5,1])
# dist[0][0]=-0.347383
# dist[1][0]=0.081489
# dist[2][0]=0.004733
# dist[3][0]=-0.001698
# dist[4][0]=0
# dist = np.mat(dist)
# mtx = np.array[[4.21973959e+03, 0.00000000e+00, 2.98431689e+02]
#        [0.00000000e+00, 1.15719468e+04, 2.26890226e+02]
#        [0.00000000e+00, 0.00000000e+00, 1.00000000e+00]]
# mtx[0][0]=421.973959
# mtx[0][1]=0.000000
# mtx[0][2]=29.8431689
# mtx[1][0]=0.000000
# mtx[1][1]=1157.19468
# mtx[1][2]=22.6028774
# mtx[2][0]=0.000000
# mtx[2][1]=0.000000
# mtx[2][2]=1.000000
# mtx = np.mat(mtx)
# dist = np.array[[-4.47983756e+01,  2.18216004e+03,
#          4.95048635e-01,  9.31525962e-02, 1.32547204e+01]]
# dist[0][0]=-4.479837
# dist[1][0]=218.21600
# dist[2][0]=0.495048635
# dist[3][0]=0.0931525
# dist[4][0]=1.32547294
# dist = np.mat(dist)
for j in range(0,2):
    try:
        print(j)
        cap = cv.VideoCapture(j)
        _,frame = cap.read()
        cv.imwrite('./test'+str(j)+'.jpg', frame)
    except :
        print("camera not find")
for j in range(30):
    _,frame = cap.read()

# cv.imwrite('./test.jpg', frame)
#    key = cv.waitKey(30)
#    if key == ord('c'):
#        cv.imwrite("./test.jpg", dst)
#    elif key == ord('q'):
#        break
#print(mtx, dist)
