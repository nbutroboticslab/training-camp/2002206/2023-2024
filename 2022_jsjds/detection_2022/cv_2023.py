import importlib
import sys
sys.path.remove("/opt/ros/kinetic/lib/python2.7/dist-packages")
import re
importlib.reload(sys)
from pyzbar import pyzbar
from PIL import Image
import cv2
import math


import time
global barcodeData

def find_QRCode():
    # 初始化摄像头
    cap = cv2.VideoCapture("/dev/video0")


    # 设置视频编码格式
    fourcc = cv2.VideoWriter_fourcc(*'XVID')

    # 设置视频保存路径
    # out = cv2.VideoWriter('./output.avi', fourcc, 20.0, (640, 480))
    # 帧大小为640x480 帧率为20
    start_recongnition=time.time()
    while True:
        # 读取图像
        ret, frame = cap.read()
        # 获取图像中心点
        frame_height, frame_width, _ = frame.shape
        camera_center = [frame_width / 2, frame_height / 2]
        
        # 寻找二维码
        barcodes = pyzbar.decode(frame)
        barcodeData = None
        qr_center = None
        # flag=True
        # # 二维码中心坐标与摄像头中心坐标的差值，大于零则在右与上，小于零则在左与下
        # x_diff = None
        # y_diff = None


        # if time.time()-start_recongnition>8:
        #     flag=False
        #     # print("识别失效！！！")
        # if flag==False:
        #     break
        # 处理每一个二维码
        for barcode in barcodes:
            # 提取二维码边框的位置
            if len(barcodes) > 0:
                barcode = barcodes[0]
                (x, y, w, h) = barcode.rect
                top_left = (x, y)
                top_right = (x + w, y)
                bottom_left = (x, y + h)
                bottom_right = (x + w, y + h)
                qr_center = [(top_left[0] + bottom_right[0]) / 2, (top_left[1] + bottom_right[1]) / 2]

            # if qr_center is not None and camera_center is not None:
            #     x_diff = qr_center[0] - camera_center[0]
            #     y_diff = qr_center[1] - camera_center[1]

            # 在图像上绘制矩形
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

            # 提取二维码中的数据
            barcodeData = barcode.data.decode("utf-8")
            barcodeType = barcode.type

            # 在图像上绘制二维码数据
            text = "{} ({})".format(barcodeData, barcodeType)
            cv2.putText(frame, text, (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

        # 将帧写入视频文件
        # out.write(frame)

        # 显示图像
        # cv2.imshow("Barcode Scanner", frame)

        if barcodeData is not None:
            print("bushiongde")
            cv2.imwrite("/home/eaibot/2022_jsjds/detection_2022/1.png",frame)
            break

    # if qr_center is not None:
    #     cv2.line(frame, (int(qr_center[0]), int(qr_center[1])),
    #              (int(camera_center[0]), int(camera_center[1])), (0, 0, 255),2)

    # # 释放资源
    # cap.release()
    # out.release()

    # if flag==True:
    #     if len(barcodeData) > 15:

    #         search_point = r"address"
    #         address_return = re.search(search_point, barcodeData)
    #         if address_return is not None:
    #             num2 = address_return.span()[1]
    #             province_data = barcodeData[num2+5:num2+7]
    #             print(province_data, qr_center, w)
    #         # print(type(qr_center))

    #             return province_data, qr_center, w
    #         else:
    #             print ("no match found")

    #     else:
    #         print("失效",len(barcodeData))
    #         print("失效", qr_center, w)
    #         return "失效", qr_center, w
    # #如果超时识别的时候
    # else:
    #     return False,False,False,False
    if len(barcodeData) > 15:

        search_point = r"address"
        address_return = re.search(search_point, barcodeData)
        if address_return is not None:
            num2 = address_return.span()[1]
            province_data = barcodeData[num2+5:num2+7]
            print(province_data, qr_center, w)
        # print(type(qr_center))

            return province_data, qr_center, w
        else:
            print ("no match found")

    else:
        print("失效",len(barcodeData))
        print("失效", qr_center, w)
        return "失效", qr_center, w
    


if __name__ == '__main__':
    find_QRCode()


#
#     # 寻找二维码
#     barcodes = pyzbar.decode(frame)
#     barcodeData = None
#
#     # 处理每一个二维码
#     for barcode in barcodes:
#         # 提取二维码边框的位置
#         (x, y, w, h) = barcode.rect
#
#         # 在图像上绘制矩形
#         cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
#
#         # 提取二维码中的数据
#         barcodeData = barcode.data.decode("utf-8")
#         barcodeType = barcode.type
#
#         # 在图像上绘制二维码数据
#         text = "{} ({})".format(barcodeData, barcodeType)
#         cv2.putText(frame, text, (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
#
#     # 显示图像
#     cv2.imshow("Barcode Scanner", frame)
#
#     # 按下 q 键退出
#
#     # if cv2.waitKey(1) & 0xFF == ord("q"):
#     #     break
#     if barcodeData is not None:
#             break
#
# search_point = r"'address'"
# address_return = re.search(search_point, barcodeData)
# num2 = address_return.span()[1]
# province_data = barcodeData[num2+3:num2+5]
# print(province_data)
#
# # 释放资源
# cap.release()
# cv2.destroyAllWindows()



