#!/bin/bash

cd ~/code/NNWORK_LEO/
# rosservice call /global_localization "{}"
########################################
# FOR TEST
echo "run start"
# 删除缓存文件
rm -rf ./depository/*
# 机械臂归零
./grab_init.py
#开启除amcl 以外节点
roslaunch dashgo_nav navigation_imu_2.launch &
echo "run_base"
# rosnode kill --all
#摆正方向
rosrun mrobot_navigation find_self.py
#开启amcl节点
roslaunch dashgo_nav amcl.launch &


for i in {1,3}
do
	rm -rf ./depository/store.pkl
	# 对应右侧抓取
	# #######################################
	rosrun mrobot_navigation to_grab_right.py
	rosrun dashgo_tools move.py -0.14
	./grab_pro.py "right"
	rosrun dashgo_tools move.py 0.23
	
	#对应左侧抓取
	# #######################################
	rosrun mrobot_navigation to_grab_left.py
	rosrun dashgo_tools move.py -0.14
	./grab_pro.py "left"
	rosrun dashgo_tools move.py 0.12

	# 投递部分
	# #######################################
	#前往投递区域，第二遍是为了保证导航准确性
	rosrun mrobot_navigation to_delivery_position.py
	rosrun mrobot_navigation to_delivery_position.py
	# 寻找投递箱
	rosrun mrobot_navigation find_deliver_position.py
	# 投递过程
	./delivery_process.py
	#./delivery_process_for_test.py
	# #######################################
	rosrun mrobot_navigation prepare_move.py
	#rosrun dashgo_tools move.py -2.0
	rosrun mrobot_navigation to_home.py
done
exit
