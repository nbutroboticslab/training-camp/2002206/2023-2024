#!/bin/bash
rostopic pub -1 /move_base/goal move_base_msgs/MoveBaseActionGoal "
header:
  seq: 1
  stamp:
    secs: 1608878329
    nsecs: 918657271
  frame_id: 'map'
goal_id:
  stamp:
    secs: 1608878329
    nsecs: 918657271
  id: '1S'
goal:
  target_pose:
      header:
          seq: 0
          stamp:
              secs: 1608877072
              nsecs: 564020392
          frame_id: 'map'
      pose:
          position:
              x: 1.06864976883
              y: 0.13583651185
              z: 0.0
          orientation:
              x: 0.0
              y: 0.0
              z: 0.497782833881
              w: 0.86730170661
"