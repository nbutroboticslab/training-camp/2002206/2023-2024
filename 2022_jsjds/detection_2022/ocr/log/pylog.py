# _*_ coding: utf-8 _*_
from fileinput import filename
import logging
import os.path
import time


class Logger(object):
    def __init__(self, loggername):
        '''''
            指定保存日志的文件路径，日志级别，以及调用文件
            将日志存入到指定的文件中
        '''

        # 创建一个logger
        self.logger = logging.getLogger(loggername)
        self.logger.setLevel(logging.INFO)

        # 创建一个handler，用于写入日志文件
        rq = time.strftime('%Y%m%d%H%M', time.localtime(time.time()))
        log_path = os.path.dirname(os.path.abspath('.')) + '/logs/'
        if not os.path.exists(log_path):
            os.makedirs(log_path)
        log_name = log_path + rq + '.log'

        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s - %(filename)s')#输出格式

        fh = logging.FileHandler(log_name)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)
        self.logger.addHandler(fh)

        # 再创建一个handler，用于输出到控制台
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)
    
    def getlog(self):
        return self.logger


log = Logger("main").logger


if __name__ == '__main__':
    log = Logger('test').logger
    log.info("INFO")
    log.warn("WARN")