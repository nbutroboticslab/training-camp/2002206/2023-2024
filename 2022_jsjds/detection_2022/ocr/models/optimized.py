import onnx
onnxfile="dbnet.onnx"
onnx_model = onnx.load(onnxfile)
passes = ["eliminate_unused_initializer"]
from onnx import optimizer
optimized_model = optimizer.optimize(onnx_model, passes)

onnx.save(optimized_model, onnxfile)