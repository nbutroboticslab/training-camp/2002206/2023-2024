import onnx
onnxfile="angle_net.onnx"
onnx_model = onnx.load(onnxfile)
passes = ["extract_constant_to_initializer", "eliminate_unused_initializer"]
from onnx import optimizer
optimized_model = optimizer.optimize(onnx_model, passes)

onnx.save(optimized_model, onnxfile)