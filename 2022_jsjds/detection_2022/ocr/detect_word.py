import random
import traceback
from image_angle_correct import *
from hsv_test import hsv_split, show,bigger_box,hsv_split_pre
from model import *
import sys
sys.path.append("../")
from util.keys_prov import *
from PIL import Image, ImageDraw, ImageFont
'''
    识别流程:
    1.hsv掩膜
    2.识别文字框，旋转纠正角度
    3.识别，若无，则旋转180度，再识别，若仍无结果，接连3次旋转60度并识别
'''

# TEST 
# source_image_path = "/home/meroke/code/detection/result/error/"  # test
# result_image_save_path = "/home/meroke/code/detection/result/test/result/" # test
# error_image_save_path ="/home/meroke/code/detection/result/test/error/" # test 

# MAIN
# 训练集路径
source_image_path = "./image/test/" # main
# 识别结果图片保存路径
result_image_save_path = "./image/result/" # main
# 未识别地区的图片保存路径
error_image_save_path ="./image/error/" # mian


# 地区识别结果保存路径
# word_detect_path = "log/word_detect.txt"
word_detect_path = "log/result.txt"
# 识别结果细节保存路径
detect_detail_path = "log/detect_detail.txt"

# 检测正确数量 计数器
detect_count =0
# 所有图片数量 计数器
image_count = 0

'''
description:  在图片上绘制中文信息
param {*} img  绘制图片
param {*} text  要绘制的中文信息
param {*} position 绘制的像素点位置
param {*} textColor 绘制的文字颜色
param {*} textSize 绘制的文字大小
return {*} 返回BGR格式的，绘制有中文信息的图片
'''
def cv2AddChineseText(img, text, position, textColor=(0, 255, 0), textSize=30):
    if (isinstance(img, np.ndarray)):  # 判断是否OpenCV图片类型
        img = Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    if text is "":
        text = "安徽"
    # 创建一个可以在给定图像上绘图的对象
    draw = ImageDraw.Draw(img)
    # 字体的格式
    fontStyle = ImageFont.truetype(
        "simsun.ttc", textSize, encoding="utf-8")
    # 绘制文本
    draw.text(position, text, textColor, font=fontStyle)
    # 转换回OpenCV格式
    return cv2.cvtColor(np.asarray(img), cv2.COLOR_RGB2BGR)


'''
description: 字符串匹配省份
param {*} detected_word 检测到的字符串 列表
return {*} 返回匹配的省份名称， 无结果则返回空字符串
'''
def searcch_provience(detected_word):
    result = ""
    flag = False
    for key in province:
        if flag:
            break
        for single in detected_word[1]:
            if single.find(province[key])>-1 or single.find(key)>-1:
                result=province[key]
                # print(true_result)
                flag=True
                break
    return result

'''
description: 记录识别到的省份 及 检测出省份的图片名称
param {*} name 检测出省份的图片名称
param {*} result 省份名称
'''
def record_result(name,result):
    global word_detect_path
    if not os.path.exists("log"):
        os.makedirs("log")
    with open(word_detect_path, 'a') as f:
        f.write(result+'\r')  #文件的写操作
        # f.write(" " + name + ".jpg\r")

def record_error(name,result):
    global word_detect_path
    if not os.path.exists("log"):
        os.makedirs("log")
    with open(word_detect_path, 'a') as f:
        f.write(result)  #文件的写操作
        f.write(" " + name + ".jpg\r")
'''
description:  记录识别到的所有字符串 及 对应的图片名称
param {*} name 图片名称 
param {*} detail 识别到的所有字符串
'''
def record_detail(name,detail):
    global detect_detail_path
    with open(detect_detail_path, 'a') as f:
        for i in range(len(detail)):
            f.write(detail[i] +" ")  #文件的写操作
        f.write(" " + name + ".jpg\r")

'''
description:  保存省份未匹配成功的图片（缩小尺寸）
param {*} name 图片名称（路径）
param {*} img 图片
'''
def error_img_save(name,img):
    global error_image_save_path
    try:
        img.save(os.path.join(error_image_save_path , name + ".jpg"))
    except:
        cv2.imwrite(os.path.join(error_image_save_path , name+ ".jpg"),img)


'''
description: 匹配文字框与轮廓框，获取包含文字框的轮廓
param cnt 轮廓框
param text_box 文本框
return {*} 返回包含文字框的轮廓，否则为None
'''
def get_bbox(cnt,text_box):
    for i in cnt:
        # print(i)
        # text_center = get_bbox_center(text_box)
        # if text_center[0] > i[0][0] and text_center[0] < i[1][0] \
        #     and text_center[1] > i[0][1] and text_center[0] < i[3][1]:
        #     return i
        if (text_box[0][0] > i[0][0]-30 and text_box[0][1] > i[0][1]-30 and\
            text_box[1][0] < i[1][0]+30 and text_box[1][1] > i[1][1]-30):
            # print("outpout ",i)
            return i
    return None

'''
description: 获取轮廓中心坐标
param box 轮廓
return {*} 中心坐标
'''
def get_bbox_center(box):
    x = (box[1][0] - box[0][0])/2 +  box[0][0]
    y = (box[3][1] - box[0][1])/2 +  box[0][1]

    return (int(x),int(y))

'''
description: 获取识别结果，匹配省份信息，返回省份
param img 最终处理后的图片
param name 图片名称
param origin_img 原始图片
param short_size 图片短边长度
return {*}
'''
def get_result(img,name,origin_img,short_size):
    PIL_image = Image.fromarray(cv2.cvtColor(img,cv2.COLOR_BGR2RGB))
    # time.sleep(1) 
    detected_word = OcrHandle().text_predict(PIL_image,short_size)
    result = searcch_provience(detected_word)
    # print("result:",result)
    if result is "":
        PIL_image = PIL_image.transpose(Image.ROTATE_180)
        show("04-2Rotatede",cv2.cvtColor(np.asarray(PIL_image), cv2.COLOR_RGB2BGR))
        detected_word = OcrHandle().text_predict(PIL_image,short_size)
        result = searcch_provience(detected_word)
        if result is "":
            for i in range(4):
                PIL_image = PIL_image.transpose(Image.ROTATE_90)
                show("04-2Rotatede",cv2.cvtColor(np.asarray(PIL_image), cv2.COLOR_RGB2BGR),1)
                detected_word = OcrHandle().text_predict(PIL_image,short_size)
                result = searcch_provience(detected_word)
                if result is not "":
                    break
            error_img_save(name,origin_img)

    return result,detected_word

'''
description:  最终使用识别函数，通过30度微旋转，纠正由于文字框误识别导致的错误旋转 
param {*} img 传入处理图像
param {*} name 图片名称
param {*} origin_img  原始图片，展示错误
param {*} short_size 图片短边
return {*}
'''
def get_result2(img,name,origin_img,short_size):
    global detect_count, image_count, result_image_save_path, error_image_save_path
    img,h,w =  resize_image(img)
    short_size = h if h<w else w
    # print(w,h,short_size)
    for angle in range(0,361,30):
        img = adjust_angle(img,angle)
        #how("rotated",img)
        PIL_image = Image.fromarray(cv2.cvtColor(img,cv2.COLOR_BGR2RGB))
        detected_word = OcrHandle().text_predict(PIL_image,short_size)
        result = searcch_provience(detected_word)
        # record_result(name,result)
        # 记录详细信息
        # record_detail(name,detected_word[1])
        CV_img = cv2.cvtColor(np.asarray(PIL_image),cv2.COLOR_RGB2BGR)
        if result != "":
            # detect_count +=1
            break
    if result == "":
        error_img_save(name,origin_img)
        error_img_save(name+"mask",img)
    # image_count += 1
    return result,detected_word

'''
description: 图像缩放，模型不能适应过大图像，因此进行缩小
param {*} img 传入图像
param {*} scale 指定缩放倍数，默认10倍
return {*}
warning: 窗宽不一致时，进行旋转，图片边缘部分会模糊，极大影响识别，故此处固定长宽
'''
def resize_image(img,scale=10):
    h,w = img.shape[0],img.shape[1]

    img= cv2.resize(img,(image_size,image_size))
    h = image_size
    w = image_size
    # 自适应缩放
    # if(h>1024 or w > 1024):
    #     if h > w:
    #         scale = h /1024
    #     else:
    #         scale = w /1024
    #     h = int(h/scale)
    #     w = int(w/scale)
    #     img= cv2.resize(img,(h,w),interpolation=cv2.INTER_AREA)
    return img,h,w

'''
description: 文字识别 主要流程函数
param path 图像路径
param name 图像名称
return {*}
'''
def word_detect_test(path,name,mode=1):
    #      图片数量总和，  检测成功的数量，    结果保存路径，            错误保存路径
    global detect_count, image_count, result_image_save_path, error_image_save_path
    print("{} START".format(name))
    # 模式区分
    # 1：传入图片路径
    if mode == 1:
        img = cv2.imread(path)
    # else: 传入 图像
    else:
        img = name
    img,h,w =  resize_image(img)
    # short_size 用于 dbnet_infer，文字识别模型检测
    short_size = h if h<w else w
    print(h,w,short_size)
    origin = img.copy()
    # show("origin",img)

    # 第一次蓝色分割
    # img_pre = hsv_split_pre(origin,h,w)
    #how("02hsv_split",img_pre)
    # 第二次白色分割，cnt 轮廓框列表
    cnt, img = hsv_split(origin,h,w)
    show("02-2hsv_split",img)
    max_index = 0
    # 返回识别框的坐标点列表，box_list 文字框列表
    box_list, score_list = text_handle.process(img,short_size)
    if len(box_list) < 1:
        print("detect fail {}".format(name))
        error_img_save(name,origin)
    else:
        # 获取最大识别框下标
        max_index = max_box_index(box_list,img)

        # 绘制展示最大识别框
        # img = draw_max_bbox(img, box_list[max_index])
        # 根据固定下标0 1 两点 计算图片应该旋转的角度
        angle = get_angle(img,box_list[max_index])

        # 图像角度矫正
        img = adjust_angle(img,angle)
        show("03adjust_angle",img)

    # 图像锐化，提高清晰度
    kernel = np.array([[0, -1, 0], [-1, 5, -1], [0, -1, 0]], np.float32) #定义一个核
    img = cv2.filter2D(img, -1, kernel=kernel) 
    #how("04sharp_img",img)

    # 文字识别 获取最终识别结果
    result,detected_word  = get_result2(img,name.split('.')[0],origin,short_size)
    if result is not "":
        detect_count +=1
    # 如果未成功识别，自动标注为安徽
    else:
        result = "安徽"
    image_count +=1

    print("detected:{}\r".format(result))

    # 在图像上绘制最终显示的省份
    CV_image = cv2AddChineseText(origin,result,(50,100),(255,0,0),50)
    if len(box_list) > 1:
        main_bbox =  get_bbox(cnt,box_list[max_index])
        #  如果分割效果不理想，没有产生轮廓框，则使用文字框扩大显示
        if main_bbox is None:
            main_bbox = np.array(bigger_box(box_list[max_index],100))
    else:
        # 如果 没有识别出文字框，则使用分割后轮廓检测出的轮廓
        try:
            main_bbox = cnt[0]
        # 如果 文字框 和 轮廓框 皆无，则指定main_bbox = None
        except:
            main_bbox = None

    print("main_bbox: " ,main_bbox)

    center = (512,512)  
    # 绘制最终显示文本框
    if(not isinstance(main_bbox,type(None))):
        if(main_bbox.all() != None):
            try:
                CV_image = cv2.drawContours(CV_image,[main_bbox],-1,(0,0,255),2)
                center = get_bbox_center(main_bbox)
                print("center:",center)
            except:
                print("main_bbox error")
            #how("05-1bboximage",CV_image)

    center_str = "["+str(center[0])+" "+str(center[1])+"]"
    cv2.putText(CV_image,center_str,center,
                    cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 2)
    h_w_str = str(h) +"*" + str(w)

    # 重要显示结果 ，标题上有图像尺寸，与快递盒识别中心
    img_hstack = np.hstack((origin,CV_image))
    # show(origin,'origin')
    # show(CV_image,'result')
    show("result: {} {}".format(h_w_str,center_str),img_hstack)
    cv2.moveWindow("result: {} {}".format(h_w_str,center_str),0,0)
    # 记录结果
    name = name.split('.')[0]
    cv2.imwrite(result_image_save_path +"{}_del.jpg".format(name), CV_image)
    # 记录任务要求信息
    record_result(name,result+",("+str(center[0])+" "+str(center[1])+")")
    # 记录详细信息
    record_detail(name,detected_word[1])




'''
cost time:481.8735296726227 s
cost time:8.031225494543712 m
sum:399 detected:331, rate:0.8295739348370927
    简单的90度旋转图片识别
cost time:509.4182047843933 s
cost time:8.490303413073223 m
sum:394 detected:347, rate:0.8807106598984772
    简单的60度旋转图片识别

cost time:634.2108728885651 s
cost time:10.570181214809418 m
sum:399 detected:369, rate:0.924812030075188
    简单的30度旋转图片识别
'''
'''
description: 简单识别测试函数，仅仅添加旋转处理
param {*} path 图片传入路径
param {*} name  图片名称
return {*}
'''
def word_detect_test2(path,name):
    global detect_count, image_count, result_image_save_path, error_image_save_path
    # step1 read Image
    img = cv2.imread(path)
    # step2 resize image ,set h*w = 1024*1024
    img,h,w =  resize_image(img)
    short_size = h if h<w else w
    # print(w,h,short_size)
    # step3 rotate and detect word
    for angle in range(0,360,30):
        img = adjust_angle(img,angle)
        # transgorm image from opencv's type to PIL's type
        PIL_image = Image.fromarray(cv2.cvtColor(img,cv2.COLOR_BGR2RGB))
        # Main Detect
        detected_word = OcrHandle().text_predict(PIL_image,short_size)
        # serach provience from detected str(detected_word)
        result = searcch_provience(detected_word)
        # record result to result.txt
        record_result(name,result)
        # 记录详细信息
        record_detail(name,detected_word[1])
        CV_img = cv2.cvtColor(np.asarray(PIL_image),cv2.COLOR_RGB2BGR)
        if result != "":
            detect_count +=1
            break
    image_count += 1
    return result
        

'''
description: 清空所有记录表，便于记录本次识别数据
return {*}
'''
def prepare():
    global word_detect_path
    if os.path.exists(word_detect_path):
        os.remove(word_detect_path)
    if os.path.exists(detect_detail_path):
        os.remove(detect_detail_path)
    if os.path.exists(result_image_save_path):
        for i in os.listdir(result_image_save_path):
            os.remove(os.path.join(result_image_save_path,i))
    if os.path.exists(error_image_save_path):
        for i in os.listdir(error_image_save_path):
            os.remove(os.path.join(error_image_save_path,i))
    print("prepare done!")

'''
description: 统合流程，处理400张训练集
return {*}
'''
def total_main():
    for i in os.listdir(source_image_path):
        if "jpg" in i:
            if "mask" in i:
                continue
            # hsv_split(source_image_path + i,i)
            word_detect_test(source_image_path + i,i)
            # word_detect_test(source_image_path + "62.jpg","62.jpg")
            cv2.waitKey(0)&0xff
            cv2.destroyAllWindows()
            print(i + " done!")


'''
description: 本函数流程适用于 以数字顺序命名的图片，如1.jpg,2.jpg……。
return {*}
'''
def random_20_nums_start():
    file  = os.listdir(source_image_path)
    detected_list =[18,]
    for i in range(1):
        num = random.randint(1,len(file))
        while num  in detected_list:
            num = random.randint(0,len(file))
        detected_list.append(num)
        print(source_image_path + "{}.jpg".format(num))
        word_detect_test(source_image_path + "{}.jpg".format(399),str(399))
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        print(str(num) + " done!")

'''
description:  本函数具有通用性，能够适应各种图片名称，结尾指定 jpg
return {*}
'''
def random_20_infile_start():
    count = 0
    for i in os.listdir(source_image_path):
        if i.endswith('jpg') :
            word_detect_test(source_image_path + i,i)
            count +=1
        if count == 20:
            break
        cv2.waitKey(2000)
        cv2.destroyAllWindows()



if __name__ == "__main__":
    
    prepare()
    # 开始计时
    start_time = time.time()
    # random_20_infile_start()
    total_main()
    # random_20_nums_start()
    # total_main()
    # 结束计时
    end_time  = time.time()
    print("cost time:{} s".format(end_time - start_time))
    print("cost time:{} m".format((end_time - start_time)/60))
    # 输出 总共检测图片数量，检测成功图片数量，正确识别率
    print("sum:{} detected:{}, rate:{}".format(image_count, detect_count, detect_count/image_count) )