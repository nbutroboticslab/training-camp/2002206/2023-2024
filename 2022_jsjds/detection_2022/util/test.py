# coding:utf-8
import sys
sys.path.remove("/opt/ros/kinetic/lib/python2.7/dist-packages")
import cv2
import numpy as np
cap = cv2.VideoCapture("/dev/video0")
flag = cap.isOpened()
print(flag)
i = 1

# 曝光平衡
def light_balance(img):
    # 转换为灰度图
    gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    
    # 计算当前曝光度
    mean_brightness = np.mean(gray_frame)

    # 设置目标曝光度
    target_brightness = 120

    # 计算曝光度系数
    alpha = target_brightness / mean_brightness

    # 调整曝光度
    adjusted_frame = cv2.converScaleAbs(img, alpha=0.2, beta=0)

    # 直方图均衡化
    adjusted_frame = cv2.equalizeHist(adjusted_frame)

    # 返回调整后的帧
    return adjusted_frame

while (flag):
    ret, frame = cap.read()
    i += 1
    if i>70:  # 按下s键，进入下面的保存图片操作
        file_name = "test_R_DOWN"
        cv2.imwrite("/home/eaibot/2022_jsjds/detection_2022/util/"+file_name+".png" , frame)
        cv2.imwrite("/home/eaibot/2022_jsjds/detection_2022/util/"+file_name+"_after_adjusted.png" , frame)
        print("save" + str(i) + ".jpg successfuly!")
        break
cap.release() # 释放摄像头
