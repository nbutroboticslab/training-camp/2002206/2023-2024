def takeSecond(elem):
    return elem[1]

def findPositionIndex(name,drop_boxes):
    for i,boxName in enumerate(drop_boxes):
        if name==boxName:
            return i
    return 'unkown'
        


def determine_area(store,drop_boxes):
    global area_1
    global area_2
    global area_3
    """
        决策函数，根据所带货物在三个区域的分布决定投递的先后顺序
    """    
    # 统计三个区域的分布
    area_counts=[['one',0],['two',0],['three',0]]
    # 三个区域的货物分布列表
    area_1_list=[]
    area_2_list=[]
    area_3_list=[]
    # 最终传回的投递点序号列表
    positionIndexList=[]

    # 进行统计
    for index,box in store.items():
        positionIndex=findPositionIndex(box,drop_boxes)
        if(positionIndex=='unkown'):
            store[index][1]='unkown'
            area_counts[0][1]+=1
            area_1_list.append(box[1])
        if positionIndex in area_1:
            area_counts[0][1]+=1
            area_1_list.append(box[1])
        elif positionIndex in area_2:
            area_counts[1][1]+=1
            area_2_list.append(box[1])
        elif positionIndex in area_3:
            area_counts[2][1]+=1
            area_3_list.append(box[1])

    # 从高到底进行排序
    area_counts.sort(reverse=True,key=takeSecond)

    # 最后进行拼接
    tmpList=[]
    if area_counts[0][0]=='one':
        positionIndexList=area_1_list
        tmpList=area_2_list+area_3_list
    elif area_counts[0][0][0]=='two':
        positionIndexList=area_2_list
        tmpList=area_1_list+area_3_list
    else:
        positionIndexList=area_3_list
        tmpList=area_2_list+area_3_list

    positionIndexList+=tmpList    
        
    return positionIndexList

# 第一层，第二层，第三层
drop_position = [-27.0, 10, 44.0]

def store_full(store_list):
    """检测是否已装满"""
    # 减少至一层
    for i in range(1,2):
        if store_list["l"+str(i)][0]:
            return False
        if store_list["r"+str(i)][0]:
            return False
    return True


def store_empty(store_list):
    """检测是否为空"""
    for _, v in store_list.items():
        if not v[0]:
            return False
    return True


def check_store(box_name, store_list):
    """
    box_name: 投递箱的标签
    检查当前是否有匹配的盒子
    """
    # TODO：减少至一层，应该取消循环
    for i in range(1, 0, -1):
        if not store_list["r" + str(i)][0] and (store_list["r" + str(i)][1] == box_name or store_list["r" + str(i)][1] == "unknown"):
            return True
        # if not store_list["m" + str(i)][0] and store_list["m" + str(i)][1] == box_name:
        #     return True
        if not store_list["l" + str(i)][0] and (store_list["l" + str(i)][1] == box_name or store_list["l" + str(i)][1] == "unknown"):
            return True
    return False

def check_unknown(store_list):
    """
    检查当前是否有unknown标签的盒子
    """
    for i in range(1, 0, -1):
        if not store_list["r" + str(i)][0] and  store_list["r" + str(i)][1] == "unknown":
            return True
        # if not store_list["m" + str(i)][0] and store_list["m" + str(i)][1] == box_name:
        #     return True
        if not store_list["l" + str(i)][0] and store_list["l" + str(i)][1] == "unknown":
            return True
    return False

# 初始化投递盒-序号以左上角为0,顺时针为正向一直到左下角的9号箱,起初十个均未知
# drop_boxes = ["unknown"] * 8
drop_boxes = ["湖南","广东","浙江","江苏","福建","河南","安徽","四川"]


##############################
# 将十个投递处划分为三大区域
##############################

area_1=[0,1,8,9]
area_2=[6,7]
area_3=[2,3,4,5]