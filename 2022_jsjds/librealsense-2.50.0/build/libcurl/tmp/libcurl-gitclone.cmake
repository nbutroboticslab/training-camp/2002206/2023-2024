if("2f33be817cbce6ad7a36f27dd7ada9219f13584c" STREQUAL "")
  message(FATAL_ERROR "Tag for git checkout should not be empty.")
endif()

set(run 0)

if("/home/eaibot/2022_jsjds/librealsense-2.50.0/build/libcurl/src/libcurl-stamp/libcurl-gitinfo.txt" IS_NEWER_THAN "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/libcurl/src/libcurl-stamp/libcurl-gitclone-lastrun.txt")
  set(run 1)
endif()

if(NOT run)
  message(STATUS "Avoiding repeated git clone, stamp file is up to date: '/home/eaibot/2022_jsjds/librealsense-2.50.0/build/libcurl/src/libcurl-stamp/libcurl-gitclone-lastrun.txt'")
  return()
endif()

execute_process(
  COMMAND ${CMAKE_COMMAND} -E remove_directory "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/third-party/libcurl"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to remove directory: '/home/eaibot/2022_jsjds/librealsense-2.50.0/build/third-party/libcurl'")
endif()

# try the clone 3 times incase there is an odd git clone issue
set(error_code 1)
set(number_of_tries 0)
while(error_code AND number_of_tries LESS 3)
  execute_process(
    COMMAND "/usr/bin/git" clone --origin "origin" "https://github.com/curl/curl.git" "libcurl"
    WORKING_DIRECTORY "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/third-party"
    RESULT_VARIABLE error_code
    )
  math(EXPR number_of_tries "${number_of_tries} + 1")
endwhile()
if(number_of_tries GREATER 1)
  message(STATUS "Had to git clone more than once:
          ${number_of_tries} times.")
endif()
if(error_code)
  message(FATAL_ERROR "Failed to clone repository: 'https://github.com/curl/curl.git'")
endif()

execute_process(
  COMMAND "/usr/bin/git" checkout 2f33be817cbce6ad7a36f27dd7ada9219f13584c
  WORKING_DIRECTORY "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/third-party/libcurl"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to checkout tag: '2f33be817cbce6ad7a36f27dd7ada9219f13584c'")
endif()

execute_process(
  COMMAND "/usr/bin/git" submodule init 
  WORKING_DIRECTORY "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/third-party/libcurl"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to init submodules in: '/home/eaibot/2022_jsjds/librealsense-2.50.0/build/third-party/libcurl'")
endif()

execute_process(
  COMMAND "/usr/bin/git" submodule update --recursive 
  WORKING_DIRECTORY "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/third-party/libcurl"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to update submodules in: '/home/eaibot/2022_jsjds/librealsense-2.50.0/build/third-party/libcurl'")
endif()

# Complete success, update the script-last-run stamp file:
#
execute_process(
  COMMAND ${CMAKE_COMMAND} -E copy
    "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/libcurl/src/libcurl-stamp/libcurl-gitinfo.txt"
    "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/libcurl/src/libcurl-stamp/libcurl-gitclone-lastrun.txt"
  WORKING_DIRECTORY "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/third-party/libcurl"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to copy script-last-run stamp file: '/home/eaibot/2022_jsjds/librealsense-2.50.0/build/libcurl/src/libcurl-stamp/libcurl-gitclone-lastrun.txt'")
endif()

