# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/eaibot/2022_jsjds/librealsense-2.50.0/examples/C/color/rs-color.c" "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/examples/C/color/CMakeFiles/rs-color.dir/rs-color.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "BUILD_EASYLOGGINGPP"
  "BUILD_SHARED_LIBS"
  "CHECK_FOR_UPDATES"
  "COM_MULTITHREADED"
  "CURL_STATICLIB"
  "EASYLOGGINGPP_ASYNC"
  "ELPP_NO_DEFAULT_LOG_FILE"
  "ELPP_THREAD_SAFE"
  "HWM_OVER_XU"
  "RS2_USE_V4L2_BACKEND"
  "SQLITE_HAVE_ISNAN"
  "UNICODE"
  "USING_UDEV"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../examples/C/color/../../common"
  "../examples/C/color/../../third-party/imgui"
  "../examples/C/color/../../C"
  "."
  "../include"
  "../third-party/glfw/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/CMakeFiles/realsense2.dir/DependInfo.cmake"
  "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/third-party/glfw/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
