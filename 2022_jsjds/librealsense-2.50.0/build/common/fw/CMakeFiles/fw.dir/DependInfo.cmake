# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/common/fw/D4XX_FW_Image.c" "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/common/fw/CMakeFiles/fw.dir/D4XX_FW_Image.c.o"
  "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/common/fw/L51X_FW_Image.c" "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/common/fw/CMakeFiles/fw.dir/L51X_FW_Image.c.o"
  "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/common/fw/L53X_FW_Image.c" "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/common/fw/CMakeFiles/fw.dir/L53X_FW_Image.c.o"
  "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/common/fw/SR3XX_FW_Image.c" "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/common/fw/CMakeFiles/fw.dir/SR3XX_FW_Image.c.o"
  "/home/eaibot/2022_jsjds/librealsense-2.50.0/common/fw/empty.c" "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/common/fw/CMakeFiles/fw.dir/empty.c.o"
  "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/common/fw/target.c" "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/common/fw/CMakeFiles/fw.dir/target.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "BUILD_EASYLOGGINGPP"
  "BUILD_SHARED_LIBS"
  "CHECK_FOR_UPDATES"
  "COM_MULTITHREADED"
  "CURL_STATICLIB"
  "EASYLOGGINGPP_ASYNC"
  "ELPP_NO_DEFAULT_LOG_FILE"
  "ELPP_THREAD_SAFE"
  "HWM_OVER_XU"
  "RS2_USE_V4L2_BACKEND"
  "SQLITE_HAVE_ISNAN"
  "UNICODE"
  "USING_UDEV"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "common/fw"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
