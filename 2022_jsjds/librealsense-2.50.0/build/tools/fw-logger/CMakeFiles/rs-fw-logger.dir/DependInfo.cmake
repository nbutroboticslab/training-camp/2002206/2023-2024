# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/eaibot/2022_jsjds/librealsense-2.50.0/tools/fw-logger/rs-fw-logger.cpp" "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/tools/fw-logger/CMakeFiles/rs-fw-logger.dir/rs-fw-logger.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BUILD_EASYLOGGINGPP"
  "BUILD_SHARED_LIBS"
  "CHECK_FOR_UPDATES"
  "COM_MULTITHREADED"
  "CURL_STATICLIB"
  "EASYLOGGINGPP_ASYNC"
  "ELPP_NO_DEFAULT_LOG_FILE"
  "ELPP_THREAD_SAFE"
  "HWM_OVER_XU"
  "RS2_USE_V4L2_BACKEND"
  "SQLITE_HAVE_ISNAN"
  "UNICODE"
  "USING_UDEV"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../tools/fw-logger/../../common"
  "../tools/fw-logger/../../third-party/tclap/include"
  "../tools/fw-logger/cpp-fw-logger-helpers"
  "."
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/CMakeFiles/realsense2.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
