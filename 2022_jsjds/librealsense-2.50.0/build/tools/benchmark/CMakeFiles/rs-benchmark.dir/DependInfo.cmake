# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/eaibot/2022_jsjds/librealsense-2.50.0/third-party/glad/glad.c" "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/tools/benchmark/CMakeFiles/rs-benchmark.dir/__/__/third-party/glad/glad.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "BUILD_EASYLOGGINGPP"
  "BUILD_SHARED_LIBS"
  "CHECK_FOR_UPDATES"
  "COM_MULTITHREADED"
  "CURL_STATICLIB"
  "EASYLOGGINGPP_ASYNC"
  "ELPP_NO_DEFAULT_LOG_FILE"
  "ELPP_THREAD_SAFE"
  "HWM_OVER_XU"
  "RS2_USE_V4L2_BACKEND"
  "SQLITE_HAVE_ISNAN"
  "UNICODE"
  "USING_UDEV"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../tools/benchmark/../../third-party/tclap/include"
  "../tools/benchmark/../../third-party/glad"
  "../tools/benchmark/../../examples"
  "."
  "../include"
  "../third-party/glfw/include"
  "../src/gl/../../include"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/eaibot/2022_jsjds/librealsense-2.50.0/tools/benchmark/rs-benchmark.cpp" "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/tools/benchmark/CMakeFiles/rs-benchmark.dir/rs-benchmark.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BUILD_EASYLOGGINGPP"
  "BUILD_SHARED_LIBS"
  "CHECK_FOR_UPDATES"
  "COM_MULTITHREADED"
  "CURL_STATICLIB"
  "EASYLOGGINGPP_ASYNC"
  "ELPP_NO_DEFAULT_LOG_FILE"
  "ELPP_THREAD_SAFE"
  "HWM_OVER_XU"
  "RS2_USE_V4L2_BACKEND"
  "SQLITE_HAVE_ISNAN"
  "UNICODE"
  "USING_UDEV"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../tools/benchmark/../../third-party/tclap/include"
  "../tools/benchmark/../../third-party/glad"
  "../tools/benchmark/../../examples"
  "."
  "../include"
  "../third-party/glfw/include"
  "../src/gl/../../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/third-party/glfw/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/src/gl/CMakeFiles/realsense2-gl.dir/DependInfo.cmake"
  "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/CMakeFiles/realsense2.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
