# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/eaibot/2022_jsjds/librealsense-2.50.0/third-party/realsense-file/lz4/lz4.c" "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/tools/embed/CMakeFiles/rs-embed.dir/__/__/third-party/realsense-file/lz4/lz4.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "BUILD_EASYLOGGINGPP"
  "BUILD_SHARED_LIBS"
  "CHECK_FOR_UPDATES"
  "COM_MULTITHREADED"
  "CURL_STATICLIB"
  "EASYLOGGINGPP_ASYNC"
  "ELPP_NO_DEFAULT_LOG_FILE"
  "ELPP_THREAD_SAFE"
  "HWM_OVER_XU"
  "RS2_USE_V4L2_BACKEND"
  "SQLITE_HAVE_ISNAN"
  "UNICODE"
  "USING_UDEV"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../tools/embed/rs-embed"
  "../third-party/realsense-file/lz4"
  "../tools/embed/../../third-party"
  "../tools/embed/../../third-party/tclap/include"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/eaibot/2022_jsjds/librealsense-2.50.0/tools/embed/rs-embed.cpp" "/home/eaibot/2022_jsjds/librealsense-2.50.0/build/tools/embed/CMakeFiles/rs-embed.dir/rs-embed.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BUILD_EASYLOGGINGPP"
  "BUILD_SHARED_LIBS"
  "CHECK_FOR_UPDATES"
  "COM_MULTITHREADED"
  "CURL_STATICLIB"
  "EASYLOGGINGPP_ASYNC"
  "ELPP_NO_DEFAULT_LOG_FILE"
  "ELPP_THREAD_SAFE"
  "HWM_OVER_XU"
  "RS2_USE_V4L2_BACKEND"
  "SQLITE_HAVE_ISNAN"
  "UNICODE"
  "USING_UDEV"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../tools/embed/rs-embed"
  "../third-party/realsense-file/lz4"
  "../tools/embed/../../third-party"
  "../tools/embed/../../third-party/tclap/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
