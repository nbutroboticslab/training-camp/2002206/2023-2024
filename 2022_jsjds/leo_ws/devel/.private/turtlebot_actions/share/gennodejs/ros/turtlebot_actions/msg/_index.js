
"use strict";

let FindFiducialActionFeedback = require('./FindFiducialActionFeedback.js');
let FindFiducialResult = require('./FindFiducialResult.js');
let FindFiducialActionGoal = require('./FindFiducialActionGoal.js');
let TurtlebotMoveActionResult = require('./TurtlebotMoveActionResult.js');
let FindFiducialAction = require('./FindFiducialAction.js');
let TurtlebotMoveResult = require('./TurtlebotMoveResult.js');
let TurtlebotMoveFeedback = require('./TurtlebotMoveFeedback.js');
let TurtlebotMoveAction = require('./TurtlebotMoveAction.js');
let TurtlebotMoveActionFeedback = require('./TurtlebotMoveActionFeedback.js');
let FindFiducialGoal = require('./FindFiducialGoal.js');
let FindFiducialFeedback = require('./FindFiducialFeedback.js');
let TurtlebotMoveGoal = require('./TurtlebotMoveGoal.js');
let FindFiducialActionResult = require('./FindFiducialActionResult.js');
let TurtlebotMoveActionGoal = require('./TurtlebotMoveActionGoal.js');

module.exports = {
  FindFiducialActionFeedback: FindFiducialActionFeedback,
  FindFiducialResult: FindFiducialResult,
  FindFiducialActionGoal: FindFiducialActionGoal,
  TurtlebotMoveActionResult: TurtlebotMoveActionResult,
  FindFiducialAction: FindFiducialAction,
  TurtlebotMoveResult: TurtlebotMoveResult,
  TurtlebotMoveFeedback: TurtlebotMoveFeedback,
  TurtlebotMoveAction: TurtlebotMoveAction,
  TurtlebotMoveActionFeedback: TurtlebotMoveActionFeedback,
  FindFiducialGoal: FindFiducialGoal,
  FindFiducialFeedback: FindFiducialFeedback,
  TurtlebotMoveGoal: TurtlebotMoveGoal,
  FindFiducialActionResult: FindFiducialActionResult,
  TurtlebotMoveActionGoal: TurtlebotMoveActionGoal,
};
