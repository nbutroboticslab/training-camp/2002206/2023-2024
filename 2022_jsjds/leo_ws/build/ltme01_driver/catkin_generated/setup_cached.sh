#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/eaibot/2022_jsjds/leo_ws/devel/.private/ltme01_driver:$CMAKE_PREFIX_PATH"
export PWD='/home/eaibot/2022_jsjds/leo_ws/build/ltme01_driver'
export ROSLISP_PACKAGE_DIRECTORIES="/home/eaibot/2022_jsjds/leo_ws/devel/.private/ltme01_driver/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/eaibot/2022_jsjds/leo_ws/src/ltme01_driver-1.0.1:$ROS_PACKAGE_PATH"