#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/eaibot/2022_jsjds/leo_ws/devel/.private/eai_tutorials:$CMAKE_PREFIX_PATH"
export PWD='/home/eaibot/2022_jsjds/leo_ws/build/eai_tutorials'
export ROSLISP_PACKAGE_DIRECTORIES="/home/eaibot/2022_jsjds/leo_ws/devel/.private/eai_tutorials/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/eaibot/2022_jsjds/leo_ws/src/eai_tutorials:$ROS_PACKAGE_PATH"