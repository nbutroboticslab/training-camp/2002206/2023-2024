set(_CATKIN_CURRENT_PACKAGE "eai_tutorials")
set(eai_tutorials_VERSION "0.0.0")
set(eai_tutorials_MAINTAINER "eaibot <eaibot@todo.todo>")
set(eai_tutorials_PACKAGE_FORMAT "2")
set(eai_tutorials_BUILD_DEPENDS "actionlib" "actionlib_msgs" "roscpp" "rospy" "std_msgs")
set(eai_tutorials_BUILD_EXPORT_DEPENDS "actionlib" "actionlib_msgs" "roscpp" "rospy" "std_msgs")
set(eai_tutorials_BUILDTOOL_DEPENDS "catkin")
set(eai_tutorials_BUILDTOOL_EXPORT_DEPENDS )
set(eai_tutorials_EXEC_DEPENDS "actionlib" "actionlib_msgs" "roscpp" "rospy" "std_msgs")
set(eai_tutorials_RUN_DEPENDS "actionlib" "actionlib_msgs" "roscpp" "rospy" "std_msgs")
set(eai_tutorials_TEST_DEPENDS )
set(eai_tutorials_DOC_DEPENDS )
set(eai_tutorials_URL_WEBSITE "")
set(eai_tutorials_URL_BUGTRACKER "")
set(eai_tutorials_URL_REPOSITORY "")
set(eai_tutorials_DEPRECATED "")