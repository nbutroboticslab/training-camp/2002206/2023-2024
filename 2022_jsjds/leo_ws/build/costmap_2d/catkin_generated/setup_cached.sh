#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/eaibot/2022_jsjds/leo_ws/devel/.private/costmap_2d:$CMAKE_PREFIX_PATH"
export PWD='/home/eaibot/2022_jsjds/leo_ws/build/costmap_2d'
export ROSLISP_PACKAGE_DIRECTORIES="/home/eaibot/2022_jsjds/leo_ws/devel/.private/costmap_2d/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/eaibot/2022_jsjds/leo_ws/src/navigation-kinetic-devel/costmap_2d:$ROS_PACKAGE_PATH"