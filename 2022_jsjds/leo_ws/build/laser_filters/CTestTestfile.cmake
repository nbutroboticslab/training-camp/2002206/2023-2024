# CMake generated Testfile for 
# Source directory: /home/eaibot/2022_jsjds/leo_ws/src/laser_filters
# Build directory: /home/eaibot/2022_jsjds/leo_ws/build/laser_filters
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_laser_filters_rostest_test_test_scan_filter_chain.launch "/home/eaibot/2022_jsjds/leo_ws/build/laser_filters/catkin_generated/env_cached.sh" "/usr/bin/python2" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/eaibot/2022_jsjds/leo_ws/build/laser_filters/test_results/laser_filters/rostest-test_test_scan_filter_chain.xml" "--return-code" "/opt/ros/kinetic/share/rostest/cmake/../../../bin/rostest --pkgdir=/home/eaibot/2022_jsjds/leo_ws/src/laser_filters --package=laser_filters --results-filename test_test_scan_filter_chain.xml --results-base-dir \"/home/eaibot/2022_jsjds/leo_ws/build/laser_filters/test_results\" /home/eaibot/2022_jsjds/leo_ws/src/laser_filters/test/test_scan_filter_chain.launch ")
subdirs(gtest)
